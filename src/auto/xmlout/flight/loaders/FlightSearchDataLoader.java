package auto.xmlout.flight.loaders;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import auto.xmlout.com.types.CabinType;
import auto.xmlout.com.types.HotelXmlSearchByType;
import auto.xmlout.com.types.ServerType;
import auto.xmlout.com.types.TripType;
import auto.xmlout.flight.pojo.FlightSearchType;
import auto.xmlout.hotel.pojo.SearchType;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.hotel.pojo.RoomType;
import auto.xmlout.hotel.pojo.CartDetails;

public class FlightSearchDataLoader {

	private Map<String, String> CurrencyMap;
	private String PortalCurrency;
	private Logger logger = null;

	public FlightSearchDataLoader(Map<Integer, String> map, String Currency) {
		logger = Logger.getLogger(this.getClass().getName());

		try {
			PortalCurrency = Currency;
			CurrencyMap = new HashMap<String, String>();
			Iterator<Map.Entry<Integer, String>> iterator = map.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<Integer, String> entry = (Map.Entry<Integer, String>) iterator.next();
				String[] AllValues = entry.getValue().split(",");
				CurrencyMap.put(AllValues[0], AllValues[1]);

			}

		} catch (Exception e) {
			logger.fatal("Error when updating the currency list -->", e);
			// System.out.println("ErrorOccured While updating the CurrencyList");
		}

	}

	public Map<String, FlightSearchType> loadSearchDetails(Map<Integer, String> map) throws ParseException {
		Map<String, FlightSearchType> searchMap = new HashMap<String, FlightSearchType>();

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			FlightSearchType CurrentSearch = new FlightSearchType();
			String[] AllValues = it.next().getValue().split(",");

			CurrentSearch.setCountryOFResidance(AllValues[1].trim());

			CurrentSearch.setSerachBy(HotelXmlSearchByType.getChargeType(AllValues[2].trim()));
			CurrentSearch.setTriptype(TripType.getChargeType(AllValues[3].trim()));
			CurrentSearch.setDepLocation1(AllValues[4].trim());
			CurrentSearch.setDepartureDate(AllValues[5].trim());
			CurrentSearch.setArrLocation1(AllValues[6].trim());
			CurrentSearch.setDepLocation2(AllValues[7].trim());
			CurrentSearch.setArrivalDate(AllValues[8].trim());
			CurrentSearch.setArrLocation2(AllValues[9].trim());
			CurrentSearch.setBookingType(AllValues[10].trim());
			// CurrentSearch.setBookinDateType(AllValues[4].trim());

			CurrentSearch.setAdults(AllValues[11].trim());
			CurrentSearch.setChildren(AllValues[12].trim());
			CurrentSearch.setInfants(AllValues[13].trim());
			CurrentSearch.setChildAges(AllValues[14].trim().split(";"));
			CurrentSearch.setCabinClass(AllValues[15].trim());
			CurrentSearch.setNonstop(AllValues[16].trim().equalsIgnoreCase("true") ? true : false);
			CurrentSearch.setPrefferedVendor(AllValues[17].trim());
			CurrentSearch.setAvailability(AllValues[18].trim());
			// CurrentSearch.setHotelAvailability(AllValues[11]);
			// CurrentSearch.setRoomTypes(AllValues[13].trim());
			// CurrentSearch.setDefaultRoom(AllValues[14].trim());
			// CurrentSearch.setSelectedRooms(AllValues[15].trim());
			CurrentSearch.setDescription(AllValues[19].trim());
			CurrentSearch.setSearchCurrency(AllValues[20].trim());
			// CurrentSearch.setTotalRate(AllValues[15].trim());
			CurrentSearch.setPaymentType(AllValues[21].trim());
			CurrentSearch.setCardType(AllValues[22].trim());
			CurrentSearch.setOfflineMethod(AllValues[23].trim());
			CurrentSearch.setPaymentRef(AllValues[24].trim());
			CurrentSearch.setServertype(ServerType.getChargeType(AllValues[25].trim()));
			CurrentSearch.setAgentID(AllValues[26].trim());
			CurrentSearch.setScenarioExcecutionState(AllValues[27].trim());
			CurrentSearch.setResultsSortOrderType(AllValues[28].trim());
			CurrentSearch.setResultsSortByType(AllValues[29].trim());
			CurrentSearch.setBaseCurrency(PortalCurrency);
			/*
			 * if(!(AllValues[13].equalsIgnoreCase("Not Available")) && !(PortalCurrency.equalsIgnoreCase(AllValues[14]))) CurrentSearch.setExpected (AllValues[13],CurrencyMap.get(AllValues[14]));
			 * else CurrentSearch.setExpected(AllValues[13]);
			 */

			searchMap.put(AllValues[0].trim(), CurrentSearch);
		}
		return searchMap;

	}

	public double convertCurrency(String Value, String FromCurrency, String ToCurrency) {
		double returnValue = 0.0;

		try {
			double value = Double.parseDouble(Value.trim());
			double Portalvalue = value / (Double.parseDouble(CurrencyMap.get(FromCurrency.trim())));
			returnValue = Portalvalue * (Double.parseDouble(CurrencyMap.get(ToCurrency.trim())));
		} catch (Exception e) {
			System.out.println("Error in exchange rate conversion" + e.toString());
		}

		return returnValue;

	}

	public Map<String, FlightSearchType> getSearchDetails(ArrayList<Map<Integer, String>> sheetList) {
		Map<String, FlightSearchType> SearchList = new HashMap<String, FlightSearchType>();
		try {
			SearchList = loadSearchDetails(sheetList.get(0));
			logger.info("Initial Flight Search Details Loaded Successfully");
			logger.debug(SearchList);
		} catch (Exception e) {
			logger.fatal("Initial Flight Search Details Not Loaded Sucessfully", e);

		}

		return SearchList;

	}

}
