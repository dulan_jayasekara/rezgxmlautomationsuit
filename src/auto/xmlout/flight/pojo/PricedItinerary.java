package auto.xmlout.flight.pojo;

import auto.xmlout.com.types.PricingSourceType;

public class PricedItinerary {

private String SequenceNumber                = "";
private String DirectionIndicator            = "";
private PricingSourceType PricingSource      = PricingSourceType.NONE;
private String QuoteID                       = "";
private String TicketTimeLimit               = "";
private String BaseFare                      = "";
private String CurrencyCode                  = "";
private String DecimalPlaces                 = "";
private String Tax                           = "";
private String AditionalFee                  = "";
private String TotalFare                     = "";
private OriginDestinationOption OutBoundOption = null;
private OriginDestinationOption InBoundOption  = null;
private String   FareBasisCode               = "";



public String getCurrencyCode() {
	return CurrencyCode;
}
public void setCurrencyCode(String currencyCode) {
	CurrencyCode = currencyCode;
}
public String getDecimalPlaces() {
	return DecimalPlaces;
}
public void setDecimalPlaces(String decimalPlaces) {
	DecimalPlaces = decimalPlaces;
}
public String getFareBasisCode() {
	return FareBasisCode;
}
public void setFareBasisCode(String fareBasisCode) {
	FareBasisCode = fareBasisCode;
}
public String getSequenceNumber() {
	return SequenceNumber;
}
public void setSequenceNumber(String sequenceNumber) {
	SequenceNumber = sequenceNumber;
}
public String getDirectionIndicator() {
	return DirectionIndicator;
}
public void setDirectionIndicator(String directionIndicator) {
	DirectionIndicator = directionIndicator;
}
public PricingSourceType getPricingSource() {
	return PricingSource;
}
public void setPricingSource(PricingSourceType pricingSource) {
	PricingSource = pricingSource;
}
public String getQuoteID() {
	return QuoteID;
}
public void setQuoteID(String quoteID) {
	QuoteID = quoteID;
}
public String getTicketTimeLimit() {
	return TicketTimeLimit;
}
public void setTicketTimeLimit(String ticketTimeLimit) {
	TicketTimeLimit = ticketTimeLimit;
}
public String getBaseFare() {
	return BaseFare;
}
public void setBaseFare(String baseFare) {
	BaseFare = baseFare;
}
public String getTax() {
	return Tax;
}
public void setTax(String tax) {
	Tax = tax;
}
public String getAditionalFee() {
	return AditionalFee;
}
public void setAditionalFee(String aditionalFee) {
	AditionalFee = aditionalFee;
}
public String getTotalFare() {
	return TotalFare;
}
public void setTotalFare(String totalFare) {
	TotalFare = totalFare;
}
public OriginDestinationOption getOutBoundOption() {
	return OutBoundOption;
}
public void setOutBoundOption(OriginDestinationOption outBoundOption) {
	OutBoundOption = outBoundOption;
}
public OriginDestinationOption getInBoundOption() {
	return InBoundOption;
}
public void setInBoundOption(OriginDestinationOption inBoundOption) {
	InBoundOption = inBoundOption;
}




}
