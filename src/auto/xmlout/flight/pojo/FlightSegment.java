package auto.xmlout.flight.pojo;

public class FlightSegment {
	
private String ArrivalDateTime = "";
private String DepartureDateTime = "";
private String TicketEligibility = "";
private String FlightNumber = "";
private String NumberInParty  = "";
private String RPH	 = "";
private String ResBookDesigCode = "";
private boolean StopoverInd = false;
private String DepartureLocationCode = "";
private String ArrivalLocationCode = "";
private String OperatingAirline  = "";
private String OperatingAirlineCompany = "";
private String AirEquipTypeCode = "";
private String AirEquipTypeName = "";
private String MarketingAirlineCode  = "";
private String MarketingAirlineName  = "";
private String JourneyTotalDuration = "";
private String CabinType = "";

public String getArrivalDateTime() {
	return ArrivalDateTime;
}
public void setArrivalDateTime(String arrivalDateTime) {
	ArrivalDateTime = arrivalDateTime;
}
public String getDepartureDateTime() {
	return DepartureDateTime;
}
public void setDepartureDateTime(String departureDateTime) {
	DepartureDateTime = departureDateTime;
}
public String getTicketEligibility() {
	return TicketEligibility;
}
public void setTicketEligibility(String ticketEligibility) {
	TicketEligibility = ticketEligibility;
}
public String getFlightNumber() {
	return FlightNumber;
}
public void setFlightNumber(String flightNumber) {
	FlightNumber = flightNumber;
}
public String getNumberInParty() {
	return NumberInParty;
}
public void setNumberInParty(String numberInParty) {
	NumberInParty = numberInParty;
}
public String getRPH() {
	return RPH;
}
public void setRPH(String rPH) {
	RPH = rPH;
}
public String getResBookDesigCode() {
	return ResBookDesigCode;
}
public void setResBookDesigCode(String resBookDesigCode) {
	ResBookDesigCode = resBookDesigCode;
}
public boolean getStopoverInd() {
	return StopoverInd;
}
public void setStopoverInd(boolean stopoverInd) {
	StopoverInd = stopoverInd;
}
public String getDepartureLocationCode() {
	return DepartureLocationCode;
}
public void setDepartureLocationCode(String departureLocationCode) {
	DepartureLocationCode = departureLocationCode;
}
public String getArrivalLocationCode() {
	return ArrivalLocationCode;
}
public void setArrivalLocationCode(String arrivalLocationCode) {
	ArrivalLocationCode = arrivalLocationCode;
}
public String getOperatingAirline() {
	return OperatingAirline;
}
public void setOperatingAirline(String operatingAirline) {
	OperatingAirline = operatingAirline;
}
public String getOperatingAirlineCompany() {
	return OperatingAirlineCompany;
}
public void setOperatingAirlineCompany(String operatingAirlineCompany) {
	OperatingAirlineCompany = operatingAirlineCompany;
}
public String getAirEquipTypeCode() {
	return AirEquipTypeCode;
}
public void setAirEquipTypeCode(String airEquipTypeCode) {
	AirEquipTypeCode = airEquipTypeCode;
}
public String getAirEquipTypeName() {
	return AirEquipTypeName;
}
public void setAirEquipTypeName(String airEquipTypeName) {
	AirEquipTypeName = airEquipTypeName;
}
public String getMarketingAirlineCode() {
	return MarketingAirlineCode;
}
public void setMarketingAirlineCode(String marketingAirlineCode) {
	MarketingAirlineCode = marketingAirlineCode;
}
public String getMarketingAirlineName() {
	return MarketingAirlineName;
}
public void setMarketingAirlineName(String marketingAirlineName) {
	MarketingAirlineName = marketingAirlineName;
}
public String getJourneyTotalDuration() {
	return JourneyTotalDuration;
}
public void setJourneyTotalDuration(String journeyTotalDuration) {
	JourneyTotalDuration = journeyTotalDuration;
}
public String getCabinType() {
	return CabinType;
}
public void setCabinType(String cabinType) {
	CabinType = cabinType;
}


}
