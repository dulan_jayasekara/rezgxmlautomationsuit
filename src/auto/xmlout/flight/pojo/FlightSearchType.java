package auto.xmlout.flight.pojo;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortBy_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortOrder_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type10;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type2;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type20;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type4;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type6;
import auto.xmlout.com.types.ContractType;
import auto.xmlout.com.types.HotelXmlSearchByType;
import auto.xmlout.com.types.ServerType;
import auto.xmlout.com.types.TripType;
import auto.xmlout.com.utilities.Calender;

public class FlightSearchType {

	private String SearchIndex = "";
	private String CountryOFResidance = "";
	private HotelXmlSearchByType SearchBy = HotelXmlSearchByType.NONE;
	private TripType Triptype = TripType.NONE;
	private String DepLocation1 = "";
	private String ArrLocation1 = "";
	private String DepLocation2 = "";
	private String ArrLocation2 = "";
	private String DepartureDate = "N/A";
	private String ArrivalDate = "";
	private String DaysBetween = "3";
	private auto.xmlout.com.types.BookingType BookingType = auto.xmlout.com.types.BookingType.NONE;
	private String Adults = null;
	private String Children = null;
	private String Infants = null;
	private String[] ChildAges = null;
	private xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinType CabinClass = xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinType.Economy;
	private boolean Nonstop = false;
	private String PrefferedVendor = "";
	private String Description = "";
	private String Expected = "";
	private String SearchCurrency = "";
	private String BaseCurrency = "";
	private String CheckOutDate = "";
	private String Indate = null;
	private String OutDate = null;
	private boolean HotelAvailability;
	private String PrefferedAirline = "";
	private String BookingDate = "N/A";
	private String Availability = "";
	private String TotalRate = "";
	private String BookinDateType = "";
	private boolean ScenarioExcecutionState = false;
	private String PaymentType = "-";
	private String CardType = "-";
	private String OfflineMethod = "-";
	private String PaymentRef = "-";
	private int PassengerTypeCount = 1;

	private Target_type6 hotel_info_Target = Target_type6.Test;
	private Target_type2 hotel_room_Target = Target_type2.Test;
	private Target_type20 air_search_Target = Target_type20.Test;
	private Target_type4 hotel_res_Target = Target_type4.Test;
	private Target_type10 hotel_cncl_Target = Target_type10.Test;

	private String AgentID = "Default";
	private String UserName = "Default";
	private String PassWord = "Default";
	private HotelXmlSearchByType serachBy = HotelXmlSearchByType.NONE;
	private String AirPortCode = "";
	private String HotelID = "000";
	private String HotelVendor = "rezbase_V3";
	private SortBy_type0 ResultsSortByType = SortBy_type0.ASC;
	private SortOrder_type0 ResultsSortOrderType = SortOrder_type0.PRICE;
	private ServerType servertype = ServerType.STAGING;
	private String AvailabilityTracer = "";
	private ContractType HotelContract = ContractType.NONE;

	public String getPrefferedAirline() {
		return PrefferedAirline;
	}

	public void setPrefferedAirline(String prefferedAirline) {
		PrefferedAirline = prefferedAirline;
	}

	public int getPassengerTypeCount() {
		return PassengerTypeCount;
	}

	public void setPassengerTypeCount(int passengerTypeCount) {
		PassengerTypeCount = passengerTypeCount;
	}

	public xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinType getCabinClass() {
		return CabinClass;
	}

	public void setCabinClass(String cabinClass) {
		if (cabinClass.equalsIgnoreCase("Economy"))
			CabinClass = xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinType.Economy;
		else if (cabinClass.equalsIgnoreCase("Bussiness"))
			CabinClass = xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinType.Business;
		else if (cabinClass.equalsIgnoreCase("First"))
			;
		CabinClass = xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinType.First;
	}

	public String getAdults() {
		return Adults;
	}

	public void setAdults(String adults) {
		Adults = adults;
	}

	public String getChildren() {
		return Children;
	}

	public void setChildren(String children) {
		Children = children;
		if (!children.equalsIgnoreCase("0"))
			PassengerTypeCount++;

	}

	public String getInfants() {
		return Infants;
	}

	public void setInfants(String infants) {
		Infants = infants;
		if (!infants.equalsIgnoreCase("0"))
			PassengerTypeCount++;
	}

	public void setBookingType(auto.xmlout.com.types.BookingType bookingType) {
		BookingType = bookingType;
	}

	public HotelXmlSearchByType getSearchBy() {
		return SearchBy;
	}

	public void setSearchBy(HotelXmlSearchByType searchBy) {
		SearchBy = searchBy;
	}

	public TripType getTriptype() {
		return Triptype;
	}

	public void setTriptype(TripType triptype) {
		Triptype = triptype;
	}

	public String getDepLocation1() {
		return DepLocation1;
	}

	public void setDepLocation1(String depLocation1) {
		DepLocation1 = depLocation1;
	}

	public String getArrLocation1() {
		return ArrLocation1;
	}

	public void setArrLocation1(String arrLocation1) {
		ArrLocation1 = arrLocation1;
	}

	public String getDepLocation2() {
		return DepLocation2;
	}

	public void setDepLocation2(String depLocation2) {
		DepLocation2 = depLocation2;
	}

	public String getArrLocation2() {
		return ArrLocation2;
	}

	public void setArrLocation2(String arrLocation2) {
		ArrLocation2 = arrLocation2;
	}

	public String getDepartureDate() {
		return DepartureDate;
	}

	public void setDepartureDate(String departureDate) throws ParseException {
		DepartureDate = departureDate;

		if (!departureDate.equalsIgnoreCase("N/A")) {
			Indate = Calender.getDate("dd-MMM-yyyy", "yyyy-MM-dd", departureDate)+"T00:00:00";
			
		}
	}

	public String getDaysBetween() {
		return DaysBetween;
	}

	public void setDaysBetween(String daysBetween) {
		DaysBetween = daysBetween;
	}

	public boolean isNonstop() {
		return Nonstop;
	}

	public void setNonstop(boolean nonstop) {
		Nonstop = nonstop;
	}

	public String getPrefferedVendor() {
		return PrefferedVendor;
	}

	public void setPrefferedVendor(String prefferedVendor) {
		PrefferedVendor = prefferedVendor;
	}

	public void setIndate(String indate) {
		Indate = indate;
	}

	public void setOutDate(String outDate) {
		OutDate = outDate;
	}

	public void setScenarioExcecutionState(boolean scenarioExcecutionState) {
		ScenarioExcecutionState = scenarioExcecutionState;
	}

	public void setHotel_info_Target(Target_type6 hotel_info_Target) {
		this.hotel_info_Target = hotel_info_Target;
	}

	public void setHotel_room_Target(Target_type2 hotel_room_Target) {
		this.hotel_room_Target = hotel_room_Target;
	}

	public void setAir_search_Target(Target_type20 air_search_Target) {
		this.air_search_Target = air_search_Target;
	}

	public void setHotel_res_Target(Target_type4 hotel_res_Target) {
		this.hotel_res_Target = hotel_res_Target;
	}

	public void setHotel_cncl_Target(Target_type10 hotel_cncl_Target) {
		this.hotel_cncl_Target = hotel_cncl_Target;
	}

	public void setResultsSortByType(SortBy_type0 resultsSortByType) {
		ResultsSortByType = resultsSortByType;
	}

	public void setResultsSortOrderType(SortOrder_type0 resultsSortOrderType) {
		ResultsSortOrderType = resultsSortOrderType;
	}

	public void setHotelContract(ContractType hotelContract) {
		HotelContract = hotelContract;
	}

	public ContractType getHotelContract() {
		return HotelContract;
	}

	public void setHotelContract(String hotelContract) {
		HotelContract = ContractType.getChargeType(hotelContract.trim());
	}

	public ServerType getServertype() {
		return servertype;
	}

	public void setServertype(ServerType servertype) {
		this.servertype = servertype;
	}

	public String getAvailabilityTracer() {
		return AvailabilityTracer;
	}

	public void setAvailabilityTracer(String availabilityTracer) {
		AvailabilityTracer = availabilityTracer;
	}

	public SortBy_type0 getResultsSortByType() {
		return ResultsSortByType;
	}

	public void setResultsSortByType(String resultsSortByType) {
		if (resultsSortByType.trim().equalsIgnoreCase("ASC"))
			ResultsSortByType = SortBy_type0.ASC;
		else if (resultsSortByType.trim().equalsIgnoreCase("DSC"))
			ResultsSortByType = SortBy_type0.DESC;
	}

	public SortOrder_type0 getResultsSortOrderType() {
		return ResultsSortOrderType;
	}

	public void setResultsSortOrderType(String resultsSortOrder) {

		if (resultsSortOrder.equalsIgnoreCase("Price"))
			ResultsSortOrderType = SortOrder_type0.PRICE;
		else if (resultsSortOrder.equalsIgnoreCase("StarRating"))
			ResultsSortOrderType = SortOrder_type0.RATING;
		else if (resultsSortOrder.equalsIgnoreCase("Rank"))
			ResultsSortOrderType = SortOrder_type0.HOTELRANK;
		else if (resultsSortOrder.equalsIgnoreCase("HotelName"))
			ResultsSortOrderType = SortOrder_type0.HOTELNAME;
	}

	public String getHotelID() {
		return HotelID;
	}

	public void setHotelID(String hotelID) {
		HotelID = hotelID;
	}

	public String getHotelVendor() {
		return HotelVendor;
	}

	public void setHotelVendor(String hotelVendor) {
		HotelVendor = hotelVendor;
	}

	public String getIndate() {
		return Indate;
	}

	public String getOutDate() {
		return OutDate;
	}

	public String getAirPortCode() {
		return AirPortCode;
	}

	public void setAirPortCode(String airPortCode) {
		AirPortCode = airPortCode;
	}

	public HotelXmlSearchByType getSerachBy() {
		return serachBy;
	}

	public void setSerachBy(HotelXmlSearchByType serachBy) {
		this.serachBy = serachBy;
	}

	public String getAgentID() {
		return AgentID;
	}

	public void setAgentID(String agentID) {
		AgentID = agentID;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassWord() {
		return PassWord;
	}

	public void setPassWord(String passWord) {
		PassWord = passWord;
	}

	public Target_type6 getHotel_info_Target() {
		return hotel_info_Target;
	}

	public Target_type2 getHotel_room_Target() {
		return hotel_room_Target;
	}

	public Target_type20 getAir_search_Target() {
		return air_search_Target;
	}

	public Target_type4 getHotel_res_Target() {
		return hotel_res_Target;
	}

	public Target_type10 getHotel_cncl_Target() {
		return hotel_cncl_Target;
	}

	public void setTargetTypes(String target) {
		if (target.equalsIgnoreCase("Staging")) {
			hotel_info_Target = Target_type6.Test;
			hotel_room_Target = Target_type2.Test;
			air_search_Target = Target_type20.Test;
			hotel_res_Target = Target_type4.Test;
			hotel_cncl_Target = Target_type10.Test;

		} else if (target.equalsIgnoreCase("live")) {
			hotel_info_Target = Target_type6.Production;
			hotel_room_Target = Target_type2.Production;
			air_search_Target = Target_type20.Production;
			hotel_res_Target = Target_type4.Production;
			hotel_cncl_Target = Target_type10.Production;

		}

	}

	public String getBaseCurrency() {
		return BaseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		BaseCurrency = baseCurrency;
	}

	public boolean isScenarioExcecutionState() {
		return ScenarioExcecutionState;
	}

	public void setScenarioExcecutionState(String scenarioExcecutionState) {
		if (scenarioExcecutionState.trim().equalsIgnoreCase("True"))
			ScenarioExcecutionState = true;
		else if (scenarioExcecutionState.trim().equalsIgnoreCase("false"))
			ScenarioExcecutionState = false;
	}

	public String getPaymentType() {
		return PaymentType;
	}

	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}

	public String getCardType() {
		return CardType;
	}

	public void setCardType(String cardType) {
		CardType = cardType;
	}

	public String getOfflineMethod() {
		return OfflineMethod;
	}

	public void setOfflineMethod(String offlineMethod) {
		OfflineMethod = offlineMethod;
	}

	public String getPaymentRef() {
		return PaymentRef;
	}

	public void setPaymentRef(String paymentRef) {
		PaymentRef = paymentRef;
	}

    public String getSearchIndex() {
		return SearchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		SearchIndex = searchIndex;
	}

	public String getBookinDateType() {
		return BookinDateType;
	}

	public void setBookinDateType(String bookinDateType) {
		BookinDateType = bookinDateType;
	}

	public String getBookingDate() {
		return BookingDate;
	}

	public void setBookingDate(String bookingDate) {

		if (!bookingDate.trim().equalsIgnoreCase("N/A")) {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/yyyy");
			Date Checkin = null;

			try {
				Checkin = sdf.parse(bookingDate.trim());
				cal.setTime(Checkin);
				this.ArrivalDate = sdf2.format(cal.getTime());
				cal.add(Calendar.DATE, Integer.parseInt(this.DaysBetween));
				this.DepartureDate = sdf2.format(cal.getTime());
				Indate = Calender.getDate("dd-MMM-yyyy", "yyyy-MM-dd", DepartureDate)+"T00:00:00";
				OutDate = Calender.getDate("dd-MMM-yyyy", "yyyy-MM-dd", ArrivalDate)+"T00:00:00";
				System.out.println(Indate);
				System.out.println(OutDate);
			} catch (Exception e) {
				System.out.println("Pharse Error: " + bookingDate + " " + e.toString());
			}
		} else {
			BookingDate = "-";
		}

	}

	public String getAvailability() {
		return Availability;
	}

	public void setAvailability(String availability) {
		Availability = availability;
	}

	public String getTotalRate() {
		return TotalRate;
	}

	public void setTotalRate(String totalRate) {
		TotalRate = totalRate;
	}

	public void setHotelAvailability(boolean hotelAvailability) {
		HotelAvailability = hotelAvailability;
	}

	public boolean isHotelAvailability() {
		return HotelAvailability;
	}

	public void setHotelAvailability(String hotelAvailability) {
		if (hotelAvailability.equalsIgnoreCase("Available"))
			HotelAvailability = true;
		else
			HotelAvailability = false;
	}

	public String getCheckOutDate() {
		return CheckOutDate;
	}

	public void setSearchCurrency(String Currency) {
		SearchCurrency = Currency;
	}

	public String getSearchCurrency() {
		return SearchCurrency;
	}

	public String getCountryOFResidance() {
		return CountryOFResidance;
	}

	public void setCountryOFResidance(String countryOFResidance) {
		CountryOFResidance = countryOFResidance;
	}

	public auto.xmlout.com.types.BookingType getBookingType() {
		return BookingType;
	}

	public void setBookingType(String bookingType) throws ParseException {
		BookingType = auto.xmlout.com.types.BookingType.getBookingType(bookingType.trim());

		if (this.DepartureDate.equalsIgnoreCase("N/A")) {

			if (BookingType == auto.xmlout.com.types.BookingType.OUTSIDECANCELLATION) {
				DepartureDate = Calender.getDate(Calendar.MONTH, 1, "MM/dd/yyyy");

				if (this.Triptype == TripType.TWOWAY)
					ArrivalDate = Calender.getDate(1, Integer.parseInt(this.DaysBetween), "MM/dd/yyyy", "");
			} else {
				DepartureDate = Calender.getDate(Calendar.DATE, 1, "MM/dd/yyyy");
				if (this.Triptype == TripType.TWOWAY)
					ArrivalDate = Calender.getDate(Calendar.DATE, 1 + Integer.parseInt(this.DaysBetween), "MM/dd/yyyy");

			}

			Indate = Calender.getDate("dd-MMM-yyyy", "yyyy-MM-dd", DepartureDate)+"T00:00:00";
			
            if (this.Triptype == TripType.TWOWAY)
			OutDate = Calender.getDate("dd-MMM-yyyy", "yyyy-MM-dd", ArrivalDate)+"T00:00:00";
		}

	}

	public String[] getChildAges() {
		return ChildAges;
	}

	public void setChildAges(String[] childAges) {
		this.ChildAges = childAges;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getExpected() {
		return Expected;
	}

	public void setExpected(String expected) {
		Expected = expected;
	}

	public void setExpected(String expected, String Rate) {
		Double ExpectedValue = Double.parseDouble(expected) * Double.parseDouble(Rate);
		DecimalFormat df = new DecimalFormat("#.##");
		ExpectedValue = Double.valueOf(df.format(ExpectedValue));
		Expected = Double.toString(ExpectedValue);
	}

	public String getArrivalDate() {
		return ArrivalDate;
	}

	public void setArrivalDate(String arrivalDate) throws ParseException {
		ArrivalDate = arrivalDate;
		if (!(arrivalDate.equalsIgnoreCase("N/A")))
		OutDate = Calender.getDate("dd-MMM-yyyy", "yyyy-MM-dd", ArrivalDate)+"T00:00:00";
	}

}
