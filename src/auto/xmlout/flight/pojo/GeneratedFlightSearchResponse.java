package auto.xmlout.flight.pojo;

import java.util.ArrayList;

import auto.xmlout.hotel.pojo.XmlOutError;



public class GeneratedFlightSearchResponse {
	
private boolean   ResultsAvailable             = false;
private String    TransactionIdentifier        = null;
private int       NumberOfResults              = 0;
private boolean   ErrorResponse                = false;
private String    ErrorCode                    = "N/A";
private String    ErrorMessage                 = "N/A";
private String    ErrorLog                     = "N/A";
private ArrayList<XmlOutError>  Errors         = new ArrayList<XmlOutError>();
private ArrayList<PricedItinerary> ItenaryList = new ArrayList<PricedItinerary>();




public ArrayList<PricedItinerary> getItenaryList() {
	return ItenaryList;
}
public void setItenaryList(ArrayList<PricedItinerary> itenaryList) {
	ItenaryList = itenaryList;
}
public boolean isResultsAvailable() {
	return ResultsAvailable;
}
public void setResultsAvailable(boolean resultsAvailable) {
	ResultsAvailable = resultsAvailable;
}
public int getNumberOfResults() {
	return NumberOfResults;
}
public void setNumberOfResults(int numberOfResults) {
	NumberOfResults = numberOfResults;
}
public ArrayList<XmlOutError> getErrors() {
	return Errors;
}
public void addToErrors(XmlOutError error) {
	Errors.add(error);
}

public void setErrors(ArrayList<XmlOutError> errors) {
	Errors = errors;
}
public String getTransactionIdentifier() {
	return TransactionIdentifier;
}
public void setTransactionIdentifier(String stringLength1To128) {
	TransactionIdentifier = stringLength1To128;
}

public boolean isErrorResponse() {
	return ErrorResponse;
}
public void setErrorResponse(boolean errorResponse) {
	ErrorResponse = errorResponse;
}
public String getErrorCode() {
	return ErrorCode;
}
public void setErrorCode(String errorCode) {
	ErrorCode = errorCode;
}
public String getErrorMessage() {
	return ErrorMessage;
}
public void setErrorMessage(String errorMessage) {
	ErrorMessage = errorMessage;
}
public String getErrorLog() {
	return ErrorLog;
}
public void setErrorLog(String errorLog) {
	ErrorLog = errorLog;
}




}
