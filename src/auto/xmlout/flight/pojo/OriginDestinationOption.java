package auto.xmlout.flight.pojo;

import java.util.ArrayList;

public class OriginDestinationOption {

private int NumberOfSegments                 = 0;
private ArrayList<FlightSegment> SegmentList = null;
private String TotalJourneyDuration           = "";

public int getNumberOfSegments() {
	return NumberOfSegments;
}
public void setNumberOfSegments(int numberOfSegments) {
	NumberOfSegments = numberOfSegments;
}
public ArrayList<FlightSegment> getSegmentList() {
	return SegmentList;
}
public void setSegmentList(ArrayList<FlightSegment> segmentList) {
	SegmentList = segmentList;
}

public void addToSegmentList(FlightSegment segment) {
  if(SegmentList == null ){
	  SegmentList  = new ArrayList<FlightSegment>();
	  SegmentList.add(segment);
  }else{
	  SegmentList.add(segment); 
  }

}
public String getTotalJourneyDuration() {
	return TotalJourneyDuration;
}
public void setTotalJourneyDuration(String totalJourneyDuration) {
	TotalJourneyDuration = totalJourneyDuration;
}



}
