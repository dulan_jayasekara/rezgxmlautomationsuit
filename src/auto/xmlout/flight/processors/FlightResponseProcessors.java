package auto.xmlout.flight.processors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import auto.xmlout.com.types.ContractType;
import auto.xmlout.com.types.PricingSourceType;
import auto.xmlout.flight.pojo.FlightSegment;
import auto.xmlout.flight.pojo.GeneratedFlightSearchResponse;
import auto.xmlout.flight.pojo.OriginDestinationOption;
import auto.xmlout.flight.pojo.PricedItinerary;
import auto.xmlout.hotel.pojo.GeneratedAvailabilityResponse;
import auto.xmlout.hotel.pojo.GeneratedCancellationResponse;
import auto.xmlout.hotel.pojo.GeneratedHotelInfoResponse;
import auto.xmlout.hotel.pojo.GeneratedReservationResponse;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.hotel.pojo.RoomType;
import auto.xmlout.hotel.pojo.SearchType;
import auto.xmlout.hotel.pojo.XmlOutError;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Comment_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ErrorType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ErrorsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FlightSegment_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelResResponseTypeChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelStay_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirLowFareSearchRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirLowFareSearchRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelResRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OriginDestinationOption_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PricedItinerary_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResGlobalInfoType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStay_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UniqueID_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.VendorMessageType;

public class FlightResponseProcessors {

	public GeneratedFlightSearchResponse processFlightSearchResponse(OTA_AirLowFareSearchRS response) {
		GeneratedFlightSearchResponse ExtractedResponse = new GeneratedFlightSearchResponse();

		if (response == null) {
			ExtractedResponse.setResultsAvailable(false);
			return ExtractedResponse;
		}
		OTA_AirLowFareSearchRSChoice_type0 RequestType = response.getOTA_AirLowFareSearchRSChoice_type0();
		ErrorsType ErrorsList = RequestType.getErrors();

		if (ErrorsList != null) {
			ExtractedResponse.setResultsAvailable(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setResultsAvailable(true);
		}

		if (ExtractedResponse.isResultsAvailable()) {
			try {
				ExtractedResponse.setTransactionIdentifier(response.getTransactionIdentifier().getStringLength1To128());
				System.out.println("CorrelationID:" + response.getCorrelationID());
				System.out.println("EchoToken:" + response.getEchoToken());
				System.out.println("TransactionIdentifier:" + response.getTransactionIdentifier());
				int resultsSize = response.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary().length;
				PricedItinerary_type0[] ItenariesType = response.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary();
				for (int i = 0; i < resultsSize; i++) {
					PricedItinerary itenary = new PricedItinerary();
					PricedItinerary_type0 CurrentItenary = ItenariesType[i];
					itenary.setSequenceNumber(CurrentItenary.getSequenceNumber().toString());
					itenary.setSequenceNumber(CurrentItenary.getAirItinerary().getDirectionInd().getValue());
					itenary.setQuoteID(CurrentItenary.getAirItineraryPricingInfo().getQuoteID().getStringLength1To128());

					itenary.setPricingSource(PricingSourceType.getPricingSource(CurrentItenary.getAirItineraryPricingInfo().getPricingSource().getValue()));
					itenary.setBaseFare(CurrentItenary.getAirItineraryPricingInfo().getItinTotalFare()[0].getBaseFare().getAmount().toString());
					itenary.setCurrencyCode(CurrentItenary.getAirItineraryPricingInfo().getItinTotalFare()[0].getBaseFare().getCurrencyCode().getAlphaLength3());
					itenary.setDecimalPlaces(CurrentItenary.getAirItineraryPricingInfo().getItinTotalFare()[0].getBaseFare().getDecimalPlaces().toString());
					itenary.setTax(CurrentItenary.getAirItineraryPricingInfo().getItinTotalFare()[0].getTaxes().getAmount().toString());
					itenary.setAditionalFee(CurrentItenary.getAirItineraryPricingInfo().getItinTotalFare()[0].getFees().getFee()[0].getAmount().toString());
					itenary.setTotalFare(CurrentItenary.getAirItineraryPricingInfo().getItinTotalFare()[0].getTotalFare().getAmount().toString());
					itenary.setTicketTimeLimit(CurrentItenary.getTicketingInfo().getTicketTimeLimit());

					OriginDestinationOption OutboundOption = new OriginDestinationOption();
					OriginDestinationOption ArrivalOption = new OriginDestinationOption();
					OriginDestinationOption_type0[] InOutOptions = CurrentItenary.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption();

					FlightSegment_type0[] OutBoundSegments = InOutOptions[0].getFlightSegment();
					OutboundOption.setNumberOfSegments(OutBoundSegments.length);
					for (int j = 0; j < OutBoundSegments.length; j++) {
						FlightSegment_type0 flightSegmentType = OutBoundSegments[j];
						FlightSegment flight = getSegmentData(flightSegmentType);
						OutboundOption.addToSegmentList(flight);
					}
					itenary.setOutBoundOption(OutboundOption);

					if (InOutOptions.length > 1) {
						FlightSegment_type0[] InBoundSegments = InOutOptions[1].getFlightSegment();
						ArrivalOption.setNumberOfSegments(InBoundSegments.length);

						for (int j = 0; j < InBoundSegments.length; j++) {
							FlightSegment_type0 flightSegmentType = InBoundSegments[j];
							FlightSegment flight = getSegmentData(flightSegmentType);
							ArrivalOption.addToSegmentList(flight);
						}
						itenary.setInBoundOption(ArrivalOption);
					}

				}
			} catch (Exception e) {
			}

		}
		return ExtractedResponse;
	}

	public void fillAvailabilityDetails(SearchType search, Hotel hotel) {
		search.setHotelID(hotel.getHotelCode().trim());
		search.setHotelVendor(hotel.getHotelVendor());

		if (search.getHotelContract() == ContractType.INTERNAL) {
			java.util.TreeMap<String, String> ExpectedRooms = (java.util.TreeMap<String, String>) search.getSelectedRooms();

			Iterator<Map.Entry<String, String>> itr = ExpectedRooms.entrySet().iterator();
			while (itr.hasNext()) {
				Map.Entry<String, String> entry = itr.next();
				String RoomNum = entry.getKey();
				String RoomCode = entry.getValue();

				ArrayList<RoomType> AvailableRooms = hotel.getResultsRooms().get(RoomNum);

				for (Iterator<RoomType> iterator = AvailableRooms.iterator(); iterator.hasNext();) {
					RoomType roomType = (RoomType) iterator.next();
					String AvailableCode = roomType.getRoomType() + "/" + roomType.getBedType() + "/" + roomType.getRateType();

					if (RoomCode.trim().equalsIgnoreCase(AvailableCode)) {
						search.addToSelectedRoomCodes(RoomNum, roomType.getRoomCode().trim());
					}

				}
			}
		} else {

			Map<String, ArrayList<RoomType>> AvailableRooms = hotel.getResultsRooms();

			Iterator<Map.Entry<String, ArrayList<RoomType>>> itr = AvailableRooms.entrySet().iterator();
			while (itr.hasNext()) {
				Map.Entry<java.lang.String, java.util.ArrayList<auto.xmlout.hotel.pojo.RoomType>> entry = (Map.Entry<java.lang.String, java.util.ArrayList<auto.xmlout.hotel.pojo.RoomType>>) itr.next();
				String RoomNumber = entry.getKey().trim();
				int incr = (entry.getValue().size() == 1) ? 0 : new Random().nextInt(entry.getValue().size() - 1);
				String SelectedCode = entry.getValue().get(incr).getRoomCode();
				search.addToSelectedRoomCodes(RoomNumber, SelectedCode);

			}

		}

	}

	public GeneratedAvailabilityResponse processAvailabilityResponse(OTA_HotelAvailRS response) {
		GeneratedAvailabilityResponse ExtractedResponse = new GeneratedAvailabilityResponse();

		if (response == null) {
			ExtractedResponse.setHotelAvailable(false);
			return ExtractedResponse;
		}
		OTA_HotelAvailRSChoice_type0 RequestType = response.getOTA_HotelAvailRSChoice_type0();
		ErrorsType ErrorsList = RequestType.getErrors();
		HotelStay_type0 hotelStay_type0 = null;

		if (ErrorsList != null) {
			ExtractedResponse.setHotelAvailable(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setHotelAvailable(true);
		}

		// ================================================================================================//

		if (ExtractedResponse.isHotelAvailable()) {
			try {
				Hotel ResultHotel = new Hotel();
				ExtractedResponse.setTransactionIdentifier(response.getTransactionIdentifier().toString());
				hotelStay_type0 = response.getOTA_HotelAvailRSChoice_type0().getOTA_HotelAvailRSSequence_type0().getHotelStays().getHotelStay()[0];

				ResultHotel.setHotelCode(hotelStay_type0.getBasicPropertyInfo().getHotelCode().getStringLength1To16());
				ResultHotel.setHotelName(hotelStay_type0.getBasicPropertyInfo().getHotelName().getStringLength1To128());
				ResultHotel.setHotelVendor(hotelStay_type0.getBasicPropertyInfo().getBrandCode().getStringLength1To64());
				ResultHotel.setHotelAvailability(hotelStay_type0.getAvailability().toString());

				VendorMessageType[] VendorMessages = hotelStay_type0.getBasicPropertyInfo().getVendorMessages()[0].getVendorMessage();

				for (VendorMessageType vendorMessageType : VendorMessages) {
					String MessageType = vendorMessageType.getSubSection()[0].getSubTitle().toString();
					String Message = vendorMessageType.getSubSection()[0].getParagraph()[0].getParagraphTypeChoice()[0].getText().toString();

					if (MessageType.contains("Short"))
						ResultHotel.setShortDescription(Message);
					else if (MessageType.contains("long"))
						ResultHotel.setLongDescription(Message);
				}

				RoomStay_type0[] roomStay_type0s = response.getOTA_HotelAvailRSChoice_type0().getOTA_HotelAvailRSSequence_type0().getRoomStays().getRoomStay();

				for (int k = 0; k < roomStay_type0s.length; k++) {

					String RoomTypeCode = roomStay_type0s[k].getRoomTypes().getRoomType()[0].getRoomTypeCode().getStringLength1To64();
					String RoomType = roomStay_type0s[k].getRoomTypes().getRoomType()[0].getRoomType().getStringLength1To64();

					System.out.println("RoomTypeCode-->" + RoomTypeCode);
					System.out.println("RoomType-->" + RoomType);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

	public GeneratedReservationResponse processReservationResponse(OTA_HotelResRS response) {
		GeneratedReservationResponse ExtractedResponse = new GeneratedReservationResponse();

		if (response == null) {
			ExtractedResponse.setConfirmationSuccess(false);
			return ExtractedResponse;
		}
		HotelResResponseTypeChoice_type0 ResponseType = response.getOTA_HotelResRS().getHotelResResponseTypeChoice_type0();
		ErrorsType ErrorsList = ResponseType.getErrors();

		if (ErrorsList != null) {
			ExtractedResponse.setConfirmationSuccess(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setConfirmationSuccess(true);
		}

		// ================================================================================================//

		if (ExtractedResponse.isConfirmationSuccess()) {
			try {
				UniqueID_Type[] UniqueIDs = ResponseType.getHotelResResponseTypeSequence_type0().getHotelReservations().getHotelReservation()[0].getUniqueID();

				for (UniqueID_Type uniqueID_Type : UniqueIDs) {
					String Context = uniqueID_Type.getID_Context().getStringLength1To32();

					if (Context.equalsIgnoreCase("ReservationNo")) {
						ExtractedResponse.setConfirmationNumber(uniqueID_Type.getID().getStringLength1To64());
					} else if (Context.equalsIgnoreCase("ServiceID")) {
						ExtractedResponse.setServiceID(uniqueID_Type.getID().getStringLength1To64());

					}
				}

				ResGlobalInfoType GlobalInfo = ResponseType.getHotelResResponseTypeSequence_type0().getHotelReservations().getHotelReservation()[0].getResGlobalInfo();
				Comment_type1[] Comments = GlobalInfo.getComments().getComment();

				for (Comment_type1 comment_type1 : Comments) {
					String CommentCategory = comment_type1.getName().getStringLength1To64().trim();

					if (CommentCategory.equalsIgnoreCase("SupplierReferenceMessage")) {
						ExtractedResponse.setSupplierReferenceMessage(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("BookingConfirmationDetail")) {
						ExtractedResponse.setBookingConfirmationDetail(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("TripSummary")) {
						ExtractedResponse.setTripSummary(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("CustomerContactDetails")) {
						ExtractedResponse.setCustomerContactDetails(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("TripSummaryBookingStatus")) {
						ExtractedResponse.setTripSummaryBookingStatus(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("EstimatedArrivalTime")) {
						ExtractedResponse.setEstimatedArrivalTime(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("TripSummaryCheckIn")) {
						ExtractedResponse.setTripSummaryCheckIn(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("TripSummaryCheckOut")) {
						ExtractedResponse.setTripSummaryCheckOut(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					}
				}

				ExtractedResponse.setTotalRate(GlobalInfo.getTotal().getAmountAfterTax().toString());
				ExtractedResponse.setRateCurrency(GlobalInfo.getTotal().getCurrencyCode().getAlphaLength3());
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

	public GeneratedCancellationResponse processCancellationResponse(OTA_CancelRS response) {
		GeneratedCancellationResponse ExtractedResponse = new GeneratedCancellationResponse();

		if (response == null) {
			ExtractedResponse.setCancellationSuccess(false);
			return ExtractedResponse;
		}
		OTA_CancelRSChoice_type0 ResponseType = response.getOTA_CancelRSChoice_type0();
		ErrorsType ErrorsList = ResponseType.getErrors();

		if (ErrorsList != null) {
			ExtractedResponse.setCancellationSuccess(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setCancellationSuccess(true);
		}

		// ================================================================================================//

		if (ExtractedResponse.isCancellationSuccess()) {
			try {
				UniqueID_Type[] UniqueType = ResponseType.getOTA_CancelRSSequence_type0().getUniqueID();

				for (int i = 0; i < UniqueType.length; i++) {
					if (UniqueType[i].getID_Context().getStringLength1To32().equalsIgnoreCase("CancellationNo"))
						ExtractedResponse.setCancellationNumber(UniqueType[i].getID().getStringLength1To64());
					else if (UniqueType[i].getID_Context().getStringLength1To32().equalsIgnoreCase("ServiceID"))
						ExtractedResponse.setServiceID(UniqueType[i].getID().getStringLength1To64());
					else if (UniqueType[i].getID_Context().getStringLength1To32().equalsIgnoreCase("SupplierConfirmation"))
						ExtractedResponse.setSupplierConfirmation(UniqueType[i].getID().getStringLength1To64());
				}

				ExtractedResponse.setComment(ResponseType.getOTA_CancelRSSequence_type0().getComment().getParagraphTypeChoice()[0].getText().toString());
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

	public GeneratedHotelInfoResponse processHotelInfoResponse(OTA_HotelDescriptiveInfoRS response) {
		GeneratedHotelInfoResponse ExtractedResponse = new GeneratedHotelInfoResponse();

		if (response == null) {
			ExtractedResponse.setHotelInfoAvailable(false);
			return ExtractedResponse;
		}
		OTA_HotelDescriptiveInfoRSChoice_type0 ResponseType = response.getOTA_HotelDescriptiveInfoRSChoice_type0();
		ErrorsType ErrorsList = ResponseType.getErrors();

		if (ErrorsList != null) {
			ExtractedResponse.setHotelInfoAvailable(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setHotelInfoAvailable(true);
		}

		// ================================================================================================//

		if (ExtractedResponse.isHotelInfoAvailable()) {
			try {

				org.apache.axis2.databinding.types.URI urlz = ResponseType.getOTA_HotelDescriptiveInfoRSSequence_type0().getHotelDescriptiveContents().getHotelDescriptiveContent()[0].getMultimediaDescriptions().getMultimediaDescription()[0]
						.getImageItems().getImageItem()[0].getImageFormat()[0].getURL();
				String imagepath = urlz.getPath();
				System.out.println("imagepath befor decode-->" + imagepath);
				imagepath = java.net.URLDecoder.decode(imagepath, "UTF-8");
				System.out.println("imagepath after decode-->" + imagepath);

			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

	public FlightSegment getSegmentData(FlightSegment_type0 FlightSegmentType) {

		FlightSegment ReturnSegment = new FlightSegment();
		ReturnSegment.setArrivalDateTime(FlightSegmentType.getArrivalDateTime());
		ReturnSegment.setDepartureDateTime(FlightSegmentType.getDepartureDateTime());
		ReturnSegment.setTicketEligibility(FlightSegmentType.getArrivalDateTime());
		ReturnSegment.setFlightNumber(FlightSegmentType.getFlightNumber().toString());
		ReturnSegment.setNumberInParty(FlightSegmentType.getNumberInParty().toString());
		ReturnSegment.setRPH(FlightSegmentType.getRPH().getRPH_Type());
		ReturnSegment.setResBookDesigCode(FlightSegmentType.getResBookDesigCode().getUpperCaseAlphaLength1To2());
		ReturnSegment.setStopoverInd(FlightSegmentType.getStopoverInd());
		ReturnSegment.setDepartureLocationCode(FlightSegmentType.getDepartureAirport().getCodeContext().getStringLength1To32());
		ReturnSegment.setArrivalLocationCode(FlightSegmentType.getArrivalAirport().getCodeContext().getStringLength1To32());
		ReturnSegment.setOperatingAirline(FlightSegmentType.getOperatingAirline().getCode().getStringLength1To16());
		ReturnSegment.setOperatingAirlineCompany(FlightSegmentType.getOperatingAirline().getStringLength0To128());
		ReturnSegment.setAirEquipTypeCode(FlightSegmentType.getEquipment()[0].getAirEquipType().getStringLength3());
		ReturnSegment.setAirEquipTypeName(FlightSegmentType.getEquipment()[0].getStringLength0To64());
		ReturnSegment.setMarketingAirlineCode(FlightSegmentType.getMarketingAirline().getCode().getStringLength1To16());
		ReturnSegment.setMarketingAirlineName(FlightSegmentType.getMarketingAirline().getStringLength0To128());
		ReturnSegment.setJourneyTotalDuration(FlightSegmentType.getTPA_Extensions().getJourneyTotalDuration());
		ReturnSegment.setCabinType(FlightSegmentType.getTPA_Extensions().getCabinType());

		return ReturnSegment;
	}
}
