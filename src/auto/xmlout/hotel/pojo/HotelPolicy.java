package auto.xmlout.hotel.pojo;

import java.util.Date;

import auto.xmlout.com.types.ChargeByType;

public class HotelPolicy {
	
private String From = "";
private String To   = "";
private String ArrivalLessThan           = "";
private String CancellatrionBuffer       = "";
private ChargeByType StdChargeByType     = null;
private String       StdValue            = "";
private ChargeByType NoShowChargeByType  = null;
private String       NoshowValue         = "";
private Date         PolicyStartDate          ;
private Date         PolicyEndDate            ;
private boolean      PolicyApplicable    = true;



public boolean isPolicyApplicable() {
	return PolicyApplicable;
}
public void setPolicyApplicable(boolean policyApplicable) {
	PolicyApplicable = policyApplicable;
}
public Date getPolicyStartDate() {
	return PolicyStartDate;
}
public void setPolicyStartDate(Date policyStartDate) {
	PolicyStartDate = policyStartDate;
}
public Date getPolicyEndDate() {
	return PolicyEndDate;
}
public void setPolicyEndDate(Date policyEndDate) {
	PolicyEndDate = policyEndDate;
}
public String getFrom() {
	return From;
}
public void setFrom(String from) {
	From = from;
}
public String getTo() {
	return To;
}
public void setTo(String to) {
	To = to;
}
public String getArrivalLessThan() {
	return ArrivalLessThan;
}
public void setArrivalLessThan(String arrivalLessThan) {
	ArrivalLessThan = arrivalLessThan;
}
public String getCancellatrionBuffer() {
	return CancellatrionBuffer;
}
public void setCancellatrionBuffer(String cancellatrionBuffer) {
	CancellatrionBuffer = cancellatrionBuffer;
}
public ChargeByType getStdChargeByType() {
	return StdChargeByType;
}
public void setStdChargeByType(ChargeByType stdChargeByType) {
	StdChargeByType = stdChargeByType;
}
public String getStdValue() {
	return StdValue;
}
public void setStdValue(String stdValue) {
	StdValue = stdValue;
}
public ChargeByType getNoShowChargeByType() {
	return NoShowChargeByType;
}
public void setNoShowChargeByType(ChargeByType noShowChargeByType) {
	NoShowChargeByType = noShowChargeByType;
}
public String getNoshowValue() {
	return NoshowValue;
}
public void setNoshowValue(String noshowValue) {
	NoshowValue = noshowValue;
}




}
