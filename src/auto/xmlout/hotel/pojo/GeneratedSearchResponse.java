package auto.xmlout.hotel.pojo;

import java.util.ArrayList;
import java.util.HashMap;



public class GeneratedSearchResponse {
	
private boolean ResultsAvailable      = false;
private String  TransactionIdentifier = null;
private int  NumberOfHotels        = 0;
private boolean   ErrorResponse    = false;
private String    ErrorCode        = "N/A";
private String    ErrorMessage     = "N/A";
private String    ErrorLog         = "N/A";
private ArrayList<XmlOutError>  Errors = new ArrayList<XmlOutError>();
private ArrayList<Hotel> ResponseHotelList = new ArrayList<Hotel>();
private HashMap<String,ArrayList<Hotel>> VendorHotelMap = new HashMap<String, ArrayList<Hotel>>();




public ArrayList<XmlOutError> getErrors() {
	return Errors;
}
public void addToErrors(XmlOutError error) {
	Errors.add(error);
}

public void addToVendorHotelMap(String Vendor,Hotel hotel){
   try {
	VendorHotelMap.get(Vendor).add(hotel);
} catch (Exception e) {
	VendorHotelMap.put(Vendor, new ArrayList<Hotel>());
	VendorHotelMap.get(Vendor).add(hotel);
}	
}



public HashMap<String, ArrayList<Hotel>> getVendorHotelMap() {
	return VendorHotelMap;
}
public void setVendorHotelMap(HashMap<String, ArrayList<Hotel>> vendorHotelMap) {
	VendorHotelMap = vendorHotelMap;
}
public ArrayList<Hotel> getResponseHotelList() {
	return ResponseHotelList;
}
public void setResponseHotelList(ArrayList<Hotel> responseHotelList) {
	ResponseHotelList = responseHotelList;
}

public void addToHotelList(Hotel hotel) {
	ResponseHotelList.add(hotel);
}
public boolean isResultsAvailable() {
	return ResultsAvailable;
}
public void setResultsAvailable(boolean resultsAvailable) {
	ResultsAvailable = resultsAvailable;
}
public String getTransactionIdentifier() {
	return TransactionIdentifier;
}
public void setTransactionIdentifier(String stringLength1To128) {
	TransactionIdentifier = stringLength1To128;
}
public int getNumberOfHotels() {
	return NumberOfHotels;
}
public void setNumberOfHotels(int numberOfHotels) {
	NumberOfHotels = numberOfHotels;
}
public boolean isErrorResponse() {
	return ErrorResponse;
}
public void setErrorResponse(boolean errorResponse) {
	ErrorResponse = errorResponse;
}
public String getErrorCode() {
	return ErrorCode;
}
public void setErrorCode(String errorCode) {
	ErrorCode = errorCode;
}
public String getErrorMessage() {
	return ErrorMessage;
}
public void setErrorMessage(String errorMessage) {
	ErrorMessage = errorMessage;
}
public String getErrorLog() {
	return ErrorLog;
}
public void setErrorLog(String errorLog) {
	ErrorLog = errorLog;
}




}
