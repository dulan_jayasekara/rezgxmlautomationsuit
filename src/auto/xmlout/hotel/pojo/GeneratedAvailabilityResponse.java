package auto.xmlout.hotel.pojo;

import java.util.ArrayList;



public class GeneratedAvailabilityResponse {
	
private boolean   HotelAvailable      = false;
private String  TransactionIdentifier = null;
private int  NumberOfHotels        = 0;
private boolean   ErrorResponse    = false;
private String    ErrorCode        = "N/A";
private String    ErrorMessage     = "N/A";
private String    ErrorLog         = "N/A";
private ArrayList<XmlOutError>  Errors       = new ArrayList<XmlOutError>();
private Hotel  AvailableHotel      = null;


public ArrayList<XmlOutError> getErrors() {
	return Errors;
}
public void addToErrors(XmlOutError error) {
	Errors.add(error);
}

public boolean isHotelAvailable() {
	return HotelAvailable;
}
public void setHotelAvailable(boolean hotelAvailable) {
	HotelAvailable = hotelAvailable;
}
public Hotel getAvailableHotel() {
	return AvailableHotel;
}
public void setAvailableHotel(Hotel availableHotel) {
	AvailableHotel = availableHotel;
}
public void setErrors(ArrayList<XmlOutError> errors) {
	Errors = errors;
}
public String getTransactionIdentifier() {
	return TransactionIdentifier;
}
public void setTransactionIdentifier(String stringLength1To128) {
	TransactionIdentifier = stringLength1To128;
}
public int getNumberOfHotels() {
	return NumberOfHotels;
}
public void setNumberOfHotels(int numberOfHotels) {
	NumberOfHotels = numberOfHotels;
}
public boolean isErrorResponse() {
	return ErrorResponse;
}
public void setErrorResponse(boolean errorResponse) {
	ErrorResponse = errorResponse;
}
public String getErrorCode() {
	return ErrorCode;
}
public void setErrorCode(String errorCode) {
	ErrorCode = errorCode;
}
public String getErrorMessage() {
	return ErrorMessage;
}
public void setErrorMessage(String errorMessage) {
	ErrorMessage = errorMessage;
}
public String getErrorLog() {
	return ErrorLog;
}
public void setErrorLog(String errorLog) {
	ErrorLog = errorLog;
}




}
