package auto.xmlout.hotel.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class ResultsType {
	
 
    boolean   RP_Can_PopAvailable                    = false;
	private   boolean      RoomFound                 = false;
	private   boolean      HotelFound                = false; 
	private   boolean      AvailabilityStatus        = false; 
	private   boolean      SearchPageLoaded          = false; 
    private   int          NumberOfHotels;
	private   int          TotalPages;
	private   String      ResultsRate           = "";
	private   String      AdjustedRate          = "";
    private   String      CurrencyType          = ""; 
	private   String      ErrorMessage          = "";
    private   String      RoomCurrency          = ""; 
	private   String      CancellationDeadLine  = ""; 

	private   String      HiddenId              = "";
	private   String      StarRating            = "";
	private   String      DefaultRate           = "NA";
	private   String      HotelName             = "";
	private   String      HotelAddress          = "";
	private   String      HotelStarRating       = "";
	private   boolean     FeaturedProperty      = false ;
	private   boolean     BestRate              = false ;
	private   boolean     DealOfTheDay          = false ;
	private   String      HotelStatus           = "";
	private   String      HotelCurrency         = "";
	private   String      TotalRate             = "";
	private   String      NumberOfResultsRooms  = "";
	private   boolean     isIntermediateFrameLoaded = false;
	private   String IntermediateStatus         = "";
	private   String IntermediateMessage        = "";
	private   boolean     isMoreRoomButtonAvailable             = false;
	private   boolean     isHideRoomButtonAvailable             = false;
    private   int         ItemsPerPage                          = 0;
    
    private Map<String, ArrayList<ResultsRoom>>   ActualResultsRoomMap                       = new HashMap<String, ArrayList<ResultsRoom>>();
	private   Map<String, String>   DefaultRooms                                             = new TreeMap<String, String>();

    private   CartDetails        ResultCart                  = null;

    private   CancellationPolicy ResultsPageCancellation     = null;

    
    
    
  


	public boolean isDealOfTheDay() {
		return DealOfTheDay;
	}

	public void setDealOfTheDay(boolean dealOfTheDay) {
		DealOfTheDay = dealOfTheDay;
	}

	public void setActualResultsRoomMap(Map<String, ArrayList<ResultsRoom>> actualResultsRoomMap) {
		ActualResultsRoomMap = actualResultsRoomMap;
	}

	public int getItemsPerPage() {
		return ItemsPerPage;
	}

	public void setItemsPerPage(int itemsPerPage) {
		ItemsPerPage = itemsPerPage;
	}

	public boolean isMoreRoomButtonAvailable() {
		return isMoreRoomButtonAvailable;
	}

	public void setMoreRoomButtonAvailable(boolean isMoreRoomButtonAvailable) {
		this.isMoreRoomButtonAvailable = isMoreRoomButtonAvailable;
	}

	public boolean isHideRoomButtonAvailable() {
		return isHideRoomButtonAvailable;
	}

	public void setHideRoomButtonAvailable(boolean isHideRoomButtonAvailable) {
		this.isHideRoomButtonAvailable = isHideRoomButtonAvailable;
	}

	public boolean isBestRate() {
		return BestRate;
	}

	public void setBestRate(boolean bestRate) {
		BestRate = bestRate;
	}

	public String getAdjustedRate() {
		return AdjustedRate;
	}

	public void setAdjustedRate(String adjustedRate) {
		AdjustedRate = adjustedRate;
	}

	public boolean isIntermediateFrameLoaded() {
		return isIntermediateFrameLoaded;
	}

	public void setIntermediateFrameLoaded(boolean isIntermediateFrameLoaded) {
		this.isIntermediateFrameLoaded = isIntermediateFrameLoaded;
	}

	public boolean isSearchPageLoaded() {
		return SearchPageLoaded;
	}

	public void setSearchPageLoaded(boolean searchPageLoaded) {
		SearchPageLoaded = searchPageLoaded;
	}

   public String getIntermediateStatus() {
		return IntermediateStatus;
	}

	public void setIntermediateStatus(String intermediateStatus) {
		IntermediateStatus = intermediateStatus;
	}

	public String getIntermediateMessage() {
		return IntermediateMessage;
	}

	public void setIntermediateMessage(String intermediateMessage) {
		IntermediateMessage = intermediateMessage;
	}


	public String getHiddenId() {
		return HiddenId;
	}

	public void setHiddenId(String hiddenId) {
		HiddenId = hiddenId;
	}

	public CancellationPolicy getResultsPageCancellation() {
		return ResultsPageCancellation;
	}

	public void setResultsPageCancellation(
			CancellationPolicy resultsPageCancellation) {
		ResultsPageCancellation = resultsPageCancellation;
	}

	public boolean isRP_Can_PopAvailable() {
		return RP_Can_PopAvailable;
	}

	public void setRP_Can_PopAvailable(boolean rP_Can_PopAvailable) {
		RP_Can_PopAvailable = rP_Can_PopAvailable;
	}

	public void setDefaultRooms(Map<String, String> defaultRooms) {
		DefaultRooms = defaultRooms;
	}

	public void addToActualMap(String room,ArrayList<ResultsRoom> rooms)  
    {
    	ActualResultsRoomMap.put(room, rooms);
    }
    
    public Map<String, ArrayList<ResultsRoom>>  getActualResultsRoomMap()
    {
    	return ActualResultsRoomMap;
    }
	
	public CartDetails getResultCart() {
		return ResultCart;
	}


	public void setResultCart(CartDetails resultCart) {
		ResultCart = resultCart;
	}



	public String getHotelName() {
		return HotelName;
	}


	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}


	public String getHotelAddress() {
		return HotelAddress;
	}


	public void setHotelAddress(String hotelAddress) {
		HotelAddress = hotelAddress;
	}


	public String getHotelStarRating() {
		return HotelStarRating;
	}


	public void setHotelStarRating(String hotelStarRating) {
		HotelStarRating = hotelStarRating;
	}


	public boolean getFeaturedProperty() {
		return FeaturedProperty;
	}


	public void setFeaturedProperty(boolean featuredProperty) {
		FeaturedProperty = featuredProperty;
	}


	public String getHotelStatus() {
		return HotelStatus;
	}


	public void setHotelStatus(String hotelStatus) {
		HotelStatus = hotelStatus;
	}


	public String getHotelCurrency() {
		return HotelCurrency;
	}


	public void setHotelCurrency(String hotelCurrency) {
		HotelCurrency = hotelCurrency;
	}


	public String getTotalRate() {
		return TotalRate;
	}


	public void setTotalRate(String totalRate) {
		TotalRate = totalRate;
	}


	public String getNumberOfResultsRooms() {
		return NumberOfResultsRooms;
	}


	public void setNumberOfResultsRooms(String numberOfResultsRooms) {
		NumberOfResultsRooms = numberOfResultsRooms;
	}


	public void setTotalPages(int totalPages) {
		TotalPages = totalPages;
	}


	public void setRoomCurrency(String roomCurrency) {
		RoomCurrency = roomCurrency;
	}


	public int getResultsPages() {
		if( NumberOfHotels % 10 == 0)
			return NumberOfHotels/10;
		else
			return (NumberOfHotels/10)+1;
	}

	
	public TreeMap<String, String> getDefaultRooms() {
		return (TreeMap<String, String>) DefaultRooms;
	}

	public void addToDefaultRooms(String defaultRoom,String Rate ) {
		DefaultRooms.put(defaultRoom, Rate);
	}

	public String getDefaultRate() {
		return DefaultRate;
	}

	public void setDefaultRate(String defaultRate) {
		DefaultRate = defaultRate;
	}

	public String getStarRating() {
		return StarRating;
	}

	public void setStarRating(String starRating) {
		StarRating = starRating;
	}

    public String getCancellationDeadLine() {
		return CancellationDeadLine;
	}

	public void setCancellationDeadLine(String cancellationDeadLine) {
		CancellationDeadLine = cancellationDeadLine;
	}

	public boolean isRoomFound() {
		return RoomFound;
	}

	public void setRoomFound(boolean roomFound) {
		RoomFound = roomFound;
	}


	public int getNumberOfHotels() {
		return NumberOfHotels;
	}

	public void setNumberOfHotels(int numberOfHotels) {
		NumberOfHotels = numberOfHotels;
		TotalPages = NumberOfHotels/10 ;
		if(!(NumberOfHotels%10 == 0)) TotalPages ++;
	}

	public String getCurrencyType() {
		return CurrencyType;
	}

	public void setCurrencyType(String currencyType) {
		CurrencyType = currencyType;
	}

	public boolean isAvailabilityStatus() {
		return AvailabilityStatus;
	}

	public void setAvailabilityStatus(boolean availabilityStatus) {
		AvailabilityStatus = availabilityStatus;
	}
	

	public boolean isHotelFound() {
		return HotelFound;
	}
	
	public void setHotelFound(boolean hotelFound) {
		HotelFound = hotelFound;
	}
	
	public String getResultsRate() {
		return ResultsRate;
	}
	
	public void setResultsRate(String resultsRate) {
		ResultsRate  = resultsRate.split("  ")[1].replace(",","");
		RoomCurrency = resultsRate.split(" ")[0];
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}
	
	public int getTotalPages() {
		return TotalPages;
	}
	public String getRoomCurrency() {
		return RoomCurrency;
	}
	
	

}
