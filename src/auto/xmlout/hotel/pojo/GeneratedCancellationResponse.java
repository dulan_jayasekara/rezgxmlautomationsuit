package auto.xmlout.hotel.pojo;

import java.util.ArrayList;

public class GeneratedCancellationResponse {

	private boolean CancellationSuccess   = false;
	private String ServiceID              = null;
	private String TransactionIdentifier  = null;
	private ArrayList<XmlOutError> Errors = new ArrayList<XmlOutError>();
	private String CancellationNumber     = null;
	private String Comment                = null;
	private String SupplierConfirmation   = null;

	

	public String getSupplierConfirmation() {
		return SupplierConfirmation;
	}


	public void setSupplierConfirmation(String supplierConfirmation) {
		SupplierConfirmation = supplierConfirmation;
	}


	public String getServiceID() {
		return ServiceID;
	}


	public void setServiceID(String serviceID) {
		ServiceID = serviceID;
	}


	public String getComment() {
		return Comment;
	}


	public void setComment(String comment) {
		Comment = comment;
	}


	public boolean isCancellationSuccess() {
		return CancellationSuccess;
	}


	public void setCancellationSuccess(boolean cancellationSuccess) {
		CancellationSuccess = cancellationSuccess;
	}


	public String getCancellationNumber() {
		return CancellationNumber;
	}


	public void setCancellationNumber(String cancellationNumber) {
		CancellationNumber = cancellationNumber;
	}


	public String getTransactionIdentifier() {
		return TransactionIdentifier;
	}


	public void addToErrors(XmlOutError error) {
		Errors.add(error);
	}


	public void setTransactionIdentifier(String transactionIdentifier) {
		TransactionIdentifier = transactionIdentifier;
	}

	public ArrayList<XmlOutError> getErrors() {
		return Errors;
	}

	public void setErrors(ArrayList<XmlOutError> errors) {
		Errors = errors;
	}



}
