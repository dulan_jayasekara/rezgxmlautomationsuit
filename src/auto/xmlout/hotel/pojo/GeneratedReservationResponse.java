package auto.xmlout.hotel.pojo;

import java.util.ArrayList;

public class GeneratedReservationResponse {

	private boolean ConfirmationSuccess = false;
	private String TransactionIdentifier = null;
	private ArrayList<XmlOutError> Errors = new ArrayList<XmlOutError>();
	private Hotel AvailableHotel = null;
	private String ConfirmationNumber = null;
	private String ServiceID = null;
	private String SupplierReferenceMessage = null;
	private String BookingConfirmationDetail = null;
	private String TripSummary = null;
	private String CustomerContactDetails = null;
	private String TripSummaryBookingStatus = null;
	private String EstimatedArrivalTime = null;
	private String TripSummaryCheckIn = null;
	private String TripSummaryCheckOut = null;
	private String TotalRate = null;
	private String RateCurrency = null;

	public String getSupplierReferenceMessage() {
		return SupplierReferenceMessage;
	}

	public void setSupplierReferenceMessage(String supplierReferenceMessage) {
		SupplierReferenceMessage = supplierReferenceMessage;
	}

	public String getBookingConfirmationDetail() {
		return BookingConfirmationDetail;
	}

	public void setBookingConfirmationDetail(String bookingConfirmationDetail) {
		BookingConfirmationDetail = bookingConfirmationDetail;
	}

	public String getTripSummary() {
		return TripSummary;
	}

	public void setTripSummary(String tripSummary) {
		TripSummary = tripSummary;
	}

	public String getCustomerContactDetails() {
		return CustomerContactDetails;
	}

	public void setCustomerContactDetails(String customerContactDetails) {
		CustomerContactDetails = customerContactDetails;
	}

	public String getTripSummaryBookingStatus() {
		return TripSummaryBookingStatus;
	}

	public void setTripSummaryBookingStatus(String tripSummaryBookingStatus) {
		TripSummaryBookingStatus = tripSummaryBookingStatus;
	}

	public String getEstimatedArrivalTime() {
		return EstimatedArrivalTime;
	}

	public void setEstimatedArrivalTime(String estimatedArrivalTime) {
		EstimatedArrivalTime = estimatedArrivalTime;
	}

	public String getTripSummaryCheckIn() {
		return TripSummaryCheckIn;
	}

	public void setTripSummaryCheckIn(String tripSummaryCheckIn) {
		TripSummaryCheckIn = tripSummaryCheckIn;
	}

	public String getTripSummaryCheckOut() {
		return TripSummaryCheckOut;
	}

	public void setTripSummaryCheckOut(String tripSummaryCheckOut) {
		TripSummaryCheckOut = tripSummaryCheckOut;
	}

	public String getTotalRate() {
		return TotalRate;
	}

	public void setTotalRate(String totalRate) {
		TotalRate = totalRate;
	}

	public String getRateCurrency() {
		return RateCurrency;
	}

	public void setRateCurrency(String rateCurrency) {
		RateCurrency = rateCurrency;
	}

	public String getServiceID() {
		return ServiceID;
	}

	public void setServiceID(String serviceID) {
		ServiceID = serviceID;
	}

	public void addToErrors(XmlOutError error) {
		Errors.add(error);
	}

	public boolean isConfirmationSuccess() {
		return ConfirmationSuccess;
	}

	public void setConfirmationSuccess(boolean confirmationSuccess) {
		ConfirmationSuccess = confirmationSuccess;
	}

	public String getTransactionIdentifier() {
		return TransactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		TransactionIdentifier = transactionIdentifier;
	}

	public ArrayList<XmlOutError> getErrors() {
		return Errors;
	}

	public void setErrors(ArrayList<XmlOutError> errors) {
		Errors = errors;
	}

	public Hotel getAvailableHotel() {
		return AvailableHotel;
	}

	public void setAvailableHotel(Hotel availableHotel) {
		AvailableHotel = availableHotel;
	}

	public String getConfirmationNumber() {
		return ConfirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		ConfirmationNumber = confirmationNumber;
	}

}
