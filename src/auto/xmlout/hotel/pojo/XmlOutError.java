package auto.xmlout.hotel.pojo;

public class XmlOutError {
	
private String ErrorCode        = "Null";
private String ErrorDescription = "Null";

public String getErrorCode() {
	return ErrorCode;
}
public void setErrorCode(String errorCode) {
	ErrorCode = errorCode;
}
public String getErrorDescription() {
	return ErrorDescription;
}
public void setErrorDescription(String errorDescription) {
	ErrorDescription = errorDescription;
}



}
