package auto.xmlout.hotel.pojo;

import java.util.ArrayList;



public class GeneratedHotelInfoResponse {
	
private boolean   HotelInfoAvailable         = false;
private String  TransactionIdentifier        = null;
private ArrayList<XmlOutError>  Errors       = new ArrayList<XmlOutError>();
private String DetailedDescription           = null;
private String[] ImagePathList               = null;


public ArrayList<XmlOutError> getErrors() {
	return Errors;
}
public void addToErrors(XmlOutError error) {
	Errors.add(error);
}

public void setErrors(ArrayList<XmlOutError> errors) {
	Errors = errors;
}
public String getTransactionIdentifier() {
	return TransactionIdentifier;
}
public void setTransactionIdentifier(String stringLength1To128) {
	TransactionIdentifier = stringLength1To128;
}
public boolean isHotelInfoAvailable() {
	return HotelInfoAvailable;
}
public void setHotelInfoAvailable(boolean hotelInfoAvailable) {
	HotelInfoAvailable = hotelInfoAvailable;
}
public String getDetailedDescription() {
	return DetailedDescription;
}
public void setDetailedDescription(String detailedDescription) {
	DetailedDescription = detailedDescription;
}
public String[] getImagePathList() {
	return ImagePathList;
}
public void setImagePathList(String[] imagePathList) {
	ImagePathList = imagePathList;
}



}
