package auto.xmlout.hotel.reqbuilders;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.databinding.types.NonNegativeInteger;
import org.apache.axis2.databinding.types.PositiveInteger;
import org.apache.log4j.Logger;

import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Address_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AlphaLength3;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AlternateAvailability_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AvailRequestSegment_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AvailRequestSegments_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.BasicPropertyInfoType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.BookingChannel_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CodeContext_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CompanyNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CountryNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Criterion_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CustomerType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DateTimeSpanType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DropOff_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.EmailType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FilterResultsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Filter_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FreeTextType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuaranteeAccepted_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuaranteeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuaranteesAccepted_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuestCountType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuestCount_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelDescriptiveInfo_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelDescriptiveInfos_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelRef_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelResRequestType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelResRequestTypeSequence_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelReservationType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelReservationsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelSearchCriteriaType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ISO3166;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.MMYYDate;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Numeric0To999;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.NumericStringLength1To19;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.NumericStringLength1To8;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelResRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.POS_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentCardCodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentCardType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentTransactionTypeCode_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PersonNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PickUp_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ProfileInfo_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ProfileType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ProfilesType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Reasons_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RefPoint_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RequestorID_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResGlobalInfoType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResGuest_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResGuestsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ReservationDetails_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ReturnResultsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStayCandidateType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStayCandidates_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStay_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStaysType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomTypeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomTypes_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortBy_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortOrder_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortResults_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SourceType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StateProvCodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StateProvType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To128;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To16;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To255;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To32;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To64;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To8;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TPA_ExtensionsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Telephone_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TimeSpan_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UniqueID_type0;
import auto.xmlout.com.types.HotelXmlSearchByType;
import auto.xmlout.com.types.ServerType;
import auto.xmlout.common.configure.GlobalConfigurator;
import auto.xmlout.hotel.loaders.PropertySingleton;
import auto.xmlout.hotel.pojo.SearchType;


public class HotelRequestBuilder {

//private HashMap<String, String> propertyList   = null;
private Logger                  logger         = null;

public HotelRequestBuilder(){
	logger        = Logger.getLogger(this.getClass());
	//propertyList  = prop;
}
	
public OTA_HotelSearchRQ buildHotelSearchRequest(SearchType search) throws Exception{
  
	logger.info("======================== Hotel Search Request Build Started ======================");
	OTA_HotelSearchRQ SearchRQ = new OTA_HotelSearchRQ();
    SearchRQ.setTarget(search.getHotel_search_Target());
    Calendar calendar = Calendar.getInstance();
	SearchRQ.setTimeStamp(calendar);

	// For Single Hotel Search 
	StringLength1To128 stringLength1To128 = new StringLength1To128();
	stringLength1To128.setStringLength1To128(GlobalConfigurator.getCacheId());
	logger.info("Hotel Cache---->"+GlobalConfigurator.HOTEL_SEARCH_CACHE);
	logger.info("Checking Whether Single Hotel Search---->");
	SearchRQ.setTransactionIdentifier(stringLength1To128);
	if (search.isSingleHotelSearch()){
		logger.info("Single Hotel Search----> "+true);
		SearchRQ.setTransactionIdentifier(stringLength1To128);
	}else{
		logger.info("Single Hotel Search----> "+false);
	}
	POS_Type pos = new POS_Type();
	SourceType[] sourceType = new SourceType[1];
	sourceType[0] = new SourceType();

	RequestorID_type0 requestorID_type0 = new RequestorID_type0();
	StringLength1To64 st132 = new StringLength1To64();
	logger.info("Agent ID ----> "+search.getAgentID());
	st132.setStringLength1To64(search.getAgentID());
	requestorID_type0.setID(st132);

	OTA_CodeType codeType = new OTA_CodeType();
	codeType.setOTA_CodeType("5");
	requestorID_type0.setType(codeType);
	sourceType[0].setRequestorID(requestorID_type0);

	ISO3166 iso3166 = new ISO3166();
	logger.info("Country of residance ----> "+search.getCountryOFResidance());
	iso3166.setISO3166(search.getCountryOFResidance());
	sourceType[0].setISOCountry(iso3166);

	AlphaLength3 alphaLength3 = new AlphaLength3();
	logger.info("Search Currency ----> "+search.getSearchCurrency());
	alphaLength3.setAlphaLength3(search.getSearchCurrency());
	sourceType[0].setISOCurrency(alphaLength3);
	pos.setSource(sourceType);
	SearchRQ.setPOS(pos);

	StringLength1To128 stringLength1To12811 = new StringLength1To128();
	logger.info("Echo Token ----> "+PropertySingleton.getInstance().getProperty("Xmlout.EchoToken"));
	stringLength1To12811.setStringLength1To128(PropertySingleton.getInstance().getProperty("Xmlout.EchoToken"));
	SearchRQ.setEchoToken(stringLength1To12811);

	logger.info("MaxResponses ----> "+PropertySingleton.getInstance().getProperty("Xmlout.MaxResponses"));
	PositiveInteger positiveInteger = new PositiveInteger(PropertySingleton.getInstance().getProperty("Xmlout.MaxResponses"));
	SearchRQ.setMaxResponses(positiveInteger);

	logger.info("MinResponses ----> "+PropertySingleton.getInstance().getProperty("Xmlout.MinResponses"));
	NonNegativeInteger nonNegativeInteger = new NonNegativeInteger(PropertySingleton.getInstance().getProperty("Xmlout.MinResponses"));
	SearchRQ.setSequenceNmbr(nonNegativeInteger);

	HotelSearchCriteriaType hotelSearchCriteriaType = new HotelSearchCriteriaType();

	Criterion_type0[] criterion_type0 = new Criterion_type0[1];
	criterion_type0[0] = new Criterion_type0();

	RefPoint_type0[] refPoint_type0 = new RefPoint_type0[1];
	refPoint_type0[0] = new RefPoint_type0();
	StringLength1To8 stringLength1To8 = new StringLength1To8();

	if(search.getSerachBy() == HotelXmlSearchByType.AIRPORTCODE){
		logger.info("Search By Airport Code ----> "+ search.getAirPortCode());
		stringLength1To8.setStringLength1To8(search.getAirPortCode());
		refPoint_type0[0].setCodeContext(CodeContext_type0.airport);	
	}else if(search.getSerachBy() == HotelXmlSearchByType.CITYCODE){
		logger.info("Search By City Code ----> "+ search.getCityCode());
		stringLength1To8.setStringLength1To8(search.getCityCode());
	}else{
		
	}

    refPoint_type0[0].setCode(stringLength1To8);
	criterion_type0[0].setRefPoint(refPoint_type0);
	criterion_type0[0].setAlternateAvailability(AlternateAvailability_type0.Always);

	DateTimeSpanType dateTimeSpanType = new DateTimeSpanType();

	dateTimeSpanType.setStart(search.getIndate());
	dateTimeSpanType.setEnd(search.getOutDate());

	criterion_type0[0].setStayDateRange(dateTimeSpanType);

	int  noOfRooms = Integer.parseInt(search.getRooms());
	RoomStayCandidates_type0 roomStayCandidates_type0 = new RoomStayCandidates_type0();
	RoomStayCandidateType[] roomStayCandidateType = new RoomStayCandidateType[noOfRooms];
   
	String[] noOfAdults                   = search.getAdults(); 
    String[] noOfChildren                 = search.getChildren();
    Map<Integer, String[]> ChildAges      = search.getChildAges();
    
	for (int i = 0; i < noOfRooms; i++) {

		int RoomAdults                = Integer.parseInt(noOfAdults[i]);
		int RoomChildren              = Integer.parseInt(noOfChildren[i]);
		roomStayCandidateType[i]      = new RoomStayCandidateType();
		GuestCountType guestCountType = new GuestCountType();

		GuestCount_type0[] guestCount_type0 = new GuestCount_type0[RoomAdults + RoomChildren];

		int roomPassCount;

		for (roomPassCount = 0; roomPassCount < RoomAdults; roomPassCount++) {

			guestCount_type0[roomPassCount] = new GuestCount_type0();
			OTA_CodeType oTA_CodeType = new OTA_CodeType();
			oTA_CodeType.setOTA_CodeType("10");
			guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType);

		}

		String[] RoomChildAges    = ChildAges.get(i+1);
		for (int childCount = 0; childCount < RoomChildren; childCount++, roomPassCount++) {

			guestCount_type0[roomPassCount] = new GuestCount_type0();

			OTA_CodeType oTA_CodeType22 = new OTA_CodeType();
			oTA_CodeType22.setOTA_CodeType("8");

			Numeric0To999 numeric0To9993 = new Numeric0To999();
			BigInteger b3 = new BigInteger(RoomChildAges[childCount]);
			numeric0To9993.setNumeric0To999(b3);
			guestCount_type0[roomPassCount].setAge(numeric0To9993);
			guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType22);

		}

		guestCountType.setGuestCount(guestCount_type0);
		roomStayCandidateType[i].setGuestCounts(guestCountType);

	}

	roomStayCandidates_type0.setRoomStayCandidate(roomStayCandidateType);

	criterion_type0[0].setRoomStayCandidates(roomStayCandidates_type0);

	/*
	 * Award_type0[] award_type0 = new Award_type0[1]; award_type0[0] = new Award_type0(); award_type0[0].setProvider("RezG"); award_type0[0].setRating("5EST");
	 * criterion_type0[0].setAward(award_type0);
	 */

	if (search.isSingleHotelSearch()) {

		HotelRef_type0 hotelRef_type0 = new HotelRef_type0();

		StringLength1To8 stringLength1To82 = new StringLength1To8();
		stringLength1To82.setStringLength1To8(search.getCityCode().trim());
		hotelRef_type0.setHotelCityCode(stringLength1To82);
		StringLength1To16 stringLength1To16 = new StringLength1To16();
		stringLength1To16.setStringLength1To16(search.getHotelID().trim());
		hotelRef_type0.setHotelCode(stringLength1To16);
		StringLength1To64 stringLength1To64 = new StringLength1To64();
		stringLength1To64.setStringLength1To64(search.getHotelVendor().trim());
		hotelRef_type0.setBrandCode(stringLength1To64);
		criterion_type0[0].addHotelRef(hotelRef_type0);

	}

	TPA_ExtensionsType tPA_ExtensionsType = new TPA_ExtensionsType();

	ReservationDetails_type0 reservationDetails_type0 = new ReservationDetails_type0();

	DropOff_type0 dropOff_type0 = new DropOff_type0();
	StringLength1To255 stringLength1To255 = new StringLength1To255();
	stringLength1To255.setStringLength1To255("Domestic");
	dropOff_type0.setShipCompany(stringLength1To255);
	reservationDetails_type0.setDropOff(dropOff_type0);

	PickUp_type0 pickUp_type0 = new PickUp_type0();
	StringLength1To255 stringLength1To2552 = new StringLength1To255();
	stringLength1To2552.setStringLength1To255("International");
	pickUp_type0.setShipCompany(stringLength1To2552);
	reservationDetails_type0.setPickUp(pickUp_type0);

	tPA_ExtensionsType.setReservationDetails(reservationDetails_type0);

	SortResults_type0 sortResults_type0ResultsType0 = new SortResults_type0();
	// sortResults_type0ResultsType0.setSortBy(SortBy_type0.PRICE );
	// sortResults_type0ResultsType0.setSortOrder(SortOrder_type0.ASC);
	sortResults_type0ResultsType0.setSortBy(SortBy_type0.ASC);
	sortResults_type0ResultsType0.setSortOrder(SortOrder_type0.PRICE);
	tPA_ExtensionsType.addSortResults(sortResults_type0ResultsType0);

	if (PropertySingleton.getInstance().getProperty("XmlOut.Server").equalsIgnoreCase("staging")) {

		ReturnResultsType returnResultsType = new ReturnResultsType();
		returnResultsType.setDescription(true);
		returnResultsType.setContract(true);
		returnResultsType.setRoomTypes(!search.isSingleHotelSearch());
		returnResultsType.setImages(true);
		tPA_ExtensionsType.setReturnResults(returnResultsType);

	}

	/*
	 * FilterResultsType filterResultsType = new FilterResultsType(); filterResultsType.setFilter(Filter_type0.HotelNameFilter); filterResultsType.setStringLength0To255("H12");
	 * tPA_ExtensionsType.addFilterResults(filterResultsType );
	 */

	FilterResultsType filterResultsType = new FilterResultsType();
	filterResultsType.setFilter(Filter_type0.AvailableHotelFilter);
	filterResultsType.setStringLength0To255(search.getHotelID());
	tPA_ExtensionsType.addFilterResults(filterResultsType);

	/*
	 * FilterResultsType filterResultsType = new FilterResultsType(); filterResultsType.setFilter(Filter_type0.StarCategoryFilter); filterResultsType.setStringLength0To255("3");
	 * tPA_ExtensionsType.addFilterResults(filterResultsType );
	 * 
	 * 
	 * FilterResultsType filterResultsType2 = new FilterResultsType(); filterResultsType2.setFilter(Filter_type0.HotelNameFilter); filterResultsType2.setStringLength0To255("Test Hotel");
	 * tPA_ExtensionsType.addFilterResults(filterResultsType2 );
	 */

	// criterion_type0[0].setTPA_Extensions(tPA_ExtensionsType);

	hotelSearchCriteriaType.setCriterion(criterion_type0);
	SearchRQ.setCriteria(hotelSearchCriteriaType);

	SearchRQ.setVersion(new BigDecimal(3.0));
	logger.info("======================== Hotel Search Request Build Completed ======================");
	return SearchRQ;
	
}


public OTA_HotelAvailRQ buildHotelAvailRequest(SearchType search) throws Exception{
	

	System.out.println("================ start Room Search =====================");
    System.out.println("hotelId-->" + search.getHotelID());

		OTA_HotelAvailRQ oTA_HotelAvailRQ4 = new OTA_HotelAvailRQ();

		POS_Type pos = new POS_Type();
		SourceType[] sourceType = new SourceType[1];
		sourceType[0] = new SourceType();
		RequestorID_type0 requestorID_type0 = new RequestorID_type0();
		StringLength1To64 st132 = new StringLength1To64();
		st132.setStringLength1To64(search.getAgentID());
		requestorID_type0.setID(st132);
		OTA_CodeType codeType = new OTA_CodeType();
		codeType.setOTA_CodeType("5");
		requestorID_type0.setType(codeType);
		sourceType[0].setRequestorID(requestorID_type0);
		ISO3166 iso3166 = new ISO3166();
		iso3166.setISO3166(search.getCountryOFResidance().trim());
		sourceType[0].setISOCountry(iso3166);
		AlphaLength3 alphaLength3 = new AlphaLength3();
		alphaLength3.setAlphaLength3(search.getSearchCurrency().trim());
		sourceType[0].setISOCurrency(alphaLength3);
		pos.setSource(sourceType);
		oTA_HotelAvailRQ4.setPOS(pos);

		StringLength1To128 stringLength1To128 = new StringLength1To128();
		stringLength1To128.setStringLength1To128(PropertySingleton.getInstance().getProperty("Xmlout.EchoToken"));
		oTA_HotelAvailRQ4.setEchoToken(stringLength1To128);

		oTA_HotelAvailRQ4.setTarget(search.getHotel_room_Target());

		oTA_HotelAvailRQ4.setVersion(new BigDecimal(3.0));

		Calendar calendar = Calendar.getInstance();
		oTA_HotelAvailRQ4.setTimeStamp(calendar);

		/*
		 * NonNegativeInteger nonNegativeInteger = new NonNegativeInteger("123456"); oTA_HotelAvailRQ4.setSequenceNmbr(nonNegativeInteger);
		 */

		StringLength1To128 stringLength1To12811 = new StringLength1To128();
		stringLength1To12811.setStringLength1To128(search.getAvailabilityTracer());
		oTA_HotelAvailRQ4.setTransactionIdentifier(stringLength1To12811);

		AvailRequestSegments_type0 availRequestSegments_type0 = new AvailRequestSegments_type0();

		AvailRequestSegment_type0[] availRequestSegment_type0 = new AvailRequestSegment_type0[1];
		availRequestSegment_type0[0] = new AvailRequestSegment_type0();

		HotelSearchCriteriaType hotelSearchCriteriaType = new HotelSearchCriteriaType();

		Criterion_type0[] criterion_type0 = new Criterion_type0[1];
		criterion_type0[0] = new Criterion_type0();

		/*
		 * RefPoint_type0[] refPoint_type0 = new RefPoint_type0[1]; refPoint_type0[0] = new RefPoint_type0(); StringLength1To8 stringLength1To8 = new StringLength1To8();
		 * stringLength1To8.setStringLength1To8("CY1"); refPoint_type0[0].setCode(stringLength1To8); criterion_type0[0].setRefPoint(refPoint_type0);
		 */

		HotelRef_type0[] hotelRef_type0 = new HotelRef_type0[1];
		hotelRef_type0[0] = new HotelRef_type0();
		StringLength1To8 stringLength1To8 = new StringLength1To8();
		stringLength1To8.setStringLength1To8(search.getCityCode());
		hotelRef_type0[0].setHotelCityCode(stringLength1To8);
		StringLength1To16 stringLength1To16 = new StringLength1To16();
		stringLength1To16.setStringLength1To16(search.getHotelID());
		hotelRef_type0[0].setHotelCode(stringLength1To16);
		StringLength1To64 stringLength1To641 = new StringLength1To64();
		stringLength1To641.setStringLength1To64(search.getHotelVendor());
		hotelRef_type0[0].setBrandCode(stringLength1To641);

		criterion_type0[0].setHotelRef(hotelRef_type0);

		DateTimeSpanType dateTimeSpanType = new DateTimeSpanType();

		dateTimeSpanType.setStart(search.getIndate());
		dateTimeSpanType.setEnd(search.getOutDate());

		criterion_type0[0].setStayDateRange(dateTimeSpanType);

		java.util.TreeMap<String, String> SelectedRoomMap = (java.util.TreeMap<String, String>) search.getSelectedRoomCodes();
		int  noOfRooms   =  Integer.parseInt(search.getRooms());
		if (SelectedRoomMap.size() != Integer.parseInt(search.getRooms()))
			throw new Exception("No of Selected Room Types are not equal to the No of Requested Rooms");

		RoomStayCandidates_type0 roomStayCandidates_type0 = new RoomStayCandidates_type0();
		RoomStayCandidateType[] roomStayCandidateType = new RoomStayCandidateType[noOfRooms];
	   
		String[] noOfAdults                   = search.getAdults(); 
	    String[] noOfChildren                 = search.getChildren();
	    Map<Integer, String[]> ChildAges      = search.getChildAges();
	    
		for (int i = 0; i < noOfRooms; i++) {

			int RoomAdults                = Integer.parseInt(noOfAdults[i]);
			int RoomChildren              = Integer.parseInt(noOfChildren[i]);
			roomStayCandidateType[i]      = new RoomStayCandidateType();
			GuestCountType guestCountType = new GuestCountType();

			GuestCount_type0[] guestCount_type0 = new GuestCount_type0[RoomAdults + RoomChildren];

			int roomPassCount;

			for (roomPassCount = 0; roomPassCount < RoomAdults; roomPassCount++) {

				guestCount_type0[roomPassCount] = new GuestCount_type0();
				OTA_CodeType oTA_CodeType = new OTA_CodeType();
				oTA_CodeType.setOTA_CodeType("10");
				guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType);

			}

			String[] RoomChildAges    = ChildAges.get(i+1);
			for (int childCount = 0; childCount < RoomChildren; childCount++, roomPassCount++) {

				guestCount_type0[roomPassCount] = new GuestCount_type0();

				OTA_CodeType oTA_CodeType22 = new OTA_CodeType();
				oTA_CodeType22.setOTA_CodeType("8");

				Numeric0To999 numeric0To9993 = new Numeric0To999();
				BigInteger b3 = new BigInteger(RoomChildAges[childCount]);
				numeric0To9993.setNumeric0To999(b3);
				guestCount_type0[roomPassCount].setAge(numeric0To9993);
				guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType22);

			}

			guestCountType.setGuestCount(guestCount_type0);
			roomStayCandidateType[i].setGuestCounts(guestCountType);
			
			StringLength1To64 stringLength1To64 = new StringLength1To64();
			System.out.println("RoomSearch().rid[i] : " + SelectedRoomMap.get(Integer.toString(i+1))+ " added!");

			stringLength1To64.setStringLength1To64(SelectedRoomMap.get(Integer.toString(i+1)));
			roomStayCandidateType[i].setRoomTypeCode(stringLength1To64);

		}

		roomStayCandidates_type0.setRoomStayCandidate(roomStayCandidateType);

		criterion_type0[0].setRoomStayCandidates(roomStayCandidates_type0);

	
		
/*		StringLength1To64 stringLength1To64 = new StringLength1To64();
		System.out.println("RoomSearch().rid[i] : " + rid[i] + " added!");

		stringLength1To64.setStringLength1To64(rid[i]);
		roomStayCandidateType[i].setRoomTypeCode(stringLength1To64);*/
		
		
		
		/*
		 * Award_type0[] award_type0 = new Award_type0[1]; award_type0[0] = new Award_type0(); award_type0[0].setProvider("Rezg"); award_type0[0].setRating("5");
		 * criterion_type0[0].setAward(award_type0);
		 */

		hotelSearchCriteriaType.setCriterion(criterion_type0);
		availRequestSegment_type0[0].setHotelSearchCriteria(hotelSearchCriteriaType);

		availRequestSegments_type0.setAvailRequestSegment(availRequestSegment_type0);

		oTA_HotelAvailRQ4.setAvailRequestSegments(availRequestSegments_type0);

        return oTA_HotelAvailRQ4;

	
}



public OTA_HotelResRQ buildHotelReservationRequest(SearchType search) throws Exception{
	

	logger.info("================ start Hotel Reservation =====================");

		if (search.getServertype() == ServerType.LIVE) {

			System.out.print("Going to do a HotelReservation() in a LIVE Server. Proceed (y/n) ? ");
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String input = reader.readLine();
			if (input.equals("y"))
				System.out.println("Continuing...");
			else {
				System.out.println("Abort...");
				throw new Exception("User aborted the HotelSearch()");
			}
		}

		System.out.println("hotelId-->" + search.getHotelID());

		OTA_HotelResRQ oTA_HotelResRQ0 = new OTA_HotelResRQ();
		HotelResRequestType hotelResRequestType = new HotelResRequestType();

		POS_Type pos = new POS_Type();
		SourceType[] sourceType = new SourceType[1];
		sourceType[0] = new SourceType();
		RequestorID_type0 requestorID_type0 = new RequestorID_type0();
		StringLength1To64 st132 = new StringLength1To64();
		st132.setStringLength1To64(search.getAgentID());
		requestorID_type0.setID(st132);
		OTA_CodeType codeType = new OTA_CodeType();
		codeType.setOTA_CodeType("5");
		requestorID_type0.setType(codeType);
		sourceType[0].setRequestorID(requestorID_type0);
		ISO3166 iso3166 = new ISO3166();
		iso3166.setISO3166(search.getCountryOFResidance());
		sourceType[0].setISOCountry(iso3166);
		AlphaLength3 alphaLength3 = new AlphaLength3();
		alphaLength3.setAlphaLength3(search.getSearchCurrency());
		sourceType[0].setISOCurrency(alphaLength3);

		/*------------ Package reference -----------*/
		BookingChannel_type0 bookingChannel_type0 = new BookingChannel_type0();

		CompanyNameType companyNameType = new CompanyNameType();
		companyNameType.setStringLength0To128(PropertySingleton.getInstance().getProperty("XmlOut.CompanyName"));
		bookingChannel_type0.setCompanyName(companyNameType);

		/*
		 * OTA_CodeType oTA_CodeType23= new OTA_CodeType(); oTA_CodeType23.setOTA_CodeType("2"); bookingChannel_type0.setType(oTA_CodeType23);
		 */

		StringLength1To32 stringLength1To32_divition = new StringLength1To32();
		stringLength1To32_divition.setStringLength1To32("123456");
		companyNameType.setDivision(stringLength1To32_divition);

		sourceType[0].setBookingChannel(bookingChannel_type0);
		/*------------ Package reference -----------*/

		pos.setSource(sourceType);
		hotelResRequestType.setPOS(pos);

		StringLength1To128 stringLength1To128 = new StringLength1To128();
		stringLength1To128.setStringLength1To128(PropertySingleton.getInstance().getProperty("Xmlout.EchoToken"));
		hotelResRequestType.setEchoToken(stringLength1To128);

		hotelResRequestType.setTarget(search.getHotel_res_Target());

		hotelResRequestType.setVersion(new BigDecimal(3.0));

		/*
		 * NonNegativeInteger nonNegativeInteger = new NonNegativeInteger("123456"); hotelResRequestType.setSequenceNmbr(nonNegativeInteger);
		 */

		Calendar calendar = Calendar.getInstance();
		hotelResRequestType.setTimeStamp(calendar);

		StringLength1To128 stringLength1To128111 = new StringLength1To128();
		stringLength1To128111.setStringLength1To128(search.getAvailabilityTracer());
		hotelResRequestType.setTransactionIdentifier(stringLength1To128111);

		HotelResRequestTypeSequence_type0 hotelResRequestTypeSequence_type0 = new HotelResRequestTypeSequence_type0();
		HotelReservationsType hotelReservationsType = new HotelReservationsType();
		HotelReservationType[] hotelReservationTypes = new HotelReservationType[1];
		hotelReservationTypes[0] = new HotelReservationType();

		ResGlobalInfoType resGlobalInfoType = new ResGlobalInfoType();

		ProfilesType profilesType22 = new ProfilesType();
		ProfileInfo_type0 profileInfo_type02 = new ProfileInfo_type0();
		ProfileType profileType22 = new ProfileType();
		CustomerType customerType2 = new CustomerType();

		PersonNameType personNameType2 = new PersonNameType();
		StringLength1To16 stringLength1To16s22 = new StringLength1To16();
		stringLength1To16s22 = new StringLength1To16();

		stringLength1To16s22.setStringLength1To16(PropertySingleton.getInstance().getProperty("Xmlout.CustomerTitle")); //Title
		personNameType2.setNamePrefix(stringLength1To16s22);
		StringLength1To64 stringLength1To6422s = new StringLength1To64();

		stringLength1To6422s = new StringLength1To64();
		stringLength1To6422s.setStringLength1To64(PropertySingleton.getInstance().getProperty("Xmlout.CustomerFname")); //GivenName
		personNameType2.setGivenName(stringLength1To6422s);

		StringLength1To64 stringLength1To642222 = new StringLength1To64();
		stringLength1To642222.setStringLength1To64(PropertySingleton.getInstance().getProperty("Xmlout.CustomerLastName")); //Surname
		personNameType2.setSurname(stringLength1To642222);

		StringLength1To64 stringLength1To64222222 = new StringLength1To64();
		stringLength1To64222222.setStringLength1To64(PropertySingleton.getInstance().getProperty("Xmlout.CustomerMname"));//MiddleName
		personNameType2.setMiddleName(stringLength1To64222222);

		customerType2.setPersonName(personNameType2);

		EmailType emailType2 = new EmailType();
		emailType2.setStringLength1To128(PropertySingleton.getInstance().getProperty("Xmlout.CustomerEmail")); //Email
		customerType2.setEmail(emailType2);

		Address_type0 addressInfoType = new Address_type0();
		StringLength1To255 stringLength1To255222 = new StringLength1To255();
		stringLength1To255222.setStringLength1To255(PropertySingleton.getInstance().getProperty("Xmlout.CustomerAddress1")+","+PropertySingleton.getInstance().getProperty("Xmlout.CustomerAddress2")); //Adderss
		addressInfoType.setAddressLine(stringLength1To255222);

		StateProvType stateProvType2 = new StateProvType();
		StateProvCodeType stateProvCodeType2 = new StateProvCodeType();
		stateProvCodeType2.setStringLength1To8(PropertySingleton.getInstance().getProperty("Xmlout.CustomerStatCode"));//HI
		stateProvType2.setStateCode(stateProvCodeType2);
		stateProvType2.setStringLength0To64(PropertySingleton.getInstance().getProperty("Xmlout.CustomerProvince"));//Western
		addressInfoType.setStateProv(stateProvType2);

		StringLength1To16 stringLength1To16222 = new StringLength1To16();
		stringLength1To16222.setStringLength1To16(PropertySingleton.getInstance().getProperty("Xmlout.CustomerPostal"));//Postal
		addressInfoType.setPostalCode(stringLength1To16222);

		StringLength1To64 stringLength1To6422221 = new StringLength1To64();
		stringLength1To6422221.setStringLength1To64(PropertySingleton.getInstance().getProperty("Xmlout.CustomerCity"));//City
		CountryNameType countryNameType2 = new CountryNameType();
		countryNameType2.setStringLength0To64(PropertySingleton.getInstance().getProperty("Xmlout.CustomerCountry"));//Country
		ISO3166 iso316622 = new ISO3166();
		iso316622.setISO3166(PropertySingleton.getInstance().getProperty("Xmlout.CustomerCountryCode"));  //CityCode
		countryNameType2.setCode(iso316622);
		addressInfoType.setCountryName(countryNameType2);
		addressInfoType.setCityName(stringLength1To6422221);

		customerType2.setAddress(addressInfoType);

		Telephone_type0 telephone_type2s = new Telephone_type0();
		StringLength1To32 stringLength1To32222 = new StringLength1To32();
		stringLength1To32222.setStringLength1To32(PropertySingleton.getInstance().getProperty("Xmlout.CustomerTelephone"));  //Telephone
		telephone_type2s.setPhoneNumber(stringLength1To32222);
		customerType2.setTelephone(telephone_type2s);
		// customerType2.addContactPerson(contactPersonType );

		profileType22.setCustomer(customerType2);
		profileInfo_type02.setProfile(profileType22);
		profilesType22.addProfileInfo(profileInfo_type02);
		resGlobalInfoType.setProfiles(profilesType22);

		if (search.getPaymentType().trim().equalsIgnoreCase("online")) { // for online payment method only
			GuaranteeType guaranteeType = new GuaranteeType();
			GuaranteesAccepted_type0 guaranteesAccepted_type0 = new GuaranteesAccepted_type0();
			GuaranteeAccepted_type0 guaranteeAccepted_type0 = new GuaranteeAccepted_type0();

			PaymentCardType paymentCardType = new PaymentCardType();
			NumericStringLength1To19 numericStringLength1To19 = new NumericStringLength1To19();
			numericStringLength1To19.setNumericStringLength1To19(PropertySingleton.getInstance().getProperty(search.getCardType().toUpperCase()+".CardNumber"));
			paymentCardType.setCardNumber(numericStringLength1To19);

			/*
			 * OTA_CodeType oTA_CodeType = new OTA_CodeType(); oTA_CodeType.setOTA_CodeType("1"); paymentCardType.setCardType(oTA_CodeType );
			 */

			MMYYDate mMYYDate = new MMYYDate();
			mMYYDate.setMMYYDate(PropertySingleton.getInstance().getProperty(search.getCardType().toUpperCase()+".Expiration"));
			paymentCardType.setExpireDate(mMYYDate);

			NumericStringLength1To8 numericStringLength1To8 = new NumericStringLength1To8();
			numericStringLength1To8.setNumericStringLength1To8(PropertySingleton.getInstance().getProperty(search.getCardType().toUpperCase()+".CSV"));
			paymentCardType.setSeriesCode(numericStringLength1To8);

			PaymentCardCodeType paymentCardCodeType = new PaymentCardCodeType();
			paymentCardCodeType.setUpperCaseAlphaLength1To2(PropertySingleton.getInstance().getProperty(search.getCardType().toUpperCase()+".Code"));
			paymentCardType.setCardCode(paymentCardCodeType);

			StringLength1To64 stringLength1To645 = new StringLength1To64();
			stringLength1To645.setStringLength1To64(personNameType2.getGivenName() +" "+personNameType2.getSurname());
			paymentCardType.setCardHolderName(stringLength1To645);

			PaymentCardType paymentCardType2 = new PaymentCardType();
			StringLength1To128 stringLength1To1282 = new StringLength1To128();
			stringLength1To1282.setStringLength1To128("authorization successful");
			paymentCardType2.setRemark(stringLength1To1282);
			guaranteeAccepted_type0.setPaymentCard(paymentCardType2);

			StringLength1To64 stringLength1To64225 = new StringLength1To64();
			stringLength1To64225.setStringLength1To64("d613e693-7230-428b-8b59-7f6c43ef3b45");
			guaranteeAccepted_type0.setGuaranteeID(stringLength1To64225);

			guaranteeAccepted_type0.setPaymentTransactionTypeCode(PaymentTransactionTypeCode_type0.reserve);

			guaranteesAccepted_type0.addGuaranteeAccepted(guaranteeAccepted_type0);
			guaranteeType.setGuaranteesAccepted(guaranteesAccepted_type0);
			resGlobalInfoType.setGuarantee(guaranteeType);
		}

		hotelReservationTypes[0].setResGlobalInfo(resGlobalInfoType);

		java.util.TreeMap<String, String> SelectedRoomMap = (java.util.TreeMap<String, String>) search.getSelectedRoomCodes();
		int  noOfRooms   =  Integer.parseInt(search.getRooms());
		if (SelectedRoomMap.size() != Integer.parseInt(search.getRooms()))
			throw new Exception("No of Selected Room Types are not equal to the No of Requested Rooms");
	
		RoomStaysType roomStaysType = new RoomStaysType();
		RoomStay_type1[] roomStay_type1s = new RoomStay_type1[noOfRooms];
		
		String[] noOfAdults                   = search.getAdults(); 
	    String[] noOfChildren                 = search.getChildren();
	    Map<Integer, String[]> ChildAges      = search.getChildAges();

		for (int i = 0; i < noOfRooms; i++) {

			
			int RoomAdults                = Integer.parseInt(noOfAdults[i]);
			int RoomChildren              = Integer.parseInt(noOfChildren[i]);
			roomStay_type1s[i] = new RoomStay_type1();

			RoomTypes_type1 roomTypes_type1 = new RoomTypes_type1();

			RoomTypeType[] roomTypeTypes = new RoomTypeType[1];
			roomTypeTypes[0] = new RoomTypeType();
			StringLength1To64 stringLength1To64 = new StringLength1To64();
			stringLength1To64.setStringLength1To64(SelectedRoomMap.get(Integer.toString(i+1)));
			roomTypeTypes[0].setRoomTypeCode(stringLength1To64);
			roomTypes_type1.setRoomType(roomTypeTypes);
			roomStay_type1s[i].setRoomTypes(roomTypes_type1);

			BasicPropertyInfoType basicPropertyInfoType = new BasicPropertyInfoType();
			StringLength1To16 stringLength1To16 = new StringLength1To16();
			stringLength1To16.setStringLength1To16(search.getHotelID().trim());
			basicPropertyInfoType.setHotelCode(stringLength1To16);

			StringLength1To64 stringLength1To642 = new StringLength1To64();
			stringLength1To642.setStringLength1To64(search.getHotelVendor());
			basicPropertyInfoType.setBrandCode(stringLength1To642);

			StringLength1To8 stringLength1To8 = new StringLength1To8();
			stringLength1To8.setStringLength1To8(search.getCityCode());
			basicPropertyInfoType.setHotelCityCode(stringLength1To8);

			roomStay_type1s[i].setBasicPropertyInfo(basicPropertyInfoType);

			GuestCountType guestCountType = new GuestCountType();

			GuestCount_type0[] guestCount_type0 = new GuestCount_type0[RoomAdults + RoomChildren];

			int roomPassCount;

			for (roomPassCount = 0; roomPassCount < RoomAdults; roomPassCount++) {

				guestCount_type0[roomPassCount] = new GuestCount_type0();
				OTA_CodeType oTA_CodeType = new OTA_CodeType();
				oTA_CodeType.setOTA_CodeType("10");
				guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType);

			}
			String[] RoomChildAges    = ChildAges.get(i+1);
			for (int childCount = 0; childCount < RoomChildren; childCount++, roomPassCount++) {

				guestCount_type0[roomPassCount] = new GuestCount_type0();

				OTA_CodeType oTA_CodeType22 = new OTA_CodeType();
				oTA_CodeType22.setOTA_CodeType("8");

				Numeric0To999 numeric0To9993 = new Numeric0To999();
				BigInteger b3 = new BigInteger(RoomChildAges[childCount]);
				numeric0To9993.setNumeric0To999(b3);
				guestCount_type0[roomPassCount].setAge(numeric0To9993);
				guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType22);

			}

			guestCountType.setGuestCount(guestCount_type0);

			roomStay_type1s[i].setGuestCounts(guestCountType);

			TimeSpan_type0 timeSpan_type0 = new TimeSpan_type0();

			timeSpan_type0.setStart(search.getIndate());
			timeSpan_type0.setEnd(search.getOutDate());

			roomStay_type1s[i].setTimeSpan(timeSpan_type0);

		}

		roomStaysType.setRoomStay(roomStay_type1s);

		hotelReservationTypes[0].setRoomStays(roomStaysType);

		/*
		 * UniqueID_Type uniqueID_Type = new UniqueID_Type(); StringLength1To64 stringLength1To64222 = new StringLength1To64(); stringLength1To64222.setStringLength1To64("1234567");
		 * uniqueID_Type.setID(stringLength1To64222 ); OTA_CodeType oTA_CodeType22 = new OTA_CodeType(); oTA_CodeType22.setOTA_CodeType("38"); uniqueID_Type.setType(oTA_CodeType22 );
		 * StringLength1To32 stringLength1To32223 = new StringLength1To32(); stringLength1To32223.setStringLength1To32("taproNo"); uniqueID_Type.setID_Context(stringLength1To32223 );
		 * hotelReservationTypes[0].addUniqueID(uniqueID_Type );
		 */

		ResGuestsType resGuestsType = new ResGuestsType();
		
		
		HashMap<Integer, String> NameMap = new HashMap<Integer, String>();

		NameMap.put(1, "One");
		NameMap.put(2, "two");
		NameMap.put(3, "three");
		NameMap.put(4, "Four");
		NameMap.put(5, "Five");
		NameMap.put(6, "six");
		NameMap.put(7, "seven");

		for (int i = 0; i < noOfRooms; i++) {
			
			
			int RoomAdults                = Integer.parseInt(noOfAdults[i]);
			int RoomChildren              = Integer.parseInt(noOfChildren[i]);
			String[] RoomChildAges        = ChildAges.get(i+1);
			for (int adultCount = 0; adultCount < RoomAdults; adultCount++) {
				
				String AdultFirstName = "R" + NameMap.get(i + 1) + "Adult" + NameMap.get(adultCount + 1) + "FirstXML";
				String MiddleName     = "R" + NameMap.get(i + 1) + "Adult" + NameMap.get(adultCount + 1) + "MiddleXML";
				String AdultLastName  = "R" + NameMap.get(i + 1) + "Adult" + NameMap.get(adultCount + 1) + "LastXML";

				ResGuest_type0 resGuest_type01 = new ResGuest_type0();
				ProfilesType profilesType1 = new ProfilesType();
				ProfileInfo_type0 profileInfo_type01 = new ProfileInfo_type0();
				ProfileType profileType1 = new ProfileType();
				CustomerType customerType1 = new CustomerType();
				PersonNameType personNameType1 = new PersonNameType();

				StringLength1To16 stringLength1To16s1 = new StringLength1To16();
				stringLength1To16s1 = new StringLength1To16();
				stringLength1To16s1.setStringLength1To16("Mr");
				personNameType1.setNamePrefix(stringLength1To16s1);

				StringLength1To64 stringLength1To64s1 = new StringLength1To64();
				stringLength1To64s1 = new StringLength1To64();
				stringLength1To64s1.setStringLength1To64(AdultFirstName);
				personNameType1.setGivenName(stringLength1To64s1);

				StringLength1To64 stringLength1To641 = new StringLength1To64();
				stringLength1To641.setStringLength1To64(AdultLastName);
				personNameType1.setSurname(stringLength1To641);

				StringLength1To64 stringLength1To64221 = new StringLength1To64();
				stringLength1To64221.setStringLength1To64(MiddleName);
				personNameType1.setMiddleName(stringLength1To64221);

				if (i == 0 && adultCount == 0) {

					Telephone_type0 telephone_type1s = new Telephone_type0();
					StringLength1To32 stringLength1To322 = new StringLength1To32();
					stringLength1To322.setStringLength1To32(PropertySingleton.getInstance().getProperty("Xmlout.PassengerTelephone"));
					telephone_type1s.setPhoneNumber(stringLength1To322);
					customerType1.setTelephone(telephone_type1s);

					EmailType emailType = new EmailType();
					emailType.setStringLength1To128(PropertySingleton.getInstance().getProperty("Xmlout.PassengerEmail"));
					customerType1.setEmail(emailType);

					Address_type0 address_type1s = new Address_type0();
					StringLength1To255 stringLength1To255 = new StringLength1To255();
					stringLength1To255.setStringLength1To255(PropertySingleton.getInstance().getProperty("Xmlout.PassengerAddress1")+","+PropertySingleton.getInstance().getProperty("Xmlout.PassengerAddress2"));
					address_type1s.setAddressLine(stringLength1To255);

					StateProvType stateProvType = new StateProvType();
					StateProvCodeType stateProvCodeType = new StateProvCodeType();
					stateProvCodeType.setStringLength1To8(PropertySingleton.getInstance().getProperty("Xmlout.PassengerStatCode"));
					stateProvType.setStateCode(stateProvCodeType);
					stateProvType.setStringLength0To64(PropertySingleton.getInstance().getProperty("Xmlout.PassengerProvince"));
					address_type1s.setStateProv(stateProvType);

					StringLength1To16 stringLength1To16 = new StringLength1To16();
					stringLength1To16.setStringLength1To16(PropertySingleton.getInstance().getProperty("Xmlout.PassengerPostal"));
					address_type1s.setPostalCode(stringLength1To16);

					StringLength1To64 stringLength1To642 = new StringLength1To64();
					stringLength1To642.setStringLength1To64(PropertySingleton.getInstance().getProperty("Xmlout.PassengerCity"));
					CountryNameType countryNameType = new CountryNameType();
					countryNameType.setStringLength0To64(PropertySingleton.getInstance().getProperty("Xmlout.PassengerCountry"));
					ISO3166 iso31662 = new ISO3166();
					iso31662.setISO3166(PropertySingleton.getInstance().getProperty("Xmlout.PassengerCountryCode"));
					countryNameType.setCode(iso31662);
					address_type1s.setCountryName(countryNameType);
					address_type1s.setCityName(stringLength1To642);
					customerType1.setAddress(address_type1s);
				}

				customerType1.setPersonName(personNameType1);

				profileType1.setCustomer(customerType1);
				profileInfo_type01.setProfile(profileType1);
				profilesType1.addProfileInfo(profileInfo_type01);
				resGuest_type01.setProfiles(profilesType1);

				Numeric0To999 numeric0To9991 = new Numeric0To999();
				BigInteger bigInteger1 = new BigInteger("24");
				numeric0To9991.setNumeric0To999(bigInteger1);
				resGuest_type01.setAge(numeric0To9991);
				resGuestsType.addResGuest(resGuest_type01);

			}

			for (int childCount = 0; childCount < RoomChildren; childCount++) {
				
				String ChildFirstName = "R" + NameMap.get(i + 1) + "Child" + NameMap.get(childCount + 1) + "FirstXML";
				String ChildMiddleName= "R" + NameMap.get(i + 1) + "Child" + NameMap.get(childCount + 1) + "MiddleXML";
				String ChildLastName  = "R" + NameMap.get(i + 1) + "Child" + NameMap.get(childCount + 1) + "LastXML";

				ResGuest_type0 resGuest_type01 = new ResGuest_type0();
				ProfilesType profilesType1 = new ProfilesType();
				ProfileInfo_type0 profileInfo_type01 = new ProfileInfo_type0();
				ProfileType profileType1 = new ProfileType();
				CustomerType customerType1 = new CustomerType();
				PersonNameType personNameType1 = new PersonNameType();

				StringLength1To16 stringLength1To16s1 = new StringLength1To16();
				stringLength1To16s1 = new StringLength1To16();
				stringLength1To16s1.setStringLength1To16("Mr");
				personNameType1.setNamePrefix(stringLength1To16s1);

				StringLength1To64 stringLength1To64s1 = new StringLength1To64();
				stringLength1To64s1 = new StringLength1To64();
				stringLength1To64s1.setStringLength1To64(ChildFirstName);
				personNameType1.setGivenName(stringLength1To64s1);

				StringLength1To64 stringLength1To641 = new StringLength1To64();
				stringLength1To641.setStringLength1To64(ChildLastName);
				personNameType1.setSurname(stringLength1To641);

				StringLength1To64 stringLength1To64221 = new StringLength1To64();
				stringLength1To64221.setStringLength1To64(ChildMiddleName);
				personNameType1.setMiddleName(stringLength1To64221);

				customerType1.setPersonName(personNameType1);

				profileType1.setCustomer(customerType1);
				profileInfo_type01.setProfile(profileType1);
				profilesType1.addProfileInfo(profileInfo_type01);
				resGuest_type01.setProfiles(profilesType1);

				Numeric0To999 numeric0To9991 = new Numeric0To999();
			
				BigInteger bigInteger1 = new BigInteger(RoomChildAges[childCount]);
				numeric0To9991.setNumeric0To999(bigInteger1);
				resGuest_type01.setAge(numeric0To9991);
				resGuestsType.addResGuest(resGuest_type01);

			}

		}

		hotelReservationTypes[0].setResGuests(resGuestsType);

		// ResGlobalInfoType resGlobalInfoType = new ResGlobalInfoType();
		/*
		 * TimeSpan_type0 timeSpan_type0 = new TimeSpan_type0();
		 * 
		 * Calendar cal1 = Calendar.getInstance(); cal1.setTime(indate); Calendar cal2 = Calendar.getInstance(); cal2.setTime(outdate);
		 * 
		 * timeSpan_type0.setStart(cal1); timeSpan_type0.setEnd(cal2);
		 */

		// resGlobalInfoType.setTimeSpan(timeSpan_type0);
		// hotelReservationTypes[0].setResGlobalInfo(resGlobalInfoType);

		hotelReservationsType.setHotelReservation(hotelReservationTypes);
		hotelResRequestTypeSequence_type0.setHotelReservations(hotelReservationsType);
		hotelResRequestType.setHotelResRequestTypeSequence_type0(hotelResRequestTypeSequence_type0);

		oTA_HotelResRQ0.setOTA_HotelResRQ(hotelResRequestType);
        return oTA_HotelResRQ0;

	
}


public OTA_CancelRQ   buildHotelCancellatioRequest(SearchType search) throws Exception{
	

		System.out.println("hotelId-->" + search.getHotelID());

		OTA_CancelRQ oTA_CancelRQ8 = new OTA_CancelRQ();

		POS_Type pos = new POS_Type();
		SourceType[] sourceType = new SourceType[1];
		sourceType[0] = new SourceType();
		RequestorID_type0 requestorID_type0 = new RequestorID_type0();
		StringLength1To64 st132 = new StringLength1To64();
		st132.setStringLength1To64(search.getAgentID());
		requestorID_type0.setID(st132);
		OTA_CodeType codeType = new OTA_CodeType();
		codeType.setOTA_CodeType("5");
		requestorID_type0.setType(codeType);
		sourceType[0].setRequestorID(requestorID_type0);
		ISO3166 iso3166 = new ISO3166();
		iso3166.setISO3166(search.getCountryOFResidance());
		sourceType[0].setISOCountry(iso3166);
		AlphaLength3 alphaLength3 = new AlphaLength3();
		alphaLength3.setAlphaLength3(search.getSearchCurrency());
		sourceType[0].setISOCurrency(alphaLength3);

		pos.setSource(sourceType);
		oTA_CancelRQ8.setPOS(pos);
		StringLength1To128 stringLength1To128 = new StringLength1To128();
		stringLength1To128.setStringLength1To128(PropertySingleton.getInstance().getProperty("Xmlout.EchoToken"));
		oTA_CancelRQ8.setEchoToken(stringLength1To128);

		oTA_CancelRQ8.setTarget(search.getHotel_cncl_Target());
		oTA_CancelRQ8.setVersion(new BigDecimal(3.0));
		/*
		 * NonNegativeInteger nonNegativeInteger = new NonNegativeInteger("123456"); oTA_CancelRQ8.setSequenceNmbr(nonNegativeInteger);
		 */

		Calendar calendar = Calendar.getInstance();
		oTA_CancelRQ8.setTimeStamp(calendar);

		// oTA_CancelRQ8.setCancelType(TransactionActionType.Cancel);

		UniqueID_type0 uniqueID_type0 = new UniqueID_type0();

		OTA_CodeType oTA_CodeType = new OTA_CodeType();
		oTA_CodeType.setOTA_CodeType("1");
		uniqueID_type0.setType(oTA_CodeType);

		StringLength1To64 stringLength1To32 = new StringLength1To64();
		stringLength1To32.setStringLength1To64(search.getServiceID());
		uniqueID_type0.setID(stringLength1To32);
		oTA_CancelRQ8.addUniqueID(uniqueID_type0);

		Reasons_type0 reasons_type0 = new Reasons_type0();
		FreeTextType freeTextType = new FreeTextType();
		freeTextType.setString("This Booking is beign cacellation through automation");
		reasons_type0.addReason(freeTextType);
		oTA_CancelRQ8.setReasons(reasons_type0);


		// ReservationServiceStub stub = new ReservationServiceStub();
     	return oTA_CancelRQ8;

	
}


public OTA_HotelDescriptiveInfoRQ buildHotelInfoRequest(SearchType search)throws Exception {

	
		System.out.println(" Building Hotel Info request for hotelId-->" + search.getHotelName());

		OTA_HotelDescriptiveInfoRQ oTA_HotelDescriptiveInfoRQ2 = new OTA_HotelDescriptiveInfoRQ();

		POS_Type pos = new POS_Type();
		SourceType[] sourceType = new SourceType[1];
		sourceType[0] = new SourceType();
		RequestorID_type0 requestorID_type0 = new RequestorID_type0();
		StringLength1To64 st132 = new StringLength1To64();
		st132.setStringLength1To64(search.getAgentID());
		requestorID_type0.setID(st132);
		OTA_CodeType codeType = new OTA_CodeType();
		codeType.setOTA_CodeType("5");
		requestorID_type0.setType(codeType);
		sourceType[0].setRequestorID(requestorID_type0);
		ISO3166 iso3166 = new ISO3166();
		iso3166.setISO3166(search.getCountryOFResidance());
		sourceType[0].setISOCountry(iso3166);
		AlphaLength3 alphaLength3 = new AlphaLength3();
		alphaLength3.setAlphaLength3(search.getSearchCurrency());
		sourceType[0].setISOCurrency(alphaLength3);
		pos.setSource(sourceType);
		oTA_HotelDescriptiveInfoRQ2.setPOS(pos);
		StringLength1To128 stringLength1To128 = new StringLength1To128();
		stringLength1To128.setStringLength1To128(PropertySingleton.getInstance().getProperty("Xmlout.EchoToken"));
		oTA_HotelDescriptiveInfoRQ2.setEchoToken(stringLength1To128);

		StringLength1To128 stringLength1To1281 = new StringLength1To128();
		stringLength1To1281.setStringLength1To128(search.getAvailabilityTracer());
		oTA_HotelDescriptiveInfoRQ2.setTransactionIdentifier(stringLength1To1281);

		oTA_HotelDescriptiveInfoRQ2.setTarget(search.getHotel_info_Target());
		oTA_HotelDescriptiveInfoRQ2.setVersion(new BigDecimal(3.0));

		Calendar calendar = Calendar.getInstance();
		oTA_HotelDescriptiveInfoRQ2.setTimeStamp(calendar);

		HotelDescriptiveInfos_type0 hotelDescriptiveInfos_type0 = new HotelDescriptiveInfos_type0();
		HotelDescriptiveInfo_type0 hotelDescriptiveInfo_type0 = new HotelDescriptiveInfo_type0();
		StringLength1To8 stringLength1To8 = new StringLength1To8();
		stringLength1To8.setStringLength1To8(search.getCityCode());
		hotelDescriptiveInfo_type0.setHotelCityCode(stringLength1To8);
		StringLength1To16 stringLength1To16 = new StringLength1To16();
		System.out.println("hotelCode--->" + search.getHotelID());
		stringLength1To16.setStringLength1To16(search.getHotelID());
		hotelDescriptiveInfo_type0.setHotelCode(stringLength1To16);

		StringLength1To64 stringLength1To641 = new StringLength1To64();
		stringLength1To641.setStringLength1To64(search.getHotelVendor());
		hotelDescriptiveInfo_type0.setBrandCode(stringLength1To641);

		hotelDescriptiveInfos_type0.addHotelDescriptiveInfo(hotelDescriptiveInfo_type0);
		oTA_HotelDescriptiveInfoRQ2.setHotelDescriptiveInfos(hotelDescriptiveInfos_type0);

       return oTA_HotelDescriptiveInfoRQ2;


}
}
