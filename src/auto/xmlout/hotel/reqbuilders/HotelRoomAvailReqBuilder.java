package auto.xmlout.hotel.reqbuilders;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.axis2.databinding.types.NonNegativeInteger;
import org.apache.axis2.databinding.types.PositiveInteger;
import org.apache.log4j.Logger;

import auto.xmlout.com.types.HotelXmlSearchByType;
import auto.xmlout.common.configure.GlobalConfigurator;
import auto.xmlout.hotel.loaders.PropertySingleton;
import auto.xmlout.hotel.pojo.SearchType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AlphaLength3;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AlternateAvailability_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CodeContext_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Criterion_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DateTimeSpanType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DropOff_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FilterResultsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Filter_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuestCountType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuestCount_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelRef_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelSearchCriteriaType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ISO3166;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Numeric0To999;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.POS_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PickUp_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RefPoint_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RequestorID_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ReservationDetails_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ReturnResultsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStayCandidateType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStayCandidates_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortBy_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortOrder_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortResults_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SourceType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To128;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To16;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To255;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To64;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To8;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TPA_ExtensionsType;

public class HotelRoomAvailReqBuilder {

//private HashMap<String, String> propertyList   = null;
private Logger                  logger         = null;

public HotelRoomAvailReqBuilder(){
	logger        = Logger.getLogger(this.getClass());
	//propertyList  = prop;
}
	
public OTA_HotelSearchRQ buildHotelSearchRequest(SearchType search) throws Exception{
	
	OTA_HotelSearchRQ SearchRQ = new OTA_HotelSearchRQ();
    SearchRQ.setTarget(search.getHotel_search_Target());
    Calendar calendar = Calendar.getInstance();
	SearchRQ.setTimeStamp(calendar);

	// For Single Hotel Search 
	StringLength1To128 stringLength1To128 = new StringLength1To128();
	stringLength1To128.setStringLength1To128(GlobalConfigurator.HOTEL_SEARCH_CACHE);
	if (search.isSingleHotelSearch())
		SearchRQ.setTransactionIdentifier(stringLength1To128);

	POS_Type pos = new POS_Type();
	SourceType[] sourceType = new SourceType[1];
	sourceType[0] = new SourceType();

	RequestorID_type0 requestorID_type0 = new RequestorID_type0();
	StringLength1To64 st132 = new StringLength1To64();
	st132.setStringLength1To64(search.getAgentID());
	requestorID_type0.setID(st132);

	OTA_CodeType codeType = new OTA_CodeType();
	codeType.setOTA_CodeType("5");
	requestorID_type0.setType(codeType);
	sourceType[0].setRequestorID(requestorID_type0);

	ISO3166 iso3166 = new ISO3166();
	iso3166.setISO3166(search.getCountryOFResidance());
	sourceType[0].setISOCountry(iso3166);

	AlphaLength3 alphaLength3 = new AlphaLength3();
	alphaLength3.setAlphaLength3(search.getSearchCurrency());
	sourceType[0].setISOCurrency(alphaLength3);
	pos.setSource(sourceType);
	SearchRQ.setPOS(pos);

	StringLength1To128 stringLength1To12811 = new StringLength1To128();
	stringLength1To12811.setStringLength1To128(PropertySingleton.getInstance().getProperty("Xmlout.EchoToken"));
	SearchRQ.setEchoToken(stringLength1To12811);

	PositiveInteger positiveInteger = new PositiveInteger(PropertySingleton.getInstance().getProperty("Xmlout.MaxResponses"));
	SearchRQ.setMaxResponses(positiveInteger);

	NonNegativeInteger nonNegativeInteger = new NonNegativeInteger(PropertySingleton.getInstance().getProperty("Xmlout.MinResponses"));
	SearchRQ.setSequenceNmbr(nonNegativeInteger);

	HotelSearchCriteriaType hotelSearchCriteriaType = new HotelSearchCriteriaType();

	Criterion_type0[] criterion_type0 = new Criterion_type0[1];
	criterion_type0[0] = new Criterion_type0();

	RefPoint_type0[] refPoint_type0 = new RefPoint_type0[1];
	refPoint_type0[0] = new RefPoint_type0();
	StringLength1To8 stringLength1To8 = new StringLength1To8();

	if(search.getSerachBy() == HotelXmlSearchByType.AIRPORTCODE){
		stringLength1To8.setStringLength1To8(search.getAirPortCode());
		refPoint_type0[0].setCodeContext(CodeContext_type0.airport);	
	}else if(search.getSerachBy() == HotelXmlSearchByType.CITYCODE){
		stringLength1To8.setStringLength1To8(search.getCityCode());
	}else{
		
	}

    refPoint_type0[0].setCode(stringLength1To8);
	criterion_type0[0].setRefPoint(refPoint_type0);
	criterion_type0[0].setAlternateAvailability(AlternateAvailability_type0.Always);

	DateTimeSpanType dateTimeSpanType = new DateTimeSpanType();

	dateTimeSpanType.setStart(search.getIndate());
	dateTimeSpanType.setEnd(search.getOutDate());

	criterion_type0[0].setStayDateRange(dateTimeSpanType);

	int  noOfRooms = Integer.parseInt(search.getRooms());
	RoomStayCandidates_type0 roomStayCandidates_type0 = new RoomStayCandidates_type0();
	RoomStayCandidateType[] roomStayCandidateType = new RoomStayCandidateType[noOfRooms];
   
	String[] noOfAdults                   = search.getAdults(); 
    String[] noOfChildren                 = search.getChildren();
    Map<Integer, String[]> ChildAges      = search.getChildAges();
    
	for (int i = 0; i < noOfRooms; i++) {

		int RoomAdults                = Integer.parseInt(noOfAdults[i]);
		int RoomChildren              = Integer.parseInt(noOfChildren[i]);
		roomStayCandidateType[i]      = new RoomStayCandidateType();
		GuestCountType guestCountType = new GuestCountType();

		GuestCount_type0[] guestCount_type0 = new GuestCount_type0[RoomAdults + RoomChildren];

		int roomPassCount;

		for (roomPassCount = 0; roomPassCount < RoomAdults; roomPassCount++) {

			guestCount_type0[roomPassCount] = new GuestCount_type0();
			OTA_CodeType oTA_CodeType = new OTA_CodeType();
			oTA_CodeType.setOTA_CodeType("10");
			guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType);

		}

		String[] RoomChildAges    = ChildAges.get(i+1);
		for (int childCount = 0; childCount < RoomChildren; childCount++, roomPassCount++) {

			guestCount_type0[roomPassCount] = new GuestCount_type0();

			OTA_CodeType oTA_CodeType22 = new OTA_CodeType();
			oTA_CodeType22.setOTA_CodeType("8");

			Numeric0To999 numeric0To9993 = new Numeric0To999();
			BigInteger b3 = new BigInteger(RoomChildAges[childCount]);
			numeric0To9993.setNumeric0To999(b3);
			guestCount_type0[roomPassCount].setAge(numeric0To9993);
			guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType22);

		}

		guestCountType.setGuestCount(guestCount_type0);
		roomStayCandidateType[i].setGuestCounts(guestCountType);

	}

	roomStayCandidates_type0.setRoomStayCandidate(roomStayCandidateType);

	criterion_type0[0].setRoomStayCandidates(roomStayCandidates_type0);

	/*
	 * Award_type0[] award_type0 = new Award_type0[1]; award_type0[0] = new Award_type0(); award_type0[0].setProvider("RezG"); award_type0[0].setRating("5EST");
	 * criterion_type0[0].setAward(award_type0);
	 */

	if (search.isSingleHotelSearch()) {

		HotelRef_type0 hotelRef_type0 = new HotelRef_type0();

		StringLength1To8 stringLength1To82 = new StringLength1To8();
		stringLength1To82.setStringLength1To8(search.getCityCode().trim());
		hotelRef_type0.setHotelCityCode(stringLength1To82);
		StringLength1To16 stringLength1To16 = new StringLength1To16();
		stringLength1To16.setStringLength1To16(search.getHotelID().trim());
		hotelRef_type0.setHotelCode(stringLength1To16);
		StringLength1To64 stringLength1To64 = new StringLength1To64();
		stringLength1To64.setStringLength1To64(search.getHotelVendor().trim());
		hotelRef_type0.setBrandCode(stringLength1To64);
		criterion_type0[0].addHotelRef(hotelRef_type0);

	}

	TPA_ExtensionsType tPA_ExtensionsType = new TPA_ExtensionsType();

	ReservationDetails_type0 reservationDetails_type0 = new ReservationDetails_type0();

	DropOff_type0 dropOff_type0 = new DropOff_type0();
	StringLength1To255 stringLength1To255 = new StringLength1To255();
	stringLength1To255.setStringLength1To255("Domestic");
	dropOff_type0.setShipCompany(stringLength1To255);
	reservationDetails_type0.setDropOff(dropOff_type0);

	PickUp_type0 pickUp_type0 = new PickUp_type0();
	StringLength1To255 stringLength1To2552 = new StringLength1To255();
	stringLength1To2552.setStringLength1To255("International");
	pickUp_type0.setShipCompany(stringLength1To2552);
	reservationDetails_type0.setPickUp(pickUp_type0);

	tPA_ExtensionsType.setReservationDetails(reservationDetails_type0);

	SortResults_type0 sortResults_type0ResultsType0 = new SortResults_type0();
	// sortResults_type0ResultsType0.setSortBy(SortBy_type0.PRICE );
	// sortResults_type0ResultsType0.setSortOrder(SortOrder_type0.ASC);
	sortResults_type0ResultsType0.setSortBy(SortBy_type0.ASC);
	sortResults_type0ResultsType0.setSortOrder(SortOrder_type0.PRICE);
	tPA_ExtensionsType.addSortResults(sortResults_type0ResultsType0);

	if (PropertySingleton.getInstance().getProperty("XmlOut.Server").equalsIgnoreCase("staging")) {

		ReturnResultsType returnResultsType = new ReturnResultsType();
		returnResultsType.setDescription(true);
		returnResultsType.setContract(true);
		returnResultsType.setRoomTypes(!search.isSingleHotelSearch());
		returnResultsType.setImages(true);
		tPA_ExtensionsType.setReturnResults(returnResultsType);

	}

	/*
	 * FilterResultsType filterResultsType = new FilterResultsType(); filterResultsType.setFilter(Filter_type0.HotelNameFilter); filterResultsType.setStringLength0To255("H12");
	 * tPA_ExtensionsType.addFilterResults(filterResultsType );
	 */

	FilterResultsType filterResultsType = new FilterResultsType();
	filterResultsType.setFilter(Filter_type0.AvailableHotelFilter);
	filterResultsType.setStringLength0To255(search.getHotelID());
	tPA_ExtensionsType.addFilterResults(filterResultsType);

	/*
	 * FilterResultsType filterResultsType = new FilterResultsType(); filterResultsType.setFilter(Filter_type0.StarCategoryFilter); filterResultsType.setStringLength0To255("3");
	 * tPA_ExtensionsType.addFilterResults(filterResultsType );
	 * 
	 * 
	 * FilterResultsType filterResultsType2 = new FilterResultsType(); filterResultsType2.setFilter(Filter_type0.HotelNameFilter); filterResultsType2.setStringLength0To255("Test Hotel");
	 * tPA_ExtensionsType.addFilterResults(filterResultsType2 );
	 */

	// criterion_type0[0].setTPA_Extensions(tPA_ExtensionsType);

	hotelSearchCriteriaType.setCriterion(criterion_type0);
	SearchRQ.setCriteria(hotelSearchCriteriaType);

	SearchRQ.setVersion(new BigDecimal(3.0));
	
	return SearchRQ;
	
}

}
