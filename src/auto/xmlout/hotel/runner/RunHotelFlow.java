package auto.xmlout.hotel.runner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import auto.xmlout.com.types.ContractType;
import auto.xmlout.com.utilities.ReadExcel;
import auto.xmlout.hotel.loaders.HotelDataLoader;
import auto.xmlout.hotel.loaders.PropertySingleton;
import auto.xmlout.hotel.loaders.SearchDataLoader;
import auto.xmlout.hotel.pojo.GeneratedAvailabilityResponse;
import auto.xmlout.hotel.pojo.GeneratedCancellationResponse;
import auto.xmlout.hotel.pojo.GeneratedHotelInfoResponse;
import auto.xmlout.hotel.pojo.GeneratedReservationResponse;
import auto.xmlout.hotel.pojo.GeneratedSearchResponse;
import auto.xmlout.hotel.pojo.RoomType;
import auto.xmlout.hotel.pojo.SearchType;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.hotel.pojo.XmlOutError;
import auto.xmlout.hotel.processors.ResponseProcessors;
import auto.xmlout.hotel.reqbuilders.HotelRequestBuilder;
import auto.xmlout.hotel.reqbuilders.ReservationStubBuilder;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelResRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelResRQ;

public class RunHotelFlow {

	private ReservationServiceStub stub = null;
	private HashMap<String, SearchType> SearchList = null;
	// private Map<String, String> CurrencyMap = null;
	private Logger logger = null;
	private TreeMap<String, Hotel> HotelList = null;

	private ArrayList<Map<Integer, String>> SearchSheetList = null;
	private ArrayList<Map<Integer, String>> HotelSheetList = null;

	@Before
	public void setUp() throws Exception {

		DOMConfigurator.configure("log4j.xml");
		logger = Logger.getLogger(this.getClass());

		try {
			ReadExcel readExcel = new ReadExcel();
			logger.info("Initializing Excel WorkBooks -");
			logger.info("WorkBooks Paths -Search Excel---> " + PropertySingleton.getInstance().getProperty("XmlOut.SearchExcelPath") + "-Hotel Excel-->" + PropertySingleton.getInstance().getProperty("XmlOut.HotelExcelPath"));
			SearchSheetList = readExcel.init(PropertySingleton.getInstance().getProperty("XmlOut.SearchExcelPath"));
			HotelSheetList = readExcel.init(PropertySingleton.getInstance().getProperty("XmlOut.HotelExcelPath"));
		} catch (Exception e) {
			logger.fatal("Error when initializing the Excel WorkBooks -", e);
			throw new Exception("Error when initializing the Excel WorkBooks");
		}

		try {
			HotelDataLoader Hotelloader = new HotelDataLoader();
			HotelList = Hotelloader.getHotelData(HotelSheetList);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error when initializing the Hotel Details");
		}

		try {
			SearchDataLoader SearchLoder = new SearchDataLoader(SearchSheetList.get(3), PropertySingleton.getInstance().getProperty("Xmlout.PortalCurrency"));
			SearchList = (HashMap<String, SearchType>) SearchLoder.getSearchDetails(SearchSheetList, HotelList);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error when initializing the Search Details");
		}

		System.setProperty("javax.net.ssl.trustStore", "C:/rezsystem/jboss-4.0.3SP1/bin/certs/QA.keystore");
		System.setProperty("javax.net.ssl.trustStorePassword", "123456");

	}

	@Test
	public void run() throws Exception {

		String Url = PropertySingleton.getInstance().getProperty("XmlOut.Url");
		String Username = PropertySingleton.getInstance().getProperty("XmlOut.UserName");
		String Pass = PropertySingleton.getInstance().getProperty("XmlOut.PassWord");

		logger.info("Building Reservation Stub using  Url---->" + Url + " User--->" + Username + " Pass--->" + Pass);

		logger.info("============Building Reservation Stub=====================");
		ReservationStubBuilder StubBuilder = new ReservationStubBuilder(Url, Username, Pass);
		try {
			stub = StubBuilder.buildReservationServiceStub();
		} catch (Exception e) {
			logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
			throw new Exception();
		}

		Iterator<Map.Entry<String, SearchType>> ReservationScenarioItr = SearchList.entrySet().iterator();
		while (ReservationScenarioItr.hasNext()) {
			HotelRequestBuilder ReqBuilder = new HotelRequestBuilder();
			Map.Entry<java.lang.String, auto.xmlout.hotel.pojo.SearchType> entry = (Map.Entry<java.lang.String, auto.xmlout.hotel.pojo.SearchType>) ReservationScenarioItr.next();
			SearchType CurrentSearch = entry.getValue();
			OTA_HotelSearchRQ Search = null;
			ResponseProcessors Processor = new ResponseProcessors();
			// Search

			if (CurrentSearch.isScenarioExcecutionState()) {
				logger.info("================= Building the search request=============== ");
				try {
					Search = ReqBuilder.buildHotelSearchRequest(CurrentSearch);
				} catch (Exception e) {
					logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
					throw new Exception();
				}

				logger.info("================= Sending  the search request================ ");

				OTA_HotelSearchRS SearchRS = stub.HotelSearch(Search);

				logger.info("================= Processing the search response==================== ");

				GeneratedSearchResponse ServiceResponse = Processor.processSearchResponse(SearchRS);

				logger.info("Results Available : " + ServiceResponse.isResultsAvailable());
				logger.info("Number of errors :" + ServiceResponse.getErrors().size());

				if (ServiceResponse.getErrors().size() > 0) {
					ArrayList<XmlOutError> err = ServiceResponse.getErrors();
					for (Iterator<XmlOutError> iterator = err.iterator(); iterator.hasNext();) {
						XmlOutError xmlOutError = (XmlOutError) iterator.next();
						logger.info("Error Code :" + xmlOutError.getErrorCode());
						logger.info("Error Message :" + xmlOutError.getErrorDescription());
					}
				}

				if (ServiceResponse.isResultsAvailable()) {
					CurrentSearch.setAvailabilityTracer(ServiceResponse.getTransactionIdentifier().toString().trim());
					logger.info(ServiceResponse.getNumberOfHotels());
					logger.info("Hotels Available-->" + ServiceResponse.getNumberOfHotels());
					logger.info("Availability Transaction ID-->" + CurrentSearch.getAvailabilityTracer());

					Hotel SubjectedHotel = null;
					if (CurrentSearch.getHotelContract() == ContractType.INTERNAL) {

						ArrayList<Hotel> ResultsHotelList = ServiceResponse.getResponseHotelList();
						Iterator<Hotel> itr = ResultsHotelList.iterator();

						Whileloop: while (itr.hasNext()) {
							Hotel hotel = (Hotel) itr.next();
							logger.info("Hotel Name -->" + CurrentSearch.getHotelName().trim() + " ?" + hotel.getHotelName());
							if (hotel.getHotelName().equalsIgnoreCase(CurrentSearch.getHotelName().trim())) {
								logger.info("Requested Hotel Available -->" + CurrentSearch.getHotelName());
								SubjectedHotel = hotel;
								logger.info("Hotel Code  ------->" + SubjectedHotel.getHotelCode());
								logger.info("Hotel Currency ---->" + SubjectedHotel.getHotelCurrency());
								logger.info("Hotel Address  ------->" + SubjectedHotel.getAddressLine1());
								logger.info("Hotel City ---->" + SubjectedHotel.getCity());
								logger.info("Hotel Vendor  ------->" + SubjectedHotel.getHotelVendor());
								logger.info("Hotel Country ---->" + SubjectedHotel.getCountry());
								logger.info("Hotel Latitude  ------->" + SubjectedHotel.getLatitude());
								logger.info("Hotel Longitude ---->" + SubjectedHotel.getLongitude());

								Map<String, ArrayList<RoomType>> Rooms = SubjectedHotel.getResultsRooms();
								Iterator<Map.Entry<String, ArrayList<RoomType>>> itra = Rooms.entrySet().iterator();

								while (itra.hasNext()) {
									Map.Entry<java.lang.String, java.util.ArrayList<auto.xmlout.hotel.pojo.RoomType>> entry2 = (Map.Entry<java.lang.String, java.util.ArrayList<auto.xmlout.hotel.pojo.RoomType>>) itra.next();
									ArrayList<RoomType> rmlist = entry2.getValue();
									String Roomss = "-";

									for (Iterator<RoomType> iterator = rmlist.iterator(); iterator.hasNext();) {
										RoomType roomType = (RoomType) iterator.next();
										if (Roomss.equalsIgnoreCase("-"))
											Roomss = roomType.getRoomType();
										else
											Roomss = Roomss + "," + roomType.getRoomType();
									}
									logger.info("Room " + entry2.getKey() + " Rooms ---->" + Roomss);
								}

								break Whileloop;
							}

						}
					} else if (CurrentSearch.getHotelContract() == ContractType.THIRDPARTY) {

						logger.info("Hotel Vendor to be selected ----->" + CurrentSearch.getHotelVendor());

						try {
							ArrayList<Hotel> VendorHotels = ServiceResponse.getVendorHotelMap().get(CurrentSearch.getHotelVendor().trim());
							SubjectedHotel = VendorHotels.get(0);
							logger.info(SubjectedHotel.getHotelName() + " Hotel Selected");
							CurrentSearch.setHotelName(SubjectedHotel.getHotelName());
						} catch (Exception e) {
							logger.info(CurrentSearch.getHotelVendor() + " Vendor Hotels not available");
						}

					}
					if (SubjectedHotel != null) {

						try {
							Processor.fillAvailabilityDetails(CurrentSearch, SubjectedHotel);
							OTA_HotelDescriptiveInfoRQ oTA_HotelDescriptiveInfoRQ = new OTA_HotelDescriptiveInfoRQ();
							OTA_HotelDescriptiveInfoRS oTA_HotelDescriptiveInfoRS = new OTA_HotelDescriptiveInfoRS();

							logger.info("===== Building the hotel info request======");
							try {
								oTA_HotelDescriptiveInfoRQ = ReqBuilder.buildHotelInfoRequest(CurrentSearch);
							} catch (Exception e) {
								logger.fatal("Building the hotel info failed - Skipping the hotel info check test suit", e);
								// throw new Exception();
							}

							logger.info("=== Sending  the hotel info request===");

							oTA_HotelDescriptiveInfoRS = stub.HotelInfo(oTA_HotelDescriptiveInfoRQ);

							logger.info("===== Processing the hotel info response==");

							GeneratedHotelInfoResponse HotelInfoResponse = Processor.processHotelInfoResponse(oTA_HotelDescriptiveInfoRS);
							System.out.println(oTA_HotelDescriptiveInfoRS.getTransactionIdentifier());

							if (HotelInfoResponse.getErrors().size() > 0) {
								ArrayList<XmlOutError> err = HotelInfoResponse.getErrors();
								for (Iterator<XmlOutError> iterator = err.iterator(); iterator.hasNext();) {
									XmlOutError xmlOutError = (XmlOutError) iterator.next();
									logger.info("Error Code :" + xmlOutError.getErrorCode());
									logger.info("Error Message :" + xmlOutError.getErrorDescription());
								}
							}
							
					
						} catch (Exception e1) {
							logger.fatal("Problem with Hotel Info test - Skipping the hotel info check test suit", e1);
					
						}

						// Hotel Availability request goes here

						OTA_HotelAvailRS oTA_HotelAvailRS = new OTA_HotelAvailRS();
						OTA_HotelAvailRQ oTA_HotelAvailRQ = new OTA_HotelAvailRQ();
					//	Processor.fillAvailabilityDetails(CurrentSearch, SubjectedHotel);

						logger.info("================= Building the availability request=============== ");
						try {
							oTA_HotelAvailRQ = ReqBuilder.buildHotelAvailRequest(CurrentSearch);
						} catch (Exception e) {
							logger.fatal("Building the availability request failed - Treminating the test suit", e);
							throw new Exception();
						}

						logger.info("================= Sending  the availability request====================== ");

						oTA_HotelAvailRS = stub.HotelAvailability(oTA_HotelAvailRQ);

						logger.info("================= Processing the Availability response==============");

						GeneratedAvailabilityResponse AvailResponse = Processor.processAvailabilityResponse(oTA_HotelAvailRS);
						System.out.println(oTA_HotelAvailRS.getTransactionIdentifier());

						if (AvailResponse.getErrors().size() > 0) {
							ArrayList<XmlOutError> err = AvailResponse.getErrors();
							for (Iterator<XmlOutError> iterator = err.iterator(); iterator.hasNext();) {
								XmlOutError xmlOutError = (XmlOutError) iterator.next();
								logger.info("Error Code :" + xmlOutError.getErrorCode());
								logger.info("Error Message :" + xmlOutError.getErrorDescription());
							}
						}

						if (AvailResponse.isHotelAvailable()) {

							OTA_HotelResRQ oTA_HotelResRQ = new OTA_HotelResRQ();
							OTA_HotelResRS oTA_HotelResRS = new OTA_HotelResRS();
							CurrentSearch.setAvailabilityTracer(AvailResponse.getTransactionIdentifier().trim());

							logger.info("================= Building the reservation request=============== ");
							try {
								oTA_HotelResRQ = ReqBuilder.buildHotelReservationRequest(CurrentSearch);
							} catch (Exception e) {
								logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
								throw new Exception();
							}

							logger.info("================= Sending  the reservation request ====================== ");

							oTA_HotelResRS = stub.HotelReservation(oTA_HotelResRQ);

							logger.info("================= Processing the reservation response==============");

							GeneratedReservationResponse ReservationResponse = Processor.processReservationResponse(oTA_HotelResRS);

							if (ReservationResponse.isConfirmationSuccess()) {
								CurrentSearch.setServiceID(ReservationResponse.getServiceID());
								logger.info("Reservation No:---->" + ReservationResponse.getConfirmationNumber());
								logger.info("Service ID:---->" + ReservationResponse.getServiceID());
								logger.info("SupplierReferenceMessage:---->" + ReservationResponse.getSupplierReferenceMessage());
								logger.info("BookingConfirmationDetail:---->" + ReservationResponse.getBookingConfirmationDetail());
								logger.info("TripSummary:---->" + ReservationResponse.getTripSummary());
								logger.info("CustomerContactDetails:---->" + ReservationResponse.getCustomerContactDetails());
								logger.info("TripSummaryBookingStatus:---->" + ReservationResponse.getTripSummaryBookingStatus());
								logger.info("EstimatedArrivalTime:---->" + ReservationResponse.getEstimatedArrivalTime());
								logger.info("TripSummaryCheckIn:---->" + ReservationResponse.getTripSummaryCheckIn());
								logger.info("TripSummaryCheckOut:---->" + ReservationResponse.getTripSummaryCheckOut());
								logger.info("TotalRate:---->" + ReservationResponse.getTotalRate());
								logger.info("RateCurrency:---->" + ReservationResponse.getRateCurrency());
							} else {
								if (ReservationResponse.getErrors().size() > 0) {
									ArrayList<XmlOutError> err = ReservationResponse.getErrors();
									for (Iterator<XmlOutError> iterator = err.iterator(); iterator.hasNext();) {
										XmlOutError xmlOutError = (XmlOutError) iterator.next();
										logger.info("Error Code :" + xmlOutError.getErrorCode());
										logger.info("Error Message :" + xmlOutError.getErrorDescription());
									}
								}
							}

							if (CurrentSearch.isCancellationNeeded() && ReservationResponse.isConfirmationSuccess()) {

								OTA_CancelRQ oTA_CancelRQ = new OTA_CancelRQ();
								OTA_CancelRS oTA_CancelRS = new OTA_CancelRS();

								logger.info("================= Building the cancellation request=============== ");
								try {
									oTA_CancelRQ = ReqBuilder.buildHotelCancellatioRequest(CurrentSearch);
								} catch (Exception e) {
									logger.fatal("Building the cancellation request failed - Treminating the test suit", e);
									throw new Exception();
								}

								logger.info("================= Sending  the cancellation request  ===============");

								oTA_CancelRS = stub.HotelCancel(oTA_CancelRQ);

								logger.info("================= Processing the cancellation response==============");

								GeneratedCancellationResponse CancellationResponse = Processor.processCancellationResponse(oTA_CancelRS);

								if (CancellationResponse.isCancellationSuccess()) {
									logger.info("Cancellation Success !!!");
									logger.info("Cancellation Number :" + CancellationResponse.getCancellationNumber());
									logger.info("Cancellation Service ID :" + CancellationResponse.getServiceID());
									logger.info("Supplier Conf ID :" + CancellationResponse.getSupplierConfirmation());
									logger.info("Cancellation Comment:" + CancellationResponse.getComment());
								}

							}

						} else {
							logger.info("Hotel Availability Failed for the hotel " + CurrentSearch.getHotelName());
						}
					} else {
						logger.info("Request Hotel " + CurrentSearch.getHotelName() + " Not Available Within the results ");
					}

				}

			} else {
				logger.info("Scenario--->" + entry.getKey() + "  Skipped ");
			}

		}

	}

	@After
	public void tearDown() {

	}

}
