package auto.xmlout.hotel.runner;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.databinding.types.NonNegativeInteger;
import org.apache.axis2.databinding.types.PositiveInteger;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import xmlout.org.opentravel.www.ota._2003._05.*;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AddressType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Address_type2;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AirItineraryType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AirTraveler_type2;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AirTripType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AlphaLength3;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AlphaLength4;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ArrivalAirport_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.BldgRoom_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinClass_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinPref_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CabinType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CompanyNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CountryNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CustLoyalty_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DateTimeInstant;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DemandTicketDetail_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DepartureAirport_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DestinationLocation_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.EmailType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FlightNumberType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FlightNumberType_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FlightSegmentType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FlightSegment_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FlightTypePref_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FlightTypeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FulfillmentType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ISO3166;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.InfoSourceType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ListOfRPH;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.MMYYDate;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.MessageFunction_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.NumericStringLength1To19;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.NumericStringLength1To3;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.NumericStringLength1To8;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirBookRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirBookRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirDemandTicketRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirDemandTicketRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirLowFareSearchRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirLowFareSearchRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirPriceRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirPriceRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirRulesRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirRulesRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirSeatMapRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_AirSeatMapRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_ReadRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OriginDestinationInformation_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OriginDestinationOption_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OriginDestinationOptions_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OriginLocation_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.POS_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PassengerTypeQuantityType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentCardCodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentCardType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentDetail_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentDetails_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PersonNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PriceInfo_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PriceRequestInformation_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PricingSourceType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Queue_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RPH_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Remark_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Remarks_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RequestorID_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResBookDesignation_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResBookDesignations_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SeatDetails_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SeatMapRequest_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SeatMapRequests_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SeatRequest_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SeatRequests_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SourceType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SpecialRemark_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SpecialRemarks_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SpecialReqDetailsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SpecialServiceRequest_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SpecialServiceRequests_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StateProvCodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StateProvType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StreetNmbrType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To128;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To16;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To32;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To64;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To8;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TPA_ExtensionsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type10;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type20;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type24;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type26;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type28;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type30;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type32;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Telephone_type3;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TicketType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Ticketing_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TravelPreferences_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TravelerInfoSummary_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TravelerInfoSummary_type2;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TravelerInfoSummary_type3;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TravelerInfoType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TravelerInformationType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TravelerRefNumber_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UniqueID_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UniqueID_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UniqueID_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UpperCaseAlphaLength1To2;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.VendorPref_type0;

/**
 * @author sameera
 * @modifed prageeth
 * 
 */
public class rezwsr_soap_air_test {

	public rezwsr_soap_air_test() {
		logger = Logger.getLogger(this.getClass());
	}

	public static void main(String[] args) {

//		DOMConfigurator.configure("/rezsystem/Workspace_GIT/RezgxmlClient/log4j.xml");
		rezwsr_soap_air_test rezwsr_soap_air_test = new rezwsr_soap_air_test();
		rezwsr_soap_air_test.callserver();

	}

	public void callserver() {
		/**
		 * @warning --------------- Common Errors -------------------- Error : SOAP header missing - Check rampart added or not Error : Rampart exception - Check null values in the response
		 *          --------------------------------------------------
		 * */

		// server = "live";
		server = "staging";
		System.out.println("server:" + server);

		if (server.equals("live")) {

			air_search_Target = Target_type20.Production;

			// url = "http://xml.secure-reservation.com/axis2/services/ReservationService"; // MC6 live

			/* Tripserve - Live */
			// user = "TRIP_LIVE_3"; //
			// username = "CCJourneys"; //
			// password = "123456"; //

			// user = "flightsitev3";// "FS_TOUR_1"/*AIR1/AIR3*/;//"flightsite"; //
			// username = "flightsite1"; // "flightsite";
			// password = "flightsite123"; //""123456";//"flightsitev3"/*AIR1/AIR3*/; //

		} else if (server.equals("staging")) {

			air_search_Target = Target_type20.Test;

			// url = "http://192.168.1.135:9090/axis2/services/ReservationService"; /* My machine */
			// url = "http://xml-test.secure-reservation.com/axis2/services/ReservationService"; // MC6 Stg
			url = "http://192.168.1.126:9090/axis2/services/ReservationService";
		//	url = "http://localhost:9090/axis2/services/ReservationService";
			//url = "http://192.168.1.14:9090/axis2/services/ReservationService";	// Harsha
			//url = "http://192.168.1.234:9090/axis2/services/ReservationService";	// Dharshan
			////###Dev server login#############
			user = "REZBASE_V3_TEST_DEV";
			username = "charakaxmlout";
			password = "123456";

			
/*			user = "REZBASE_V3_TEST_CHARITHA";
			username = "charithato";
			password = "123456";*/

			
			/*
			 * user = "FS_TOUR_1"; //"trips_test"; username = "flightsite"; //"tripstest"; password = "fs123"; //"rezgateway";
			 */
		}

		echoTokenMain = "Default"; // "ExternalFlights"/*RIDE*/;//
		System.out.println("echoTokenMain:" + echoTokenMain);

		isMultipleAIREnable = false; /* When two or more AIR vendors are there */
		isRemarkEnabled = false; /* Pay@ payment remark */
		isOneWay = false; /* When doing a oneway flight/ so segments will be dropped */
		isNonstop = false; /* For NONSTOP flights */
		isMulticity = false;

		System.out.println("** user--" + user + " | username--" + username + " | password--" + password);

		System.out.println("isMultipleAIREnable:" + isMultipleAIREnable + ", isNonstop:" + isNonstop);
		if (isMultipleAIREnable) {
			/**
			 * If there are multiple airline vendors and Travelfusion enabled we set these properties
			 */
			if (isOneWay) { // ONE WAY ONLY
				/* QuoteID */
				routingIds = "TRAVELFUSION@K182V22VTQ1THHKB"; /* QuoteID */
				correlationID = "Amadeus-RUHS2214J";

				inoutids = new String[1];
				inoutids[0] = "3L571BM0I8RCEPVY"; /* InfoSource */

			} else {
				/* QuoteID */
				routingIds = "TRAVELFUSION@81PT7UWEXO35Z7SR";// "TRAVELFUSION@C1U8TQC0S5XRGH65@D1IY7D21ABGZ6Y12";
				correlationID = "Amadeus-RUHS2214J";

				inoutids = new String[2];
				inoutids[0] = "T0U60UL1LL8BYX7O";//
				inoutids[1] = "FN96IG8NFAGMVWDO"; //
			}
			officeId = "IAH1S3100";

		} else {
			correlationID = "Amadeus-IAH1S3100";// "Amadeus-IAH1S3100"; //
			officeId = "MSP1S212L"; // "IAH1S3100"; //
		}



		// ---------------------------------------------------

		// ================ AIR PRICE ==========================

		// correlationID = "TRAVELFUSION@F1TZCTTFAI9OSM45";//"Amadeus-IAH1S3100"; // "TRAVELFUSION@LF4UFIRC11TMWAVS@JO73I6BR234UVJTG";// // The office id - This is needed for the price call
		// if(isOneWay){
		// directionIndicator = AirTripType.value1;
		// }else{
		// /*Circle*/
		// AirTripType.value1
		// }
		//################################################### Depature & Arrival info ###########
		
		// ================ For search lowfare ==============-
		searchDepatingDate = "2015-09-28T00:00:00"; // ONE WAY ONLY
		System.out.println("searchDepatingDate:" + searchDepatingDate);

		System.out.println("isOneWay:" + isOneWay);
		if (!isOneWay) { // Circle
			searchSecondDeptDate = "2015-09-30T00:00:00";
		}
		System.out.println("searchSecondDeptDate:" + searchSecondDeptDate);

		System.out.println("isMulticity:" + isMulticity);
		if (isMulticity) {
			depAC3 = "SIN";
			arrAC3 = "DXB";

			searchThirdDeptDate = "2015-09-20T00:00:00";
		}
		
		
		//#########################Used when search not One Way#################### 
		depAC1 = "DXB";
		departureDate1 = "2015-09-17T13:45:00";
		arrAC1 = "DOH";
		arrivalDate1 = "2014-08-19T07:10:00";
		System.out.println("depAC1:" + depAC1 + ", departureDate1:" + departureDate1 + ", arrAC1:" + arrAC1 + ", arrivalDate1:" + arrivalDate1);
		// --------------- ---------------------
		if (!isOneWay) { // Circle
			depAC2				= "DOH";// Departure Airport of Second trip
			departureDate2	 	= "2015-09-29T11:40:00";
			arrAC2				= "DXB"; // Return Airport of Second trip
			arrivalDate2	 	= "2015-09-30T11:40:00";
		}
		System.out.println("depAC2:" + depAC2 + ", departureDate2:" + departureDate2 + ", arrAC2:" + arrAC2 + ", arrivalDate2:" + arrivalDate2);
		//############################################################################################
		if (isMulticity) {	
			// 3rd Dest

			departureDate3 = "2015-08-27T05:45:00";
			arrivalDate3 = "2015-08-27T07:45:00";
		}

		if (isOneWay) {

			ResBookDesigCode = new String[1];
			ResBookDesigCode[0] = "U";	// Seat class
			// ------------------------------------------------
			flightnum = new String[1];
			flightnum[0] = "348";
			// ------------------------------------------------
			airlineCode = new String[1];
			airlineCode[0] = "EK";
			// ------------------------------------------------
			airlineName = new String[1];
			airlineName[0] = "Emirates";

		} else {

			//################ Connecting  Flight########## 
			ResBookDesigCode = new String[4];
			ResBookDesigCode[0] = "W";// Seat class
			ResBookDesigCode[1] = "W";
			
			ResBookDesigCode[2] = "W";// Seat class
			ResBookDesigCode[3] = "W";
			// ------------------------------------------------
			flightnum = new String[4];
			flightnum[0] = "509";
			flightnum[1] = "530";
			
			flightnum[2] ="527";
			flightnum[3] = "512";	
			
			// ------------------------------------------------
			airlineCode = new String[4];
			airlineCode[0] = "GF";
			airlineCode[1] = "GF";
			
			airlineCode[2] = "GF";
			airlineCode[3] = "GF";
			// ------------------------------------------------
			airlineName = new String[4];
			airlineName[0] = "Gulf Air";
			airlineName[1] = "Gulf Air";
			
			airlineName[2] = "Gulf Air";
			airlineName[3] = "Gulf Air";
			
			//################ Connecting  Flight########## 
			
			
			
			
			
			//################ Direct Flight########## 
/*			ResBookDesigCode = new String[2];
			ResBookDesigCode[0] = "V";// Seat class
			ResBookDesigCode[1] = "N";
			

			// ------------------------------------------------
			flightnum = new String[2];
			flightnum[0] = "669";
			flightnum[1] = "664";

			
			// ------------------------------------------------
			airlineCode = new String[2];
			airlineCode[0] = "QR";
			airlineCode[1] = "QR";		

			// ------------------------------------------------
			airlineName = new String[2];
			airlineName[0] = "Qatar Airways";
			airlineName[1] = "Qatar Airways";	*/		
			//################ Direct Flight########## 
			
		}

		if (isMulticity) {
			// 3rd Dest

			ResBookDesigCode = new String[2];
			ResBookDesigCode[0] = "G";	// Seat class
			ResBookDesigCode[1] = "G";
			// ------------------------------------------------
			flightnum = new String[2];
			flightnum[0] = "333";
			flightnum[1] = "378";
			// ------------------------------------------------
			airlineCode = new String[2];
			airlineCode[0] = "SA";
			airlineCode[1] = "SA";
			// ------------------------------------------------
			airlineName = new String[2];
			airlineName[0] = "South African Airways";
			airlineName[1] = "South African Airways";
		}

		/* Number of the flight */
		flightSequence = "2";

		/* Transaction */
		transactionid = "318f9dc4-63c0-43d6-9ef4-3ab64e2405d3";
		Reservationnumber = "F4491W200815";
		// ===================RESERVATION======================
		ticketingTimeLimit = "2014-02-24T23:59:00";
		companyname = "TripServe";// "FS";//
		packageId = "110000000000000";	// Tripserve package reference number

		// ====================================================
		SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MMM-dd hh:mm:ss:SSS");

		long smili = System.currentTimeMillis();
		String stime = sdf.format(new java.util.Date());
		long emili = 0;
		String etime = "";
		double tooktime = 0.0;

		callLowFareSearch(); /* 1 */ // Returns air itenararies
       // callairPrice(); /* 2 */ // Return : actual price
	//	callReservation(); /* 3 */ // Return : PNR number
		// airSeatMap(); /* 4 */ // Return : PNR + some small value set
		//callCancel(); /* 5 */
		// eticket(); /* 6 */ // Return : Successful

		// callAirRules(); /* 7 */ // Return : Rules applied to booking and passengers
		// pnrDownload(); /* 8 */ // Return : full details of the booking

		emili = System.currentTimeMillis();
		etime = sdf.format(new java.util.Date());
		tooktime = (emili - smili) / 1000.0;

		System.out.println("*** Air search Took " + tooktime + " Seconds - Started - " + stime + " End - " + etime);

	}

	public void callLowFareSearch() {
		System.out.println("start callLowFareSearch()");
		try {
			// Rampart module should be in the repository
			/*ConfigurationContext ctx = ConfigurationContextFactory
				.createConfigurationContextFromFileSystem(CommonUtil.getConfigurationFilePath1(), CommonUtil.getConfigurationFilePath2());*/
			
			ConfigurationContext ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem("D:/xmloutlibs/Axis2/axis2-1.4.1/repository", "D:/xmloutlibs/Axis2/axis2-1.4.1/conf/axis2.xml");
			ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
			ServiceClient sc = stub._getServiceClient();
			sc.engageModule("rampart");
			// sc.engageModule("xmloutlogging");
			Options options = sc.getOptions();
			options.setUserName(username);
			options.setPassword(password);
			options.setTimeOutInMilliSeconds(1000 * 60 * 4);

			// HttpTransportProperties.ProxyProperties proxyProperties = new HttpTransportProperties.ProxyProperties();
			// proxyProperties.setProxyName("192.168.1.229");
			// proxyProperties.setProxyPort(3128);
			// proxyProperties.setDomain("192.168.1.229");
			// proxyProperties.setUserName("xmluser");
			// proxyProperties.setPassWord("123456");
			//
			// options.setProperty(org.apache.axis2.transport.http.HTTPConstants.PROXY,proxyProperties);
			// options.setProperty(HTTPConstants.CHUNKED,false);

			OTA_AirLowFareSearchRQ oTA_AirLowFareSearchRQ = new OTA_AirLowFareSearchRQ();

			Calendar calendar = Calendar.getInstance();
			oTA_AirLowFareSearchRQ.setTimeStamp(calendar);

			oTA_AirLowFareSearchRQ.setTarget(air_search_Target);
			oTA_AirLowFareSearchRQ.setVersion(new BigDecimal(1.0));
			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128(echoTokenMain);
			oTA_AirLowFareSearchRQ.setEchoToken(echotoken);
			POS_Type pOS_Type = new POS_Type();

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(user);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			// OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("ZAR");
			sourceType[0].setISOCurrency(alphaLength3);
			pOS_Type.setSource(sourceType);

			oTA_AirLowFareSearchRQ.setPOS(pOS_Type);

			OriginDestinationInformation_type0 originDestinationInformation_type0 = new OriginDestinationInformation_type0();

			DateTimeInstant timeInstantType = new DateTimeInstant();

			/*
			 * String [] ids = TimeZone.getAvailableIDs(); for (int i = 0; i < ids.length; i++) { System.out.println(ids[i]); }
			 */

			timeInstantType.setString(searchDepatingDate);

			originDestinationInformation_type0.addDepartureDateTime(timeInstantType);
			System.out.println("Deptime : " + timeInstantType);

			OriginLocation_type0 originLocation_type0 = new OriginLocation_type0();
			StringLength1To16 originLocCode = new StringLength1To16();
			originLocCode.setStringLength1To16(depAC1); // Airpot code
			System.out.println("origin airport code:" + depAC1);
			originLocation_type0.setLocationCode(originLocCode);
			originDestinationInformation_type0.setOriginLocation(originLocation_type0);

			DestinationLocation_type0 destinationLocation_type0 = new DestinationLocation_type0();
			StringLength1To16 destLocCode = new StringLength1To16();
			destLocCode.setStringLength1To16(arrAC1); // Airpot code
			System.out.println("destination airport code:" + arrAC1);
			destinationLocation_type0.setLocationCode(destLocCode);
			originDestinationInformation_type0.setDestinationLocation(destinationLocation_type0);
			oTA_AirLowFareSearchRQ.addOriginDestinationInformation(originDestinationInformation_type0);

			/****************** build return destination *********************/
			if (!isOneWay) { // -------> NOT ONEWAY
				DateTimeInstant timeInstantType1 = new DateTimeInstant();
				timeInstantType1.setString(searchSecondDeptDate);
				System.out.println("second Deptime : " + timeInstantType);
				// timeInstantType1.setString(departureDate2);
				OriginDestinationInformation_type0 originDestinationInformation_type01 = new OriginDestinationInformation_type0();
				OriginLocation_type0 originLocation_type01 = new OriginLocation_type0();
				originDestinationInformation_type01.addDepartureDateTime(timeInstantType1);
				StringLength1To16 originLocCode1 = new StringLength1To16();
				// originLocCode1.setStringLength1To16("CMB");
				originLocCode1.setStringLength1To16(depAC2);
				System.out.println("second origin airport code:" + depAC2);
				originLocation_type01.setLocationCode(originLocCode1);
				originDestinationInformation_type01.setOriginLocation(originLocation_type01);

				DestinationLocation_type0 destinationLocation_type01 = new DestinationLocation_type0();
				StringLength1To16 destLocCode1 = new StringLength1To16();
				// destLocCode1.setStringLength1To16("SYD");
				destLocCode1.setStringLength1To16(arrAC2);
				System.out.println("second destination airport code:" + depAC2);
				destinationLocation_type01.setLocationCode(destLocCode1);
				originDestinationInformation_type01.setDestinationLocation(destinationLocation_type01);
				oTA_AirLowFareSearchRQ.addOriginDestinationInformation(originDestinationInformation_type01);
			} // -------> NOT ONEWAY

			// 3rd destination
			/*
			 * Date date3 = new Date(); DateTimeInstant timeInstantType3 = new DateTimeInstant();
			 * 
			 * timeInstantType3.setString(searchThirdDeptDate); OriginDestinationInformation_type0 originDestinationInformation_type03 = new OriginDestinationInformation_type0(); OriginLocation_type0
			 * originLocation_type03 = new OriginLocation_type0(); originDestinationInformation_type03.addDepartureDateTime(timeInstantType3); StringLength1To16 originLocCode3 = new
			 * StringLength1To16(); originLocCode3.setStringLength1To16(depAC3); originLocation_type03.setLocationCode(originLocCode3);
			 * originDestinationInformation_type03.setOriginLocation(originLocation_type03);
			 * 
			 * DestinationLocation_type0 destinationLocation_type03 = new DestinationLocation_type0(); StringLength1To16 destLocCode3 = new StringLength1To16();
			 * destLocCode3.setStringLength1To16(arrAC3); destinationLocation_type03.setLocationCode(destLocCode3);
			 * originDestinationInformation_type03.setDestinationLocation(destinationLocation_type03); oTA_AirLowFareSearchRQ.addOriginDestinationInformation(originDestinationInformation_type03);
			 */

			// Set TravelPref **********
			TravelPreferences_type0[] travelPreferences_type0 = new TravelPreferences_type0[1];
			travelPreferences_type0[0] = new TravelPreferences_type0();
		//	travelPreferences_type0[1] = new VendorPref_type0();
			
			// set cabin type			
			CabinPref_type0[] cabinPref = new CabinPref_type0[1];
			cabinPref[0] = new CabinPref_type0();			
			cabinPref[0].setCabin(CabinType.Economy);
			travelPreferences_type0[0].setCabinPref(cabinPref);
			travelPreferences_type0[0].setVendorPref("GF");
		
			if (isNonstop) {

				System.out.println("*********** Set nonstop flights -->" + isNonstop);

				FlightTypePref_type0[] ftp = new FlightTypePref_type0[1];
				ftp[0] = new FlightTypePref_type0();

				ftp[0].setNonStopsOnlyInd(true); /* nonstoponly true */
				ftp[0].setFlightType(FlightTypeType.Nonstop); /* Direct/Connection/SingleConnection/OneStopOnly */

				travelPreferences_type0[0].setFlightTypePref(ftp);
			}

			oTA_AirLowFareSearchRQ.setTravelPreferences(travelPreferences_type0);
			
			// Set TravelerInfoSummary *******
			TravelerInfoSummary_type0 travelerInfoSummary_type0 = new TravelerInfoSummary_type0();

			// set Seat requested
			NonNegativeInteger[] requestedSeat = new NonNegativeInteger[1];
			requestedSeat[0] = new NonNegativeInteger("1");

			travelerInfoSummary_type0.setSeatsRequested(requestedSeat);

			// set Passenger type qty. Here we add Two passengers.
			TravelerInformationType[] travelerInformationTypeArr = new TravelerInformationType[1];
			travelerInformationTypeArr[0] = new TravelerInformationType();
			//travelerInformationTypeArr[1] = new TravelerInformationType();
			PassengerTypeQuantityType passengerTypeQuantityType = new PassengerTypeQuantityType();
			

			System.out.println("Add passenger types");

			StringLength1To8 paxCode = new StringLength1To8();
			paxCode.setStringLength1To8("ADT");
			passengerTypeQuantityType.setCode(paxCode);
			NonNegativeInteger qty = new NonNegativeInteger("1");
			passengerTypeQuantityType.setQuantity(qty);
			System.out.println("passengerTypeQuantityType:" + passengerTypeQuantityType.getCode());
			travelerInformationTypeArr[0].addPassengerTypeQuantity(passengerTypeQuantityType);

			/******* when 2 pax uncomment this. *******/
/*			PassengerTypeQuantityType passengerTypeQuantityType2 = new PassengerTypeQuantityType();
			StringLength1To8 paxCode2 = new StringLength1To8();
			paxCode2.setStringLength1To8("ADT");
			passengerTypeQuantityType2.setCode(paxCode2);
			System.out.println("passengerTypeQuantityType2:" + passengerTypeQuantityType2.getCode());
			NonNegativeInteger qty2 = new NonNegativeInteger("1");
			passengerTypeQuantityType2.setQuantity(qty2);
			travelerInformationTypeArr[1].addPassengerTypeQuantity(passengerTypeQuantityType2);*/
			/******* uncomment when 2 pax ********/

			travelerInfoSummary_type0.setAirTravelerAvail(travelerInformationTypeArr);

			PriceRequestInformation_type0 priceReqInfo_type = new PriceRequestInformation_type0();

			priceReqInfo_type.setPricingSource(PricingSourceType.Both);
			travelerInfoSummary_type0.setPriceRequestInformation(priceReqInfo_type);

			oTA_AirLowFareSearchRQ.setTravelerInfoSummary(travelerInfoSummary_type0);
			
			System.out.println("start fair search");

			OTA_AirLowFareSearchRS oTA_AirLowFareSearchRS = stub.AirLowFareSearch(oTA_AirLowFareSearchRQ);
			
			System.out.println("end fair search");

			System.out.println("----------------------- oTA_AirLowFareSearchRS --------------------------------");
			System.out.println("CorrelationID:" + oTA_AirLowFareSearchRS.getCorrelationID());
			System.out.println("EchoToken:" + oTA_AirLowFareSearchRS.getEchoToken());
			System.out.println("TransactionIdentifier:" + oTA_AirLowFareSearchRS.getTransactionIdentifier());
			transactionid = oTA_AirLowFareSearchRS.getTransactionIdentifier().toString();

			int resultsSize = oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary().length;

			System.out.println("results size:" + resultsSize);
			
			for (int x = 0; x < resultsSize; x++) {
				int seqNo = oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getSequenceNumber().intValue();
				System.out.println("\n********************* Sequence number:" + seqNo + " *******************************************");
				int oriDesSize = oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption().length;
				for (int y = 0; y < oriDesSize; y++) {
					int flSegSize = oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()[y].getFlightSegment().length;
					for (int z = 0; z < flSegSize; z++) {
						System.out.println("Flight Number:" + oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()[y].getFlightSegment()[z].getFlightNumber());
						System.out.println("Departure Airport:" + oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()[y].getFlightSegment()[z].getDepartureAirport().getLocationCode());
						System.out.println("Arrival Airport:" + oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()[y].getFlightSegment()[z].getArrivalAirport().getLocationCode());
						System.out.println("Operating Airline Code:" + oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()[y].getFlightSegment()[z].getOperatingAirline().getCode());
						System.out.println("Operating Airline:" + oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()[y].getFlightSegment()[z].getOperatingAirline());
						System.out.println("ResBookDesigCode:" + oTA_AirLowFareSearchRS.getOTA_AirLowFareSearchRSChoice_type0().getOTA_AirLowFareSearchRSSequence_type0().getPricedItineraries().getPricedItinerary()[x].getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()[y].getFlightSegment()[z].getResBookDesigCode());
						System.out.println("---------------------------------------------------------------------------");
					}
					System.out.println("===========================================================================");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("end callLowFareSearch()");
	}

	
	
	public OTA_AirLowFareSearchRQ getSearchRequest() {
		System.out.println("start callLowFareSearch()");
		OTA_AirLowFareSearchRQ oTA_AirLowFareSearchRQ = new OTA_AirLowFareSearchRQ();
		try {
		
			
			Calendar calendar = Calendar.getInstance();
			oTA_AirLowFareSearchRQ.setTimeStamp(calendar);

			oTA_AirLowFareSearchRQ.setTarget(air_search_Target);
			oTA_AirLowFareSearchRQ.setVersion(new BigDecimal(1.0));
			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128(echoTokenMain);
			oTA_AirLowFareSearchRQ.setEchoToken(echotoken);
			POS_Type pOS_Type = new POS_Type();

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(user);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			// OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("ZAR");
			sourceType[0].setISOCurrency(alphaLength3);
			pOS_Type.setSource(sourceType);

			oTA_AirLowFareSearchRQ.setPOS(pOS_Type);

			OriginDestinationInformation_type0 originDestinationInformation_type0 = new OriginDestinationInformation_type0();

			DateTimeInstant timeInstantType = new DateTimeInstant();

			/*
			 * String [] ids = TimeZone.getAvailableIDs(); for (int i = 0; i < ids.length; i++) { System.out.println(ids[i]); }
			 */

			timeInstantType.setString(searchDepatingDate);

			originDestinationInformation_type0.addDepartureDateTime(timeInstantType);
			System.out.println("Deptime : " + timeInstantType);

			OriginLocation_type0 originLocation_type0 = new OriginLocation_type0();
			StringLength1To16 originLocCode = new StringLength1To16();
			originLocCode.setStringLength1To16(depAC1); // Airpot code
			System.out.println("origin airport code:" + depAC1);
			originLocation_type0.setLocationCode(originLocCode);
			originDestinationInformation_type0.setOriginLocation(originLocation_type0);

			DestinationLocation_type0 destinationLocation_type0 = new DestinationLocation_type0();
			StringLength1To16 destLocCode = new StringLength1To16();
			destLocCode.setStringLength1To16(arrAC1); // Airpot code
			System.out.println("destination airport code:" + arrAC1);
			destinationLocation_type0.setLocationCode(destLocCode);
			originDestinationInformation_type0.setDestinationLocation(destinationLocation_type0);
			oTA_AirLowFareSearchRQ.addOriginDestinationInformation(originDestinationInformation_type0);

			/****************** build return destination *********************/
			if (!isOneWay) { // -------> NOT ONEWAY
				DateTimeInstant timeInstantType1 = new DateTimeInstant();
				timeInstantType1.setString(searchSecondDeptDate);
				System.out.println("second Deptime : " + timeInstantType);
				// timeInstantType1.setString(departureDate2);
				OriginDestinationInformation_type0 originDestinationInformation_type01 = new OriginDestinationInformation_type0();
				OriginLocation_type0 originLocation_type01 = new OriginLocation_type0();
				originDestinationInformation_type01.addDepartureDateTime(timeInstantType1);
				StringLength1To16 originLocCode1 = new StringLength1To16();
				// originLocCode1.setStringLength1To16("CMB");
				originLocCode1.setStringLength1To16(depAC2);
				System.out.println("second origin airport code:" + depAC2);
				originLocation_type01.setLocationCode(originLocCode1);
				originDestinationInformation_type01.setOriginLocation(originLocation_type01);

				DestinationLocation_type0 destinationLocation_type01 = new DestinationLocation_type0();
				StringLength1To16 destLocCode1 = new StringLength1To16();
				// destLocCode1.setStringLength1To16("SYD");
				destLocCode1.setStringLength1To16(arrAC2);
				System.out.println("second destination airport code:" + depAC2);
				destinationLocation_type01.setLocationCode(destLocCode1);
				originDestinationInformation_type01.setDestinationLocation(destinationLocation_type01);
				oTA_AirLowFareSearchRQ.addOriginDestinationInformation(originDestinationInformation_type01);
			} // -------> NOT ONEWAY

			// 3rd destination
			/*
			 * Date date3 = new Date(); DateTimeInstant timeInstantType3 = new DateTimeInstant();
			 * 
			 * timeInstantType3.setString(searchThirdDeptDate); OriginDestinationInformation_type0 originDestinationInformation_type03 = new OriginDestinationInformation_type0(); OriginLocation_type0
			 * originLocation_type03 = new OriginLocation_type0(); originDestinationInformation_type03.addDepartureDateTime(timeInstantType3); StringLength1To16 originLocCode3 = new
			 * StringLength1To16(); originLocCode3.setStringLength1To16(depAC3); originLocation_type03.setLocationCode(originLocCode3);
			 * originDestinationInformation_type03.setOriginLocation(originLocation_type03);
			 * 
			 * DestinationLocation_type0 destinationLocation_type03 = new DestinationLocation_type0(); StringLength1To16 destLocCode3 = new StringLength1To16();
			 * destLocCode3.setStringLength1To16(arrAC3); destinationLocation_type03.setLocationCode(destLocCode3);
			 * originDestinationInformation_type03.setDestinationLocation(destinationLocation_type03); oTA_AirLowFareSearchRQ.addOriginDestinationInformation(originDestinationInformation_type03);
			 */

			// Set TravelPref **********
			TravelPreferences_type0[] travelPreferences_type0 = new TravelPreferences_type0[1];
			travelPreferences_type0[0] = new TravelPreferences_type0();
		//	travelPreferences_type0[1] = new VendorPref_type0();
			
			// set cabin type			
			CabinPref_type0[] cabinPref = new CabinPref_type0[1];
			cabinPref[0] = new CabinPref_type0();			
			cabinPref[0].setCabin(CabinType.Economy);
			travelPreferences_type0[0].setCabinPref(cabinPref);
			travelPreferences_type0[0].setVendorPref("GF");
		
			if (isNonstop) {

				System.out.println("*********** Set nonstop flights -->" + isNonstop);

				FlightTypePref_type0[] ftp = new FlightTypePref_type0[1];
				ftp[0] = new FlightTypePref_type0();

				ftp[0].setNonStopsOnlyInd(true); /* nonstoponly true */
				ftp[0].setFlightType(FlightTypeType.Nonstop); /* Direct/Connection/SingleConnection/OneStopOnly */

				travelPreferences_type0[0].setFlightTypePref(ftp);
			}

			oTA_AirLowFareSearchRQ.setTravelPreferences(travelPreferences_type0);
			
			// Set TravelerInfoSummary *******
			TravelerInfoSummary_type0 travelerInfoSummary_type0 = new TravelerInfoSummary_type0();

			// set Seat requested
			NonNegativeInteger[] requestedSeat = new NonNegativeInteger[1];
			requestedSeat[0] = new NonNegativeInteger("1");

			travelerInfoSummary_type0.setSeatsRequested(requestedSeat);

			// set Passenger type qty. Here we add Two passengers.
			TravelerInformationType[] travelerInformationTypeArr = new TravelerInformationType[1];
			travelerInformationTypeArr[0] = new TravelerInformationType();
			//travelerInformationTypeArr[1] = new TravelerInformationType();
			PassengerTypeQuantityType passengerTypeQuantityType = new PassengerTypeQuantityType();
			

			System.out.println("Add passenger types");

			StringLength1To8 paxCode = new StringLength1To8();
			paxCode.setStringLength1To8("ADT");
			passengerTypeQuantityType.setCode(paxCode);
			NonNegativeInteger qty = new NonNegativeInteger("1");
			passengerTypeQuantityType.setQuantity(qty);
			System.out.println("passengerTypeQuantityType:" + passengerTypeQuantityType.getCode());
			travelerInformationTypeArr[0].addPassengerTypeQuantity(passengerTypeQuantityType);

			/******* when 2 pax uncomment this. *******/
/*			PassengerTypeQuantityType passengerTypeQuantityType2 = new PassengerTypeQuantityType();
			StringLength1To8 paxCode2 = new StringLength1To8();
			paxCode2.setStringLength1To8("ADT");
			passengerTypeQuantityType2.setCode(paxCode2);
			System.out.println("passengerTypeQuantityType2:" + passengerTypeQuantityType2.getCode());
			NonNegativeInteger qty2 = new NonNegativeInteger("1");
			passengerTypeQuantityType2.setQuantity(qty2);
			travelerInformationTypeArr[1].addPassengerTypeQuantity(passengerTypeQuantityType2);*/
			/******* uncomment when 2 pax ********/

			travelerInfoSummary_type0.setAirTravelerAvail(travelerInformationTypeArr);

			PriceRequestInformation_type0 priceReqInfo_type = new PriceRequestInformation_type0();

			priceReqInfo_type.setPricingSource(PricingSourceType.Both);
			travelerInfoSummary_type0.setPriceRequestInformation(priceReqInfo_type);

			oTA_AirLowFareSearchRQ.setTravelerInfoSummary(travelerInfoSummary_type0);
			
			System.out.println("start fair search");
			
			



		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("end callLowFareSearch()");
		return oTA_AirLowFareSearchRQ;
	}

	
	
	public void callairPrice() {
		try {
			System.out.println("start callairPrice()");
			// Rampart module should be in the repository
			ConfigurationContext ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem("D:/xmloutlibs/Axis2/axis2-1.4.1/repository", "D:/xmloutlibs/Axis2/axis2-1.4.1/conf/axis2.xml");
			ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
			ServiceClient sc = stub._getServiceClient();
			sc.engageModule("rampart");
			// sc.engageModule("xmloutlogging");
			Options options = sc.getOptions();
			options.setUserName(username);
			options.setPassword(password);
			options.setTimeOutInMilliSeconds(1000 * 60 * 3);

			OTA_AirPriceRQ oTA_AirPriceRQ = new OTA_AirPriceRQ();
			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128(echoTokenMain); // externalflights
			oTA_AirPriceRQ.setEchoToken(echotoken);

			Calendar calendar = Calendar.getInstance();
			oTA_AirPriceRQ.setTimeStamp(calendar);

			// oTA_CancelRQ.setTarget(Target_type0.Test);
			oTA_AirPriceRQ.setTarget(Target_type24.Test);
			oTA_AirPriceRQ.setVersion(new BigDecimal(1.0));
			/**
			 * Travelfusion routing ids
			 * 
			 */
			if (isMultipleAIREnable) {

				if (null == routingIds || "".equals(routingIds)) {
					routingIds = "Amadeus-IAH1S3100"; // amadeus
					// routingIds = "TRAVELFUSION@M11SQZRRZER11AT1@U1HSFC7Y3DHRSWRQ";
				} else {
					System.out.println("routing id is not null");
				}

				if (null == inoutids) {
					inoutids = new String[2];
					inoutids[0] = "M11SQZRRZER11AT1";
					inoutids[1] = "U1HSFC7Y3DHRSWRQ";
				} else {
					System.out.println("inout ids are not null");
				}

			}
			/**
			 * TODO: Problem with the identifier in the searchavailabilityresponse
			 * */
			System.out.println("correlationID-->" + correlationID);
			StringLength1To64 stringLength1To64 = new StringLength1To64();
			stringLength1To64.setStringLength1To64(correlationID); // SET THIS CORRECTLY ..!!!!!!!!!!!!!!!!!!!!!!!!
			oTA_AirPriceRQ.setCorrelationID(stringLength1To64);
			/** ============================================================== */

			POS_Type pOS_Type = new POS_Type();

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(user);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			// OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("ZAR");
			sourceType[0].setISOCurrency(alphaLength3);
			StringLength1To16 pcc = new StringLength1To16();
			pcc.setStringLength1To16(officeId);
			/** MSP1S212L OFFICE ID ? */
			sourceType[0].setPseudoCityCode(pcc);
			pOS_Type.setSource(sourceType);

			oTA_AirPriceRQ.setPOS(pOS_Type);

			StringLength1To128 seqNo = new StringLength1To128();
			seqNo.setStringLength1To128(transactionid); // ------ Transaction id --------------------
			oTA_AirPriceRQ.setTransactionIdentifier(seqNo);

			AirItineraryType airItineraryType = new AirItineraryType();
			if (isOneWay) {
				airItineraryType.setDirectionInd(AirTripType.value1);
				/** value1 OneWay */
			} else {
				airItineraryType.setDirectionInd(AirTripType.value4);
				/** value 4 is refers to "Circle" */
			}

			OriginDestinationOptions_type0 originDestinationOptions_type0 = new OriginDestinationOptions_type0();
			/*
			 * SEGMENTS
			 */
			int size = 2;
			if (isOneWay) {
				size = 1;
			}
			OriginDestinationOption_type0[] originDestinationOption_type0Arr = new OriginDestinationOption_type0[size]; // =====Two segments -=-------------

			originDestinationOption_type0Arr[0] = new OriginDestinationOption_type0();

			FlightSegment_type0 flightSegment_type0 = new FlightSegment_type0();

			TPA_ExtensionsType tpa_Ext = new TPA_ExtensionsType();
			tpa_Ext.setSelectedFlightIndex(flightSequence);
			flightSegment_type0.setTPA_Extensions(tpa_Ext);
			DepartureAirport_type0 departureAirport_type0 = new DepartureAirport_type0();
			StringLength1To8 depAirportCode = new StringLength1To8();
			// depAirportCode.setStringLength1To8("CPT");
			depAirportCode.setStringLength1To8(depAC1);
			departureAirport_type0.setLocationCode(depAirportCode);
			flightSegment_type0.setDepartureAirport(departureAirport_type0);

			ArrivalAirport_type0 arrivalAirport_type0 = new ArrivalAirport_type0();
			StringLength1To8 locCode = new StringLength1To8();
			// locCode.setStringLength1To8("JNB");
			locCode.setStringLength1To8(arrAC1);
			arrivalAirport_type0.setLocationCode(locCode);
			flightSegment_type0.setArrivalAirport(arrivalAirport_type0);

			CompanyNameType companyNameType = new CompanyNameType();
			StringLength1To16 marketingAirlinecode = new StringLength1To16();

			marketingAirlinecode.setStringLength1To16(airlineCode[0]);
			companyNameType.setStringLength0To128(airlineName[0]);

			companyNameType.setCode(marketingAirlinecode);
			flightSegment_type0.setMarketingAirline(companyNameType);

			flightSegment_type0.setDepartureDateTime(departureDate1);

			// flightSegment_type0.setArrivalDateTime("2011-08-20T07:45:00");
			flightSegment_type0.setArrivalDateTime(arrivalDate1);

			RPH_Type rph_Type = new RPH_Type();
			rph_Type.setRPH_Type("1");
			flightSegment_type0.setRPH(rph_Type);

			FlightNumberType flightNumberType = new FlightNumberType();
			FlightNumberType_type0 flightNumberType_type0 = new FlightNumberType_type0();
			flightNumberType_type0.setFlightNumberType_type0(flightnum[0]);
			flightNumberType.setObject(flightNumberType_type0);
			flightSegment_type0.setFlightNumber(flightNumberType);

			UpperCaseAlphaLength1To2 bookDesigCode = new UpperCaseAlphaLength1To2();
			bookDesigCode.setUpperCaseAlphaLength1To2(ResBookDesigCode[0]); // L
			flightSegment_type0.setResBookDesigCode(bookDesigCode);

			PositiveInteger numberInPrty = new PositiveInteger("3");
			flightSegment_type0.setNumberInParty(numberInPrty);

			if (isMultipleAIREnable) {

				if (null != inoutids) {
					InfoSourceType infoSourceType = new InfoSourceType();
					infoSourceType.setInfoSourceType(inoutids[0]);
					System.out.println("Setting inoutids[0] -->" + inoutids[0]);
					flightSegment_type0.setInfoSource(infoSourceType);
				}
			}

			originDestinationOption_type0Arr[0].addFlightSegment(flightSegment_type0);

			// *************** Return flight details ********************************
			if (!isOneWay) {
				/* uncommenting set the AirTripType to circle i.e value4 */

				originDestinationOption_type0Arr[1] = new OriginDestinationOption_type0();

				FlightSegment_type0 flightSegment_type2 = new FlightSegment_type0();

				DepartureAirport_type0 departureAirport_type = new DepartureAirport_type0();
				StringLength1To8 depAirportCode2 = new StringLength1To8();
				depAirportCode2.setStringLength1To8(depAC2);
				departureAirport_type.setLocationCode(depAirportCode2);
				flightSegment_type2.setDepartureAirport(departureAirport_type);

				ArrivalAirport_type0 arrivalAirport_type = new ArrivalAirport_type0();
				StringLength1To8 arrlocCode = new StringLength1To8();
				arrlocCode.setStringLength1To8(arrAC2);
				arrivalAirport_type.setLocationCode(arrlocCode);
				flightSegment_type2.setArrivalAirport(arrivalAirport_type);

				CompanyNameType companyName = new CompanyNameType();
				StringLength1To16 marketAirlinecode = new StringLength1To16();

				marketAirlinecode.setStringLength1To16(airlineCode[1]); // MN
				companyName.setStringLength0To128(airlineName[1]); // Kulula

				companyName.setCode(marketAirlinecode);
				flightSegment_type2.setMarketingAirline(companyName);

				flightSegment_type2.setDepartureDateTime(departureDate2);

				flightSegment_type2.setArrivalDateTime(arrivalDate2);

				RPH_Type rph_Type2 = new RPH_Type();
				rph_Type2.setRPH_Type("1");
				flightSegment_type2.setRPH(rph_Type2);

				FlightNumberType flightNumberType2 = new FlightNumberType();
				FlightNumberType_type0 flightNumberType_type = new FlightNumberType_type0();
				flightNumberType_type.setFlightNumberType_type0(flightnum[1]);
				flightNumberType2.setObject(flightNumberType_type);
				flightSegment_type2.setFlightNumber(flightNumberType2);

				UpperCaseAlphaLength1To2 bookDesigCode2 = new UpperCaseAlphaLength1To2();
				bookDesigCode2.setUpperCaseAlphaLength1To2(ResBookDesigCode[1]); // G
				flightSegment_type2.setResBookDesigCode(bookDesigCode2);

				PositiveInteger numberInPrty2 = new PositiveInteger("3");
				flightSegment_type2.setNumberInParty(numberInPrty2);

				if (isMultipleAIREnable) {

					if (null != inoutids) {
						InfoSourceType infoSourceType = new InfoSourceType();
						System.out.println("inoutids[1]-->" + inoutids[1]);
						infoSourceType.setInfoSourceType(inoutids[1]);
						flightSegment_type2.setInfoSource(infoSourceType);
					}

				}

				originDestinationOption_type0Arr[1].addFlightSegment(flightSegment_type2);

			}
			// -- finish return segment
			// -- Start 3rd destination.
			/*
			 * originDestinationOption_type0Arr[2] = new OriginDestinationOption_type0();
			 * 
			 * FlightSegment_type0 flightSegment_type2 = new FlightSegment_type0();
			 * 
			 * DepartureAirport_type0 departureAirport_type = new DepartureAirport_type0(); StringLength1To8 depAirportCode2 = new StringLength1To8(); depAirportCode2.setStringLength1To8(depAC3);
			 * departureAirport_type.setLocationCode(depAirportCode2); flightSegment_type2.setDepartureAirport(departureAirport_type);
			 * 
			 * 
			 * 
			 * ArrivalAirport_type0 arrivalAirport_type = new ArrivalAirport_type0(); StringLength1To8 arrlocCode = new StringLength1To8(); arrlocCode.setStringLength1To8(arrAC3);
			 * arrivalAirport_type.setLocationCode(arrlocCode); flightSegment_type2.setArrivalAirport(arrivalAirport_type);
			 * 
			 * 
			 * CompanyNameType companyName = new CompanyNameType(); StringLength1To16 marketAirlinecode= new StringLength1To16();
			 * 
			 * marketAirlinecode.setStringLength1To16(airlineCode[2]); //MN companyName.setStringLength0To128(airlineName[2]); //Kulula
			 * 
			 * companyName.setCode(marketAirlinecode); flightSegment_type2.setMarketingAirline(companyName);
			 * 
			 * 
			 * flightSegment_type2.setDepartureDateTime(departureDate3);
			 * 
			 * flightSegment_type2.setArrivalDateTime(arrivalDate3);
			 * 
			 * 
			 * RPH_Type rph_Type2 = new RPH_Type(); rph_Type2.setRPH_Type("1"); flightSegment_type2.setRPH(rph_Type2);
			 * 
			 * FlightNumberType flightNumberType2 = new FlightNumberType(); FlightNumberType_type0 flightNumberType_type = new FlightNumberType_type0();
			 * flightNumberType_type.setFlightNumberType_type0(flightnum[2]); flightNumberType2.setObject(flightNumberType_type ); flightSegment_type2.setFlightNumber(flightNumberType2);
			 * 
			 * 
			 * UpperCaseAlphaLength1To2 bookDesigCode2 = new UpperCaseAlphaLength1To2(); bookDesigCode2.setUpperCaseAlphaLength1To2(ResBookDesigCode[2]); //G
			 * flightSegment_type2.setResBookDesigCode(bookDesigCode2);
			 * 
			 * 
			 * PositiveInteger numberInPrty2 = new PositiveInteger("3"); flightSegment_type2.setNumberInParty(numberInPrty2 );
			 * 
			 * if(isMultipleAIREnable){
			 * 
			 * if(null!=inoutids){ InfoSourceType infoSourceType = new InfoSourceType(); System.out.println("inoutids[1]-->"+inoutids[1]); infoSourceType.setInfoSourceType(inoutids[1]);
			 * flightSegment_type2.setInfoSource(infoSourceType); }
			 * 
			 * }
			 * 
			 * 
			 * 
			 * originDestinationOption_type0Arr[2].addFlightSegment(flightSegment_type2);
			 * 
			 * 
			 * //-- finsh 3rd destination
			 */
			originDestinationOptions_type0.setOriginDestinationOption(originDestinationOption_type0Arr);
			airItineraryType.setOriginDestinationOptions(originDestinationOptions_type0);
			oTA_AirPriceRQ.setAirItinerary(airItineraryType);

			TravelerInfoSummary_type2 travelerInfoSum_type2 = new TravelerInfoSummary_type2();
			NonNegativeInteger noOfSeats = new NonNegativeInteger("2");
			travelerInfoSum_type2.addSeatsRequested(noOfSeats);

			TravelerInformationType travelerInformationType = new TravelerInformationType();
			PassengerTypeQuantityType passengerTypeQtyType = new PassengerTypeQuantityType();
			StringLength1To8 code = new StringLength1To8();
			code.setStringLength1To8("ADT");
			passengerTypeQtyType.setCode(code);
			NonNegativeInteger qty = new NonNegativeInteger("1");
			passengerTypeQtyType.setQuantity(qty);
			travelerInformationType.addPassengerTypeQuantity(passengerTypeQtyType);

			// 2nd Passenger
			PassengerTypeQuantityType passengerTypeQtyType2 = new PassengerTypeQuantityType();
			StringLength1To8 code2 = new StringLength1To8();
			code2.setStringLength1To8("CHD");
			passengerTypeQtyType2.setCode(code2);
			NonNegativeInteger qty2 = new NonNegativeInteger("1");
			passengerTypeQtyType2.setQuantity(qty2);
			travelerInformationType.addPassengerTypeQuantity(passengerTypeQtyType2);

			travelerInfoSum_type2.addAirTravelerAvail(travelerInformationType);
			System.out.println("----------------- AirPrice | PriceRequestInformation_type0 ----------------------");

			PriceRequestInformation_type0 priceReqInfo_type0 = new PriceRequestInformation_type0();

			priceReqInfo_type0.setPricingSource(PricingSourceType.Published);
			travelerInfoSum_type2.setPriceRequestInformation(priceReqInfo_type0);

			oTA_AirPriceRQ.setTravelerInfoSummary(travelerInfoSum_type2);
			// ---------------------------------------------------------------------------------------------------------

			System.out.println("----------------- AirPrice  ----------------------");
			OTA_AirPriceRS oTA_AirPriceRS = stub.AirPrice(oTA_AirPriceRQ);
			// oTA_AirPriceRS.getErrors().getError()[0].getType().getOTA_CodeType().get
			System.out.println("Price Response token -->" + oTA_AirPriceRS.getEchoToken());
			System.out.println("TransactionIdentifier  :"+oTA_AirPriceRS.getTransactionIdentifier());
			transactionid = oTA_AirPriceRS.getTransactionIdentifier().toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("end callairPrice()");
	}

	public void callReservation() {
		try {
			System.out.println("----------------------AIRRESERVATION------------------------");
			// Rampart module should be in the repository
			ConfigurationContext ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem("D:/xmloutlibs/Axis2/axis2-1.4.1/repository", "D:/xmloutlibs/Axis2/axis2-1.4.1/conf/axis2.xml");
			ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
			ServiceClient sc = stub._getServiceClient();
			sc.engageModule("rampart");
			// sc.engageModule("xmloutlogging");
			Options options = sc.getOptions();
			options.setUserName(username);
			options.setPassword(password);
			options.setTimeOutInMilliSeconds(1000 * 60 * 3);

			OTA_AirBookRQ oTA_AirBookRQ = new OTA_AirBookRQ();

			oTA_AirBookRQ.setTarget(Target_type30.Test);
			oTA_AirBookRQ.setVersion(new BigDecimal(1.0));
			POS_Type pOS_Type = new POS_Type();
			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128(echoTokenMain);
			oTA_AirBookRQ.setEchoToken(echotoken);

			Calendar calendar = Calendar.getInstance();
			oTA_AirBookRQ.setTimeStamp(calendar);

			/**
			 * Travelfusion routing ids
			 * 
			 */
			if (isMultipleAIREnable) {

				if (null == routingIds || "".equals(routingIds)) {
					// routingIds = "Travelfusion@TFRI111111111@TFRI22222222";
					routingIds = "Amadeus-IAH1S3100"; // amadeus
				}

				if (null == inoutids) {
					inoutids = new String[2];
					inoutids[0] = "TFI-000";
					inoutids[1] = "TFO-111";

				}
				System.out.println("routingIds-->" + routingIds);
				// StringLength1To64 stringLength1To64 = new StringLength1To64();
				// stringLength1To64.setStringLength1To64(routingIds);
				// oTA_AirBookRQ.setCorrelationID(stringLength1To64);

			}

			StringLength1To64 stringLength1To64 = new StringLength1To64();
			stringLength1To64.setStringLength1To64(correlationID); // Office id
			oTA_AirBookRQ.setCorrelationID(stringLength1To64);

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(user);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			// OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("USD");
			sourceType[0].setISOCurrency(alphaLength3);

			/*------------ Package reference -----------*/
			// BookingChannel_type0 bookingChannel_type0 = new BookingChannel_type0();
			//
			// CompanyNameType companyNameType1 = new CompanyNameType();
			// companyNameType1.setStringLength0To128(companyname);
			// bookingChannel_type0.setCompanyName(companyNameType1);
			//
			// OTA_CodeType oTA_CodeType = new OTA_CodeType();
			// oTA_CodeType.setOTA_CodeType("2");
			// bookingChannel_type0.setType(oTA_CodeType);
			//
			// StringLength1To32 stringLength1To32_divition = new StringLength1To32();
			// stringLength1To32_divition.setStringLength1To32(packageId);
			// companyNameType1.setDivision(stringLength1To32_divition);
			//
			// sourceType[0].setBookingChannel(bookingChannel_type0);
			/*------------ Package reference -----------*/

			pOS_Type.setSource(sourceType);
			oTA_AirBookRQ.setPOS(pOS_Type);

			StringLength1To128 seqNo = new StringLength1To128();
			seqNo.setStringLength1To128(transactionid);
			oTA_AirBookRQ.setTransactionIdentifier(seqNo);

			AirItineraryType airItineraryType = new AirItineraryType();
			if (isOneWay) {
				airItineraryType.setDirectionInd(AirTripType.value1);
				/** value1 OneWay */
			} else {
				airItineraryType.setDirectionInd(AirTripType.value4);
				/** value 4 is refers to "Circle" */
			}

			OriginDestinationOptions_type0 originDestinationOptions_type0 = new OriginDestinationOptions_type0();
			/**
			 * --------- SEGMENT --------
			 * */
			int size = 2;
			if (isOneWay) {
				size = 1;
			}
			OriginDestinationOption_type0[] originDestinationOption_type0Arr = new OriginDestinationOption_type0[size]; // segments

			originDestinationOption_type0Arr[0] = new OriginDestinationOption_type0();

			FlightSegment_type0 flightSegment_type0 = new FlightSegment_type0();

			DepartureAirport_type0 departureAirport_type0 = new DepartureAirport_type0();
			StringLength1To8 depAirportCode = new StringLength1To8();
			depAirportCode.setStringLength1To8(depAC1);
			departureAirport_type0.setLocationCode(depAirportCode);
			flightSegment_type0.setDepartureAirport(departureAirport_type0);
			logger.debug("Pos !!  => '");

			ArrivalAirport_type0 arrivalAirport_type0 = new ArrivalAirport_type0();
			StringLength1To8 locCode = new StringLength1To8();
			locCode.setStringLength1To8(arrAC1);
			arrivalAirport_type0.setLocationCode(locCode);
			flightSegment_type0.setArrivalAirport(arrivalAirport_type0);

			CompanyNameType companyNameType = new CompanyNameType();
			StringLength1To16 marketingAirlinecode = new StringLength1To16();

			marketingAirlinecode.setStringLength1To16(airlineCode[0]);
			companyNameType.setStringLength0To128(airlineName[0]);

			companyNameType.setCode(marketingAirlinecode);
			flightSegment_type0.setMarketingAirline(companyNameType);

			flightSegment_type0.setDepartureDateTime(departureDate1);

			flightSegment_type0.setArrivalDateTime(arrivalDate1);

			RPH_Type rph_Type = new RPH_Type();
			rph_Type.setRPH_Type("1");
			flightSegment_type0.setRPH(rph_Type);

			FlightNumberType flightNumberType = new FlightNumberType();
			FlightNumberType_type0 flightNumberType_type0 = new FlightNumberType_type0();
			flightNumberType_type0.setFlightNumberType_type0(flightnum[0]);
			flightNumberType.setObject(flightNumberType_type0);
			flightSegment_type0.setFlightNumber(flightNumberType);

			UpperCaseAlphaLength1To2 bookDesigCode = new UpperCaseAlphaLength1To2();
			bookDesigCode.setUpperCaseAlphaLength1To2(ResBookDesigCode[0]);
			flightSegment_type0.setResBookDesigCode(bookDesigCode);

			PositiveInteger numberInPrty = new PositiveInteger("3");
			flightSegment_type0.setNumberInParty(numberInPrty);

			/*
			 * Travelfusion inout ids
			 */
			if (isMultipleAIREnable) {
				if (null != inoutids) {
					InfoSourceType infoSourceType = new InfoSourceType();
					System.out.println("inoutids[0] -->" + inoutids[0]);
					infoSourceType.setInfoSourceType(inoutids[0]);
					flightSegment_type0.setInfoSource(infoSourceType);
				}

			}

			originDestinationOption_type0Arr[0].addFlightSegment(flightSegment_type0);
			// *************** Return flight details
			if (!isOneWay) {

				originDestinationOption_type0Arr[1] = new OriginDestinationOption_type0();

				FlightSegment_type0 flightSegment_type2 = new FlightSegment_type0();

				DepartureAirport_type0 departureAirport_type = new DepartureAirport_type0();
				StringLength1To8 depAirportCode2 = new StringLength1To8();
				depAirportCode2.setStringLength1To8(depAC2);
				departureAirport_type.setLocationCode(depAirportCode2);
				flightSegment_type2.setDepartureAirport(departureAirport_type);

				ArrivalAirport_type0 arrivalAirport_type = new ArrivalAirport_type0();
				StringLength1To8 arrlocCode = new StringLength1To8();
				arrlocCode.setStringLength1To8(arrAC2);
				arrivalAirport_type.setLocationCode(arrlocCode);
				flightSegment_type2.setArrivalAirport(arrivalAirport_type);

				CompanyNameType companyName = new CompanyNameType();
				StringLength1To16 marketAirlinecode = new StringLength1To16();

				marketAirlinecode.setStringLength1To16(airlineCode[1]);
				companyName.setStringLength0To128(airlineName[1]);

				companyName.setCode(marketAirlinecode);
				flightSegment_type2.setMarketingAirline(companyName);

				flightSegment_type2.setDepartureDateTime(departureDate2);

				flightSegment_type2.setArrivalDateTime(arrivalDate2);

				RPH_Type rph_Type2 = new RPH_Type();
				rph_Type2.setRPH_Type("1");
				flightSegment_type2.setRPH(rph_Type2);

				FlightNumberType flightNumberType2 = new FlightNumberType();
				FlightNumberType_type0 flightNumberType_type = new FlightNumberType_type0();
				flightNumberType_type.setFlightNumberType_type0(flightnum[1]);
				flightNumberType2.setObject(flightNumberType_type);
				flightSegment_type2.setFlightNumber(flightNumberType2);

				UpperCaseAlphaLength1To2 bookDesigCode2 = new UpperCaseAlphaLength1To2();
				bookDesigCode2.setUpperCaseAlphaLength1To2(ResBookDesigCode[1]);
				flightSegment_type2.setResBookDesigCode(bookDesigCode2);

				PositiveInteger numberInPrty2 = new PositiveInteger("3");
				flightSegment_type2.setNumberInParty(numberInPrty2);
				/*
				 * Travelfution
				 */
				if (null != inoutids) {
					InfoSourceType infoSourceType = new InfoSourceType();
					System.out.println("inoutids[1] -->" + inoutids[1]);
					infoSourceType.setInfoSourceType(inoutids[1]);
					flightSegment_type2.setInfoSource(infoSourceType);
				}

				originDestinationOption_type0Arr[1].addFlightSegment(flightSegment_type2);

			} // 2nd flight segment.

			/** 3rd segment */
			/*
			 * FlightSegment_type0 flightSegment_type3 = new FlightSegment_type0();
			 * 
			 * DepartureAirport_type0 departureAirport_type3 = new DepartureAirport_type0(); StringLength1To8 depAirportCode3 = new StringLength1To8(); depAirportCode3.setStringLength1To8("MAA");
			 * departureAirport_type3.setLocationCode(depAirportCode3); flightSegment_type3.setDepartureAirport(departureAirport_type3);
			 * 
			 * 
			 * 
			 * ArrivalAirport_type0 arrivalAirport_type3 = new ArrivalAirport_type0(); StringLength1To8 arrlocCode3 = new StringLength1To8(); arrlocCode3.setStringLength1To8("SIN");
			 * arrivalAirport_type3.setLocationCode(arrlocCode3); flightSegment_type3.setArrivalAirport(arrivalAirport_type3);
			 * 
			 * 
			 * CompanyNameType companyName3 = new CompanyNameType(); StringLength1To16 marketAirlinecode3= new StringLength1To16(); marketAirlinecode3.setStringLength1To16("9W");
			 * 
			 * companyName3.setCode(marketAirlinecode3); companyName3.setStringLength0To128(""); flightSegment_type3.setMarketingAirline(companyName3);
			 * 
			 * 
			 * Calendar calRet3 = Calendar.getInstance(); Date dateRet3 = new Date(); dateRet3.setDate(03); dateRet3.setMonth(03); dateRet3.setMinutes(15); dateRet3.setSeconds(00);
			 * dateRet3.setHours(1); calRet3.setTime(dateRet3); flightSegment_type3.setDepartureDateTime(calRet3);
			 * 
			 * 
			 * Calendar cal3Arr = Calendar.getInstance(); Date date3Arr = new Date(); date3Arr.setDate(03); date3Arr.setMonth(03); date3Arr.setMinutes(05); date3Arr.setSeconds(00);
			 * date3Arr.setHours(8); cal3Arr.setTime(date3Arr); flightSegment_type3.setArrivalDateTime(cal3Arr);
			 * 
			 * 
			 * RPH_Type rph_Type3 = new RPH_Type(); rph_Type3.setRPH_Type("3"); flightSegment_type3.setRPH(rph_Type3);
			 * 
			 * FlightNumberType flightNumberType3 = new FlightNumberType(); FlightNumberType_type0 flightNumberType_type3 = new FlightNumberType_type0();
			 * flightNumberType_type3.setFlightNumberType_type0("16"); flightNumberType3.setObject(flightNumberType_type3 ); flightSegment_type3.setFlightNumber(flightNumberType3);
			 * 
			 * 
			 * UpperCaseAlphaLength1To2 bookDesigCode3 = new UpperCaseAlphaLength1To2(); bookDesigCode3.setUpperCaseAlphaLength1To2("L"); flightSegment_type3.setResBookDesigCode(bookDesigCode3);
			 * 
			 * 
			 * PositiveInteger numberInPrty3 = new PositiveInteger("3"); flightSegment_type3.setNumberInParty(numberInPrty3 );
			 * 
			 * originDestinationOption_type0Arr[1].addFlightSegment(flightSegment_type3);
			 */
			/** 3rd segment */
			// ***********-----------------------------

			originDestinationOptions_type0.setOriginDestinationOption(originDestinationOption_type0Arr);
			airItineraryType.setOriginDestinationOptions(originDestinationOptions_type0);
			oTA_AirBookRQ.setAirItinerary(airItineraryType);

			// ******* Traveler Info

			TravelerInfoType travelerInfoType = new TravelerInfoType();

			SpecialReqDetailsType specialReqDetailsType = new SpecialReqDetailsType();
			Remarks_type0 remarks_type0 = new Remarks_type0();
			Remark_type1 remark1 = new Remark_type1();
			Remark_type1 remark2 = new Remark_type1();
			Remark_type1 remark3 = new Remark_type1();

			remark3.setString("0.0 Owner mark up was applied");
			remark2.setString("*ST9 1971.00");
			remark1.setString("*PR*** rezbase_v1");
			remarks_type0.addRemark(remark1);
			remarks_type0.addRemark(remark2);
			remarks_type0.addRemark(remark3);
			specialReqDetailsType.setRemarks(remarks_type0);

			SpecialRemarks_type0 specialRemarks_type0 = new SpecialRemarks_type0();
			SpecialRemark_type0 sRemark = new SpecialRemark_type0();

			StringLength1To16 remarkCodeType = new StringLength1To16();
			remarkCodeType.setStringLength1To16("T");
			sRemark.setRemarkType(remarkCodeType);
			sRemark.setText("AUD1971.00");
			specialRemarks_type0.addSpecialRemark(sRemark);
			specialReqDetailsType.setSpecialRemarks(specialRemarks_type0);

			SpecialServiceRequests_type0 SpeSerRequests_type0 = new SpecialServiceRequests_type0();

			/*************************************
			 * When Passport details to be send use this********************************************** Check for Child since it too is SSR
			 ***************************************************************************************************************************/
			SpecialServiceRequest_type0[] SpeSerRequestArr = new SpecialServiceRequest_type0[2];
			/*** SpecialServiceRequest_type0[] SpeSerRequestArr = new SpecialServiceRequest_type0[1]; */
			// SpeSerRequestArr[0]= new SpecialServiceRequest_type0();
			// AlphaLength4 ssrCode = new AlphaLength4();
			// ssrCode.setAlphaLength4("CHDD");
			// SpeSerRequestArr[0].setSSRCode(ssrCode);
			// ListOfRPH listOfRPH = new ListOfRPH();
			// RPH_Type[] rph_TypeArr = new RPH_Type[1];
			// rph_TypeArr[0] = new RPH_Type();
			// rph_TypeArr[0].setRPH_Type("1");
			// listOfRPH.setRPH_Type(rph_TypeArr);
			// SpeSerRequestArr[0].setFlightRefNumberRPHList(listOfRPH);
			//
			// ListOfRPH rphList = new ListOfRPH();
			// RPH_Type[] itemListArr = new RPH_Type[1];
			// itemListArr[0] = new RPH_Type();
			// itemListArr[0].setRPH_Type("1");
			// rphList.setRPH_Type(itemListArr);
			// SpeSerRequestArr[0].setTravelerRefNumberRPHList(rphList);
			//
			// SpeSerRequestArr[0].setText("ds/sd/03");
			// SpeSerRequests_type0.setSpecialServiceRequest(SpeSerRequestArr);
			// -----------------------------------------------------

			SpeSerRequestArr[0] = new SpecialServiceRequest_type0();
			AlphaLength4 ssrCode1 = new AlphaLength4();
			ssrCode1.setAlphaLength4("DOCS");
			SpeSerRequestArr[0].setSSRCode(ssrCode1);

			ListOfRPH listOfRPH1 = new ListOfRPH();
			RPH_Type[] rph_TypeArr1 = new RPH_Type[1];
			rph_TypeArr1[0] = new RPH_Type();
			rph_TypeArr1[0].setRPH_Type("12");
			listOfRPH1.setRPH_Type(rph_TypeArr1);
			SpeSerRequestArr[0].setFlightRefNumberRPHList(listOfRPH1);

			ListOfRPH rphList1 = new ListOfRPH();
			RPH_Type[] itemListArr1 = new RPH_Type[1];
			itemListArr1[0] = new RPH_Type();
			itemListArr1[0].setRPH_Type("1");
			rphList1.setRPH_Type(itemListArr1);
			SpeSerRequestArr[0].setTravelerRefNumberRPHList(rphList1);

		//	SpeSerRequestArr[0].setText("P-GBR-012345678-GBR-30JUN73-M-14APR09-JOHN-SMITH-H");
			SpeSerRequestArr[0].setText("P-LKA-565656565656-LKA-20JUL89-M-08JUL20-Charaka-XmlOutCustomer-H");
			// SpeSerRequests_type0.setSpecialServiceRequest(SpeSerRequestArr);
			// ------------------------------------------------
			SpeSerRequestArr[1] = new SpecialServiceRequest_type0();
			AlphaLength4 ssrCode2 = new AlphaLength4();
			ssrCode2.setAlphaLength4("DOCS");
			SpeSerRequestArr[1].setSSRCode(ssrCode2);

			ListOfRPH listOfRPH2 = new ListOfRPH();
			RPH_Type[] rph_TypeArr2 = new RPH_Type[1];
			rph_TypeArr2[0] = new RPH_Type();
			rph_TypeArr2[0].setRPH_Type("12");
			listOfRPH2.setRPH_Type(rph_TypeArr2);
			SpeSerRequestArr[1].setFlightRefNumberRPHList(listOfRPH2);

			ListOfRPH rphList2 = new ListOfRPH();
			RPH_Type[] itemListArr2 = new RPH_Type[1];
			itemListArr2[0] = new RPH_Type();
			itemListArr2[0].setRPH_Type("2");
			rphList2.setRPH_Type(itemListArr2);
			SpeSerRequestArr[1].setTravelerRefNumberRPHList(rphList2);

			SpeSerRequestArr[1].setText("P-GBR-012345678-GBR-30JUN73-M-14APR09-JOHN-SMITH-H");
			SpeSerRequests_type0.setSpecialServiceRequest(SpeSerRequestArr);

			/**
			 * We need to add the passport details and check ----------------------------------------------
			 **/

			specialReqDetailsType.setSpecialServiceRequests(SpeSerRequests_type0);

			SeatRequests_type0 seatRequests_type0 = new SeatRequests_type0();
			SeatRequest_type0[] seatRequest_type0 = new SeatRequest_type0[1];
			seatRequest_type0[0] = new SeatRequest_type0();
			seatRequest_type0[0].setSeatNumber("19D");
			ListOfRPH travlistOfRPH = new ListOfRPH();
			RPH_Type[] RPH_Type = new RPH_Type[1];
			RPH_Type[0] = new RPH_Type();
			RPH_Type[0].setRPH_Type("1");
			travlistOfRPH.setRPH_Type(RPH_Type);
			seatRequest_type0[0].setTravelerRefNumberRPHList(travlistOfRPH);
			ListOfRPH flightRefRPH = new ListOfRPH();
			RPH_Type[] sFitemListArr = new RPH_Type[1];
			sFitemListArr[0] = new RPH_Type();
			sFitemListArr[0].setRPH_Type("1");

			flightRefRPH.setRPH_Type(sFitemListArr);
			seatRequest_type0[0].setFlightRefNumberRPHList(flightRefRPH);
			seatRequests_type0.setSeatRequest(seatRequest_type0);
			specialReqDetailsType.setSeatRequests(seatRequests_type0);

			travelerInfoType.addSpecialReqDetails(specialReqDetailsType);

			Date bDate = new Date();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-DD");

			AirTraveler_type2[] traveler = new AirTraveler_type2[1];// number of Pax
			
			//First pax - ADT
			setTraveller1(df, traveler);
			

			//Date bDate;
			StringLength1To64 givenName = new StringLength1To64();
			StringLength1To16 namePrefix = new StringLength1To16();
			StringLength1To64 middleName = new StringLength1To64();
			StringLength1To64 surname = new StringLength1To64();
			
			traveler[0] = new AirTraveler_type2();
			AlphaLength3 paxType1 = new AlphaLength3();
			paxType1.setAlphaLength3("ADT");
			traveler[0].setPassengerTypeCode(paxType1);
			PersonNameType pName = new PersonNameType();


			namePrefix.setStringLength1To16("Mr");
			pName.setNamePrefix(namePrefix);
			givenName.setStringLength1To64("Charaka");
			pName.setGivenName(givenName);

			middleName.setStringLength1To64("XmlOutCustomer");
			pName.setMiddleName(middleName);
			
			surname.setStringLength1To64("XmlOutCustomer");
			pName.setSurname(surname);

			traveler[0].setPersonName(pName);
			TravelerRefNumber_type0 trvRef = new TravelerRefNumber_type0();
			RPH_Type rph = new RPH_Type();
			rph.setRPH_Type("1");
			trvRef.setRPH(rph);
			traveler[0].setTravelerRefNumber(trvRef);

			try {
				bDate = df.parse("1984-02-25");
			} catch (ParseException e2) {
				e2.printStackTrace();
			}
			traveler[0].setBirthDate("25-02-1984");
			// traveler[0].setBirthDate("1984-02-25");
			// traveler[0].setBirthDate(bDate);
		
			
			
			
			// second pax -ADT 
			//setTraveller2(df, traveler );
			// Third pax - CHD
			//setTraveller3(df, traveler);

			
			//Set contact details for traveler 1 AKA Customer
			
			Telephone_type3[] Telephone_type0Arr = new Telephone_type3[1];
			
			Telephone_type0Arr[0] = new Telephone_type3();
			NumericStringLength1To8 areaCode = new NumericStringLength1To8();

			areaCode.setNumericStringLength1To8("123");

			Telephone_type0Arr[0].setAreaCityCode(areaCode);
			OTA_CodeType pLocType = new OTA_CodeType();
			System.out.println("<<<<<<<>>>>>>>>>>");
			pLocType.setOTA_CodeType("H"); // H to reffer Home
			System.out.println("<<<<<<<>sds>>>>>>>>>");

			Telephone_type0Arr[0].setPhoneLocationType(pLocType);
			NumericStringLength1To3 countryCode = new NumericStringLength1To3();
			countryCode.setNumericStringLength1To3("1");
			Telephone_type0Arr[0].setCountryAccessCode(countryCode);
			StringLength1To32 phoneNo = new StringLength1To32();
			phoneNo.setStringLength1To32("215616580");
			Telephone_type0Arr[0].setPhoneNumber(phoneNo);
			Telephone_type0Arr[0].setFormattedInd(false);
			traveler[0].setTelephone(Telephone_type0Arr);

			EmailType email = new EmailType();
			email.setStringLength1To128("charaka@colombo.rezgateway.com");
			traveler[0].addEmail(email);

			Address_type2 address_type0 = new Address_type2();
			// address_type0.setFormattedInd(false);
			OTA_CodeType adresType = new OTA_CodeType();
			adresType.setOTA_CodeType("H");
			address_type0.setType(adresType);

			StreetNmbrType streetNo = new StreetNmbrType();

			streetNo.setStringLength0To64("Geethanjalee Place");
			address_type0.setStreetNmbr(streetNo);
			StringLength1To64 cityName = new StringLength1To64();
			cityName.setStringLength1To64("Colombo 4");
			address_type0.setCityName(cityName);
			StringLength1To16 postalCode = new StringLength1To16();
			postalCode.setStringLength1To16("2356");
			address_type0.setPostalCode(postalCode);
			StateProvType statePro = new StateProvType();
			StateProvCodeType provCode = new StateProvCodeType();
			provCode.setStringLength1To8("LDF");
			statePro.setStateCode(provCode);
			statePro.setStringLength0To64("");
			address_type0.setStateProv(statePro);

			BldgRoom_type0[] param = new BldgRoom_type0[1];
			System.out.println(param.length);
			param[0] = new BldgRoom_type0();
			param[0].setStringLength0To64("No 3");
			address_type0.setBldgRoom(param);

			CountryNameType countNme = new CountryNameType();
			ISO3166 countCode = new ISO3166();
			countCode.setISO3166("LK");
			countNme.setCode(countCode);
			countNme.setStringLength0To64("Sri lanka");
			address_type0.setCountryName(countNme);
			traveler[0].addAddress(address_type0);

			travelerInfoType.setAirTraveler(traveler);

			oTA_AirBookRQ.setTravelerInfo(travelerInfoType);

			// Price Info. - Pricing Source
			try {
				PriceInfo_type0 priceInfo_type0 = new PriceInfo_type0();
				priceInfo_type0.setPricingSource(PricingSourceType.Published);
				oTA_AirBookRQ.setPriceInfo(priceInfo_type0);

			} catch (Exception e) {
				System.out.println("Error Pricing Source " + e);
			}

			// Ticketing Time Limit.
			try {
				Ticketing_type0 ttl = new Ticketing_type0();

				//SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				// Date date = new Date();
				// dff.format(date);
				// System.out.println(dff.format("2010-10-20T23:59:00"));
				// ttl.setTicketTimeLimit("2010-10-20T23:59:00");
				ttl.setTicketTimeLimit(ticketingTimeLimit);
				// TicketTimeLimit="2011-05-19T23:59:00"
				ttl.setTicketType(TicketType.eTicket);
				oTA_AirBookRQ.addTicketing(ttl);

			} catch (Exception e) {
				System.out.println("Error Ticketing Time Limit");
			}

			// Queue Details.
			try {
				Queue_type1 queue_type0 = new Queue_type1();
				// AlphaNumericStringLength1To8 queuNo = new AlphaNumericStringLength1To8();
				// queuNo.setAlphaNumericStringLength1To8("01");
				// queue_type0.setQueueNumber(queuNo);
				// AlphaNumericStringLength1To8 queueCat = new AlphaNumericStringLength1To8();
				//
				// queueCat.setAlphaNumericStringLength1To8("01");
				// queue_type0.setQueueCategory(queueCat);
				oTA_AirBookRQ.addQueue(queue_type0);

			} catch (Exception e) {
				System.out.println("Error Queue details");
			}

			// Payment Details.

			FulfillmentType fulfillment = new FulfillmentType();
			PaymentDetails_type0 paymentDtls = new PaymentDetails_type0();
			PaymentDetail_type0 paymentDetail = new PaymentDetail_type0();
			PaymentCardType paymentCard = new PaymentCardType();

			/*
			 * Pay@ payment reference request
			 */

			if (isRemarkEnabled) {
				StringLength1To128 stringLength1To128 = new StringLength1To128();
				stringLength1To128.setStringLength1To128("pay@");
				paymentDetail.setRemark(stringLength1To128);
			}

			// if(!isRemarkEnabled){ // Pay@ enabled then no need of cc

			OTA_CodeType cardType = new OTA_CodeType();
			cardType.setOTA_CodeType("C");
			paymentCard.setCardType(cardType);

			PaymentCardCodeType paymentCardCode = new PaymentCardCodeType();
			/*
			 * PaymentCardCodeType_type1 paymentCardCodeType = new PaymentCardCodeType_type1(); paymentCardCodeType.setUpperCaseAlphaLength1To2("VI"); paymentCardCode.setObject(paymentCardCodeType);
			 */
			paymentCardCode.setUpperCaseAlphaLength1To2("VI"); // this is insane?
			paymentCard.setCardCode(paymentCardCode);

			NumericStringLength1To19 cardNo = new NumericStringLength1To19();
			cardNo.setNumericStringLength1To19("6111111111111116");
			paymentCard.setCardNumber(cardNo);

			MMYYDate expDate = new MMYYDate();
			expDate.setMMYYDate("0712");
			paymentCard.setExpireDate(expDate);

			NumericStringLength1To8 cvs = new NumericStringLength1To8();
			cvs.setNumericStringLength1To8("123");
			paymentCard.setSeriesCode(cvs);

			// }

			StringLength1To64 cardHoldrName = new StringLength1To64();
			cardHoldrName.setStringLength1To64("TestA");
			paymentCard.setCardHolderName(cardHoldrName);

			AddressType addrsType = new AddressType();
			StreetNmbrType StreetNmbr = new StreetNmbrType();

			StreetNmbr.setStringLength0To64("PO BOX 104 ILLOVO BEACH");
			addrsType.setStreetNmbr(StreetNmbr);

			StringLength1To64 ccCardcityName = new StringLength1To64();
			ccCardcityName.setStringLength1To64("4155");

			addrsType.setCityName(ccCardcityName);

			CountryNameType countryName = new CountryNameType();
			ISO3166 crCardCountryCode = new ISO3166();
			crCardCountryCode.setISO3166("ZA");
			countryName.setCode(crCardCountryCode);
			countryName.setStringLength0To64("South Africa");
			addrsType.setCountryName(countryName);

			paymentCard.setAddress(addrsType);
			paymentDetail.setPaymentCard(paymentCard);

			paymentDtls.addPaymentDetail(paymentDetail);

			fulfillment.setPaymentDetails(paymentDtls);

			AddressType addressType = new AddressType();
			StreetNmbrType delvryStreet = new StreetNmbrType();
			delvryStreet.setStringLength0To64("PO BOX 104 ILLOVO BEACH");
			addressType.setStreetNmbr(delvryStreet);

			StringLength1To64 dilivryCity = new StringLength1To64();
			dilivryCity.setStringLength1To64("4155");
			addressType.setCityName(dilivryCity);
			CountryNameType delivryCountry = new CountryNameType();
			ISO3166 delContryCode = new ISO3166();
			delContryCode.setISO3166("ZA");

			delivryCountry.setCode(delContryCode);
			delivryCountry.setStringLength0To64("Sri Lanka");
			addressType.setCountryName(delivryCountry);

			fulfillment.setDeliveryAddress(addressType);
			/*
			 * PaymentText_type0 paymentText_type0 = new PaymentText_type0(); paymentText_type0.setName(); fulfillment.setPaymentText()
			 */
			oTA_AirBookRQ.setFulfillment(fulfillment);
			System.out.println("Card Holder >>>");

			/*
			 * UniqueID_Type uiq = new UniqueID_Type(); StringLength1To32 packageId = new StringLength1To32(); packageId.setStringLength1To32(packageRef); uiq.setID(packageId);
			 * oTA_AirBookRQ.setBookingReferenceID(uiq);
			 */
			// uiq.setID_Context();

			OTA_AirBookRS oTA_AirBookRS = stub.AirBook(oTA_AirBookRQ);
			
			
			oTA_AirBookRS.getOTA_AirBookRSSequence_type0()[0].getAirReservation()[0].getBookingReferenceID();
			System.out.println("--------------Response---------------------------");
			System.out.println("---Booking Referance  ID(PNR) :"+oTA_AirBookRS.getOTA_AirBookRSSequence_type0()[0].getAirReservation()[0].getBookingReferenceID()[0].getID());
			

		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param df
	 * @param traveler
	 */
	private void setTraveller1(SimpleDateFormat df, AirTraveler_type2[] traveler) {
		Date bDate;
		StringLength1To64 givenName = new StringLength1To64();
		StringLength1To16 namePrefix = new StringLength1To16();
		StringLength1To64 middleName = new StringLength1To64();
		StringLength1To64 surname = new StringLength1To64();
		
		traveler[0] = new AirTraveler_type2();
		AlphaLength3 paxType1 = new AlphaLength3();
		paxType1.setAlphaLength3("ADT");
		traveler[0].setPassengerTypeCode(paxType1);
		PersonNameType pName = new PersonNameType();


		namePrefix.setStringLength1To16("Mr");
		pName.setNamePrefix(namePrefix);
		givenName.setStringLength1To64("Charaka");
		pName.setGivenName(givenName);

		middleName.setStringLength1To64("XmlOutCustomer");
		pName.setMiddleName(middleName);
		
		surname.setStringLength1To64("XmlOutCustomer");
		pName.setSurname(surname);

		traveler[0].setPersonName(pName);
		TravelerRefNumber_type0 trvRef = new TravelerRefNumber_type0();
		RPH_Type rph = new RPH_Type();
		rph.setRPH_Type("1");
		trvRef.setRPH(rph);
		traveler[0].setTravelerRefNumber(trvRef);

		try {
			bDate = df.parse("1984-02-25");
		} catch (ParseException e2) {
			e2.printStackTrace();
		}
		traveler[0].setBirthDate("25-02-1984");
		// traveler[0].setBirthDate("1984-02-25");
		// traveler[0].setBirthDate(bDate);
	}

	/**
	 * @param df
	 * @param traveler
	 * @param middleName
	 * @param surname
	 */
	private void setTraveller2(SimpleDateFormat df,
			AirTraveler_type2[] traveler) {
		Date bDate;
		traveler[1] = new AirTraveler_type2();
		PersonNameType pName2 = new PersonNameType();
		StringLength1To64 givenName1 = new StringLength1To64();
		StringLength1To16 namePrefix1 = new StringLength1To16();
		StringLength1To64 middleName = new StringLength1To64();
		StringLength1To64 surname = new StringLength1To64();
		
		AlphaLength3 paxType2 = new AlphaLength3();
		paxType2.setAlphaLength3("ADT");
		traveler[1].setPassengerTypeCode(paxType2);
		namePrefix1.setStringLength1To16("Mr");
		pName2.setNamePrefix(namePrefix1);
		givenName1.setStringLength1To64("Passenger2");
		pName2.setGivenName(givenName1);
		middleName.setStringLength1To64("Testdg");
		pName2.setMiddleName(middleName);
		surname.setStringLength1To64("XmlOut");
		pName2.setSurname(surname);

		traveler[1].setPersonName(pName2);

		TravelerRefNumber_type0 trvRef2 = new TravelerRefNumber_type0();
		RPH_Type rph2 = new RPH_Type();
		rph2.setRPH_Type("2");
		trvRef2.setRPH(rph2);
		traveler[1].setTravelerRefNumber(trvRef2);
		CustLoyalty_type0 custLoyalty_type0 = new CustLoyalty_type0();
		StringLength1To16 prgID = new StringLength1To16();
		prgID.setStringLength1To16("EK");
		custLoyalty_type0.setProgramID(prgID);
		StringLength1To32 memberID = new StringLength1To32();
		memberID.setStringLength1To32("12356");
		custLoyalty_type0.setMembershipID(memberID);
		traveler[1].addCustLoyalty(custLoyalty_type0);

		try {
			bDate = df.parse("1986-02-25");
		} catch (ParseException e2) {
			e2.printStackTrace();
		}
		traveler[1].setBirthDate("25-02-1985");
		// traveler[1].setBirthDate("1986-02-25");
		// traveler[1].setBirthDate(bDate);
	}

	private void setTraveller3(SimpleDateFormat df,
			AirTraveler_type2[] traveler) {
		Date bDate;
		traveler[2] = new AirTraveler_type2();
		PersonNameType pName3 = new PersonNameType();
		StringLength1To64 givenName2 = new StringLength1To64();
		StringLength1To16 namePrefix2 = new StringLength1To16();
		StringLength1To64 middleName = new StringLength1To64();
		StringLength1To64 surname = new StringLength1To64();
		
		AlphaLength3 paxType3 = new AlphaLength3();
		paxType3.setAlphaLength3("CHD");
		traveler[2].setPassengerTypeCode(paxType3);
		namePrefix2.setStringLength1To16("Mst");
		pName3.setNamePrefix(namePrefix2);
		givenName2.setStringLength1To64("Chan");
		pName3.setGivenName(givenName2);
		middleName.setStringLength1To64("TestEws");
		pName3.setMiddleName(middleName);
		surname.setStringLength1To64("XmlOut");
		pName3.setSurname(surname);

		traveler[2].setPersonName(pName3);

		TravelerRefNumber_type0 trvRef3 = new TravelerRefNumber_type0();
		RPH_Type rph3 = new RPH_Type();
		rph3.setRPH_Type("3");
		trvRef3.setRPH(rph3);
		traveler[2].setTravelerRefNumber(trvRef3);

		// SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

		/*
		 * bDate.setDate(30); bDate.setMonth(11); bDate.setYear(100); df.format(bDate);
		 */
		try {
			bDate = df.parse("2009-02-25");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		// traveler[2].setBirthDate(bDate);
		// traveler[2].setBirthDate("2009-02-25");
		traveler[2].setBirthDate("09-02-2009");

		System.out.println(">>>2 ** " + traveler[2].getBirthDate());
	}

	/**
	 * 
	 * **/

	public void callAirRules() {
		try {

			System.out.println("Call Air rules");
			ConfigurationContext ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem("D:/xmloutlibs/Axis2/axis2-1.4.1/repository", "D:/xmloutlibs/Axis2/axis2-1.4.1/conf/axis2.xml");
			ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
			ServiceClient sc = stub._getServiceClient();
			sc.engageModule("rampart");
			// sc.engageModule("xmloutlogging");
			Options options = sc.getOptions();
			options.setUserName(username);
			options.setPassword(password);
			options.setTimeOutInMilliSeconds(1000 * 60 * 3);

			OTA_AirRulesRQ oTA_AirRulesRQ = new OTA_AirRulesRQ();
			
			StringLength1To64 stringLength1To64 = new StringLength1To64();
			stringLength1To64.setStringLength1To64(correlationID); // SET THIS CORRECTLY ..!!!!!!!!!!!!!!!!!!!!!!!!
			oTA_AirRulesRQ.setCorrelationID(stringLength1To64);
			
			// oTA_CancelRQ.setTarget(Target_type0.Test);
			oTA_AirRulesRQ.setTarget(Target_type26.Test);
			oTA_AirRulesRQ.setVersion(new BigDecimal(1.0));
			POS_Type pOS_Type = new POS_Type();

			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128(echoTokenMain);
			oTA_AirRulesRQ.setEchoToken(echotoken);

			Calendar calendar = Calendar.getInstance();
			oTA_AirRulesRQ.setTimeStamp(calendar);

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(user);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			// OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("USD");
			sourceType[0].setISOCurrency(alphaLength3);
			StringLength1To16 pcc = new StringLength1To16();
			pcc.setStringLength1To16(officeId);
			sourceType[0].setPseudoCityCode(pcc);
			pOS_Type.setSource(sourceType);

			oTA_AirRulesRQ.setPOS(pOS_Type);

			AirItineraryType airItineraryType = new AirItineraryType();

			airItineraryType.setDirectionInd(AirTripType.value4); // value 4 is refers to "Circle"
			OriginDestinationOptions_type0 originDestinationOptions_type0 = new OriginDestinationOptions_type0();

			OriginDestinationOption_type0[] originDestinationOption_type0Arr = new OriginDestinationOption_type0[2];
			originDestinationOption_type0Arr[0] = new OriginDestinationOption_type0();

			FlightSegment_type0 flightSegment_type0 = new FlightSegment_type0();

			DepartureAirport_type0 departureAirport_type0 = new DepartureAirport_type0();
			StringLength1To8 depAirportCode = new StringLength1To8();
			depAirportCode.setStringLength1To8(depAC1);
			departureAirport_type0.setLocationCode(depAirportCode);
			flightSegment_type0.setDepartureAirport(departureAirport_type0);

			ArrivalAirport_type0 arrivalAirport_type0 = new ArrivalAirport_type0();
			StringLength1To8 locCode = new StringLength1To8();
			locCode.setStringLength1To8(arrAC1);
			arrivalAirport_type0.setLocationCode(locCode);
			flightSegment_type0.setArrivalAirport(arrivalAirport_type0);

			CompanyNameType companyNameType = new CompanyNameType();
			StringLength1To16 marketingAirlinecode = new StringLength1To16();
			marketingAirlinecode.setStringLength1To16(airlineCode[0]);
			companyNameType.setCode(marketingAirlinecode);
			companyNameType.setStringLength0To128(airlineName[0]);
			flightSegment_type0.setMarketingAirline(companyNameType);

			/*
			 * Calendar cal = Calendar.getInstance(); Date date = new Date(); date.setDate(01); date.setMonth(03); date.setMinutes(30); date.setSeconds(00); date.setHours(16); cal.setTime(date);
			 */flightSegment_type0.setDepartureDateTime(departureDate1);

			/*
			 * Calendar cal2 = Calendar.getInstance(); Date date2 = new Date(); date2.setDate(02); date2.setMonth(03); date2.setMinutes(35); date2.setSeconds(00); date2.setHours(17);
			 * cal2.setTime(date2);
			 */
			flightSegment_type0.setArrivalDateTime(arrivalDate1);

			RPH_Type rph_Type = new RPH_Type();
			rph_Type.setRPH_Type("1");
			flightSegment_type0.setRPH(rph_Type);

			FlightNumberType flightNumberType = new FlightNumberType();
			FlightNumberType_type0 flightNumberType_type0 = new FlightNumberType_type0();
			flightNumberType_type0.setFlightNumberType_type0(flightnum[0]);
			flightNumberType.setObject(flightNumberType_type0);
			flightSegment_type0.setFlightNumber(flightNumberType);

			UpperCaseAlphaLength1To2 bookDesigCode = new UpperCaseAlphaLength1To2();
			bookDesigCode.setUpperCaseAlphaLength1To2(ResBookDesigCode[0]);
			flightSegment_type0.setResBookDesigCode(bookDesigCode);

			PositiveInteger numberInPrty = new PositiveInteger("3");
			flightSegment_type0.setNumberInParty(numberInPrty);

			originDestinationOption_type0Arr[0].addFlightSegment(flightSegment_type0);
			// *************** Return flight details

			originDestinationOption_type0Arr[1] = new OriginDestinationOption_type0();

			FlightSegment_type0 flightSegment_type2 = new FlightSegment_type0();

			DepartureAirport_type0 departureAirport_type = new DepartureAirport_type0();
			StringLength1To8 depAirportCode2 = new StringLength1To8();
			depAirportCode2.setStringLength1To8(depAC2);
			departureAirport_type.setLocationCode(depAirportCode2);
			flightSegment_type2.setDepartureAirport(departureAirport_type);

			ArrivalAirport_type0 arrivalAirport_type = new ArrivalAirport_type0();
			StringLength1To8 arrlocCode = new StringLength1To8();
			arrlocCode.setStringLength1To8(arrAC2);
			arrivalAirport_type.setLocationCode(arrlocCode);
			flightSegment_type2.setArrivalAirport(arrivalAirport_type);

			CompanyNameType companyName = new CompanyNameType();
			StringLength1To16 marketAirlinecode = new StringLength1To16();
			marketAirlinecode.setStringLength1To16(airlineCode[1]);

			companyName.setCode(marketAirlinecode);
			companyName.setStringLength0To128(airlineName[1]);
			flightSegment_type2.setMarketingAirline(companyName);

			/*
			 * Calendar calRet = Calendar.getInstance(); Date dateRet = new Date(); dateRet.setDate(10); dateRet.setMonth(03); dateRet.setMinutes(25); dateRet.setSeconds(00); dateRet.setHours(07);
			 * calRet.setTime(dateRet);
			 */
			flightSegment_type2.setDepartureDateTime(departureDate2);

			/*
			 * Calendar cal2Arr = Calendar.getInstance(); Date date2Arr = new Date(); date2Arr.setDate(10); date2Arr.setMonth(03); date2Arr.setMinutes(25); date2Arr.setSeconds(00);
			 * date2Arr.setHours(15); cal2Arr.setTime(date2Arr);
			 */
			flightSegment_type2.setArrivalDateTime(arrivalDate2);

			RPH_Type rph_Type2 = new RPH_Type();
			rph_Type2.setRPH_Type("1");
			flightSegment_type2.setRPH(rph_Type2);

			FlightNumberType flightNumberType2 = new FlightNumberType();
			FlightNumberType_type0 flightNumberType_type = new FlightNumberType_type0();
			flightNumberType_type.setFlightNumberType_type0(flightnum[1]);
			flightNumberType2.setObject(flightNumberType_type);
			flightSegment_type2.setFlightNumber(flightNumberType2);

			UpperCaseAlphaLength1To2 bookDesigCode2 = new UpperCaseAlphaLength1To2();
			bookDesigCode2.setUpperCaseAlphaLength1To2(ResBookDesigCode[1]);
			flightSegment_type2.setResBookDesigCode(bookDesigCode2);

			PositiveInteger numberInPrty2 = new PositiveInteger("1");
			flightSegment_type2.setNumberInParty(numberInPrty2);

			originDestinationOption_type0Arr[1].addFlightSegment(flightSegment_type2);
			originDestinationOptions_type0.setOriginDestinationOption(originDestinationOption_type0Arr);
			airItineraryType.setOriginDestinationOptions(originDestinationOptions_type0);
			oTA_AirRulesRQ.setAirItinerary(airItineraryType);

			TravelerInfoSummary_type3 travelerInfoSum_type3 = new TravelerInfoSummary_type3();
			NonNegativeInteger noOfSeats = new NonNegativeInteger("1");
			travelerInfoSum_type3.addSeatsRequested(noOfSeats);

			TravelerInformationType travelerInformationType = new TravelerInformationType();
			
			PassengerTypeQuantityType passengerTypeQtyType = new PassengerTypeQuantityType();
			StringLength1To8 code = new StringLength1To8();
			code.setStringLength1To8("ADT");
			passengerTypeQtyType.setCode(code);
			NonNegativeInteger qty = new NonNegativeInteger("1");
			passengerTypeQtyType.setQuantity(qty);
			travelerInformationType.addPassengerTypeQuantity(passengerTypeQtyType);

			// 2nd Passenger
/*			PassengerTypeQuantityType passengerTypeQtyType2 = new PassengerTypeQuantityType();
			StringLength1To8 code2 = new StringLength1To8();
			code2.setStringLength1To8("CHD");
			passengerTypeQtyType2.setCode(code2);
			NonNegativeInteger qty2 = new NonNegativeInteger("1");
			passengerTypeQtyType2.setQuantity(qty2);
			travelerInformationType.addPassengerTypeQuantity(passengerTypeQtyType2);*/

			travelerInfoSum_type3.addAirTravelerAvail(travelerInformationType);

			PriceRequestInformation_type0 priceReqInfo_type0 = new PriceRequestInformation_type0();

			priceReqInfo_type0.setPricingSource(PricingSourceType.Published);
			travelerInfoSum_type3.setPriceRequestInformation(priceReqInfo_type0);

			oTA_AirRulesRQ.setTravelerInfoSummary(travelerInfoSum_type3); 
			StringLength1To128 trNo = new StringLength1To128();
			trNo.setStringLength1To128(transactionid); // ------ Transaction id --------------------
			oTA_AirRulesRQ.setTransactionIdentifier(trNo);
			// ---------------------------------------------------------------------------------------------------------
			logger.info("Sending....");
			OTA_AirRulesRS oTA_AirRulesRS = stub.AirRules(oTA_AirRulesRQ);

			System.out.println("Complete...");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void callCancel() {
		try {
			
			
			ConfigurationContext ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem("D:/xmloutlibs/Axis2/axis2-1.4.1/repository", "D:/xmloutlibs/Axis2/axis2-1.4.1/conf/axis2.xml");
				ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
				ServiceClient sc = stub._getServiceClient();
				sc.engageModule("rampart");
				// sc.engageModule("xmloutlogging");
				Options options = sc.getOptions();
				options.setUserName(username);
				options.setPassword(password);
				options.setTimeOutInMilliSeconds(1000 * 60 * 4);
			
			
		//	ReservationServiceStub stub = new ReservationServiceStub();
			stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(1000 * 60 * 2);

			OTA_CancelRQ oTA_CancelRQ = new OTA_CancelRQ();

			// oTA_CancelRQ.setTarget(Target_type0.Test);
			// oTA_CancelRQ.setTarget(Target_type15.Test);
			oTA_CancelRQ.setVersion(new BigDecimal(1.0));
			POS_Type pOS_Type = new POS_Type();

			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128(echoTokenMain);
			oTA_CancelRQ.setEchoToken(echotoken);

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(user);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			// OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("GBP");
			sourceType[0].setISOCurrency(alphaLength3);
			pOS_Type.setSource(sourceType);

			oTA_CancelRQ.setPOS(pOS_Type);

			UniqueID_type0[] uniqueID_type0Arr = new UniqueID_type0[1];
			uniqueID_type0Arr[0] = new UniqueID_type0();
			StringLength1To64 pnrNo = new StringLength1To64();
			pnrNo.setStringLength1To64(Reservationnumber);
			uniqueID_type0Arr[0].setID(pnrNo);
			OTA_CodeType uniqIDType = new OTA_CodeType();
			uniqIDType.setOTA_CodeType("PNR");
			uniqueID_type0Arr[0].setType(uniqIDType);
			oTA_CancelRQ.setUniqueID(uniqueID_type0Arr);
			Calendar cal = Calendar.getInstance();
			oTA_CancelRQ.setTimeStamp(cal);
			
			Target_type10 ta = Target_type10.Test;
			oTA_CancelRQ.setTarget(ta);
			// oTA_CancelRQ.setCancelType(TransactionActionType.Cancel);
			OTA_CancelRS oTA_CancelRS = stub.AirCancel(oTA_CancelRQ);
			System.out.println("Cancelled >>> " + oTA_CancelRS.getStatus());
		} catch (Exception e) {
			System.out.println("Error in Calcel msg >>> " + e);
		}

	}

	@SuppressWarnings("deprecation")
	public void airSeatMap() {

		try {
			ConfigurationContext ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem("D:/xmloutlibs/Axis2/axis2-1.4.1/repository", "D:/xmloutlibs/Axis2/axis2-1.4.1/conf/axis2.xml");
				ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
				ServiceClient sc = stub._getServiceClient();
				sc.engageModule("rampart");
				// sc.engageModule("xmloutlogging");
				Options options = sc.getOptions();
				options.setUserName(username);
				options.setPassword(password);
				options.setTimeOutInMilliSeconds(1000 * 60 * 4);

			OTA_AirSeatMapRQ oTA_AirSeatMapRQ = new OTA_AirSeatMapRQ();

			// oTA_CancelRQ.setTarget(Target_type0.Test);
			oTA_AirSeatMapRQ.setTarget(Target_type28.Test);
			oTA_AirSeatMapRQ.setVersion(new BigDecimal(1.0));
			POS_Type pOS_Type = new POS_Type();

			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128("Flights");
			oTA_AirSeatMapRQ.setEchoToken(echotoken);

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64("TRIP_TOUR_1");
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			// OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			CompanyNameType cname = new CompanyNameType();

			cname.setStringLength0To128("Amadeus");
			requestorID_type0.setCompanyName(cname);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("GBP");
			sourceType[0].setISOCurrency(alphaLength3);
			pOS_Type.setSource(sourceType);

			oTA_AirSeatMapRQ.setPOS(pOS_Type);

			SeatMapRequests_type0 seatMapRequests_type0 = new SeatMapRequests_type0();

			SeatMapRequest_type0 seatMapRequest_type0 = new SeatMapRequest_type0();
			FlightSegmentType flightSegmentType = new FlightSegmentType();

			DepartureAirport_type0 departureAirport_type0 = new DepartureAirport_type0();
			StringLength1To8 depAirportCode = new StringLength1To8();
			depAirportCode.setStringLength1To8("SIN");
			departureAirport_type0.setLocationCode(depAirportCode);
			flightSegmentType.setDepartureAirport(departureAirport_type0);

			ArrivalAirport_type0 arrivalAirport_type = new ArrivalAirport_type0();
			StringLength1To8 arrlocCode = new StringLength1To8();
			arrlocCode.setStringLength1To8("SGN");
			arrivalAirport_type.setLocationCode(arrlocCode);
			flightSegmentType.setArrivalAirport(arrivalAirport_type);

			FlightNumberType flightNumberType = new FlightNumberType();
			FlightNumberType_type0 flightNumberType_type0 = new FlightNumberType_type0();
			flightNumberType_type0.setFlightNumberType_type0("740");
			flightNumberType.setObject(flightNumberType_type0);
			flightSegmentType.setFlightNumber(flightNumberType);

			Calendar calRet = Calendar.getInstance();
			Date dateRet = new Date();
			dateRet.setDate(10);
			dateRet.setMonth(03);
			dateRet.setMinutes(55);
			dateRet.setSeconds(00);
			dateRet.setHours(22);
			calRet.setTime(dateRet);
			flightSegmentType.setDepartureDateTime("2011-05-29T12:50:00");

			CompanyNameType companyName = new CompanyNameType();
			StringLength1To16 marketAirlinecode = new StringLength1To16();
			marketAirlinecode.setStringLength1To16("VN");

			companyName.setCode(marketAirlinecode);
			companyName.setStringLength0To128("");
			flightSegmentType.setMarketingAirline(companyName);

			seatMapRequest_type0.setFlightSegmentInfo(flightSegmentType);

			SeatDetails_type0 seatDetails_type0 = new SeatDetails_type0();
			CabinClass_type0 cabinClass_type0 = new CabinClass_type0();

			cabinClass_type0.setCabinType(CabinType.Economy);
			seatDetails_type0.addCabinClass(cabinClass_type0);

			ResBookDesignations_type0 resBookDesigs_type0 = new ResBookDesignations_type0();
			ResBookDesignation_type0 resBookDesig_type0 = new ResBookDesignation_type0();
			UpperCaseAlphaLength1To2 upperCaseAlphaLength1To2 = new UpperCaseAlphaLength1To2();
			upperCaseAlphaLength1To2.setUpperCaseAlphaLength1To2("N");
			resBookDesig_type0.setResBookDesigCode(upperCaseAlphaLength1To2);
			resBookDesigs_type0.addResBookDesignation(resBookDesig_type0);
			seatDetails_type0.setResBookDesignations(resBookDesigs_type0);

			seatMapRequest_type0.setSeatDetails(seatDetails_type0);

			seatMapRequests_type0.addSeatMapRequest(seatMapRequest_type0);
			oTA_AirSeatMapRQ.setSeatMapRequests(seatMapRequests_type0);
			OTA_AirSeatMapRS oTA_AirSeatMapRS = stub.AirSeatMap(oTA_AirSeatMapRQ);

		} catch (Exception e) {
			System.out.println("Error >>> " + e);
		}

	}

	public void eticket() {

		try {
			ConfigurationContext ctx = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem("D:/xmloutlibs/Axis2/axis2-1.4.1/repository", "D:/xmloutlibs/Axis2/axis2-1.4.1/conf/axis2.xml");
				ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
				ServiceClient sc = stub._getServiceClient();
				sc.engageModule("rampart");
				// sc.engageModule("xmloutlogging");
				Options options = sc.getOptions();
				options.setUserName(username);
				options.setPassword(password);
				options.setTimeOutInMilliSeconds(1000 * 60 * 4);

			OTA_AirDemandTicketRQ oTA_AirDemandTicketRQ = new OTA_AirDemandTicketRQ();
			
			Calendar calendar = Calendar.getInstance();
			oTA_AirDemandTicketRQ.setTimeStamp(calendar);

			oTA_AirDemandTicketRQ.setTarget(Target_type32.Test);
			oTA_AirDemandTicketRQ.setVersion(new BigDecimal(1.0));
			POS_Type pOS_Type = new POS_Type();

			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128("Flights");
			oTA_AirDemandTicketRQ.setEchoToken(echotoken);

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64("TRIP_TOUR_1");
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();

			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			CompanyNameType cname = new CompanyNameType();

			cname.setStringLength0To128("Amadeus");
			requestorID_type0.setCompanyName(cname);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("GBP");
			sourceType[0].setISOCurrency(alphaLength3);
			pOS_Type.setSource(sourceType);
			oTA_AirDemandTicketRQ.setPOS(pOS_Type);

			DemandTicketDetail_type0 demandTicketDetail_type0 = new DemandTicketDetail_type0();
			MessageFunction_type0 msgFunction_type0 = new MessageFunction_type0();

			// msgFunction_type0.setFunction(Function_type0.ET);
			demandTicketDetail_type0.addMessageFunction(msgFunction_type0);

			UniqueID_Type uniqueID_Type = new UniqueID_Type();
			StringLength1To64 bookingID = new StringLength1To64();
			bookingID.setStringLength1To64("F4862W160915");
			uniqueID_Type.setID(bookingID);
			demandTicketDetail_type0.setBookingReferenceID(uniqueID_Type);

			oTA_AirDemandTicketRQ.setDemandTicketDetail(demandTicketDetail_type0);
			OTA_AirDemandTicketRS oTA_AirDemandTicketRS = stub.AirTicket(oTA_AirDemandTicketRQ);
			System.out.println("");

		} catch (Exception e) {

		}

	}

	public void pnrDownload() {
		try {
			ReservationServiceStub stub = new ReservationServiceStub();
			stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(1000 * 60 * 2);

			OTA_ReadRQ oTA_ReadRQ = new OTA_ReadRQ();

			// oTA_ReadRQ.setTarget(Target_type14.Test);
			oTA_ReadRQ.setVersion(new BigDecimal(1.0));
			POS_Type pOS_Type = new POS_Type();

			StringLength1To128 echotoken = new StringLength1To128();
			echotoken.setStringLength1To128("externFlights");
			oTA_ReadRQ.setEchoToken(echotoken);

			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64("TRIP_TOUR_1");
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();

			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			CompanyNameType cname = new CompanyNameType();

			cname.setStringLength0To128("Amadeus");
			requestorID_type0.setCompanyName(cname);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("GBP");
			sourceType[0].setISOCurrency(alphaLength3);
			pOS_Type.setSource(sourceType);
			oTA_ReadRQ.setPOS(pOS_Type);

			UniqueID_Type uniqueID_Type = new UniqueID_Type();
			StringLength1To64 pnrNo = new StringLength1To64();
			pnrNo.setStringLength1To64("F0001W161110");
			uniqueID_Type.setID(pnrNo);
			oTA_ReadRQ.setUniqueID(uniqueID_Type);

			OTA_AirBookRS oTA_AirBookRS = stub.AirPNRRead(oTA_ReadRQ);

		} catch (Exception e) {

		}
	}

	/**
	 * @param args
	 */
	static Logger logger = null;
	static String url = null;
	static String username = null;
	static String password = null;
	String echoTokenMain = null;
	String transactionid = null;
	String Reservationnumber;
	String user = null;
	Date indate = null;
	Date outdate = null;
	String citycode = null;
	String cacheid = null;
	String Availcacheid = null;
	String[] childages = null;
	String noOfAdults = "";
	String noOfChildren = "";
	String departureDate1 = "";
	String departureDate2 = "";
	String departureDate3 = "";
	String arrivalDate1 = "";
	String arrivalDate2 = "";
	String arrivalDate3 = "";
	String searchSecondDeptDate = "";
	String searchDepatingDate = "";
	String searchThirdDeptDate = "";
	String depAC1 = "";
	String arrAC1 = "";
	String depAC2 = "";
	String arrAC2 = "";
	String depAC3 = "";
	String arrAC3 = "";
	String flightSequence = "";
	String companyname = null;
	String packageId = null;
	String ticketingTimeLimit = null;
	String correlationID = "";
	String directionIndicator = "";
	String officeId = "";

	String routingIds = null;
	String[] inoutids = null;
	String[] flightnum = null;
	String[] airlineCode = null;
	String[] airlineName = null;
	String[] ResBookDesigCode = null;

	boolean isMultipleAIREnable = false;
	boolean isRemarkEnabled = false;
	boolean isOneWay = false;
	boolean isNonstop = false;
	boolean isMulticity = false;

	Target_type20 air_search_Target = null;

	String server = null;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
	Calendar calendar = Calendar.getInstance();

}