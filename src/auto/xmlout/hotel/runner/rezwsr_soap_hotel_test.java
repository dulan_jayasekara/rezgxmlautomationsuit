package auto.xmlout.hotel.runner;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.databinding.types.NonNegativeInteger;
import org.apache.axis2.databinding.types.PositiveInteger;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import xmlout.org.opentravel.www.ota._2003._05.*;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Address_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AlphaLength3;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AlternateAvailability_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AvailRequestSegment_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.AvailRequestSegments_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.BasicPropertyInfoType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.BookingChannel_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CodeContext_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CompanyNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CountryNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Criterion_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.CustomerType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DateTimeSpanType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.DropOff_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.EmailType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FilterResultsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Filter_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.FreeTextType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuaranteeAccepted_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuaranteeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuaranteesAccepted_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuestCountType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.GuestCount_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelDescriptiveInfo_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelDescriptiveInfos_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelRef_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelResRequestType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelResRequestTypeSequence_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelReservationType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelReservationsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelSearchCriteriaType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelStay_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ISO3166;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.MMYYDate;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Numeric0To999;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.NumericStringLength1To19;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.NumericStringLength1To8;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelResRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelResRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_ReadRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_ResRetrieveRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.POS_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentCardCodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentCardType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PaymentTransactionTypeCode_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PersonNameType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.PickUp_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ProfileInfo_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ProfileType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ProfilesType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Property_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Reasons_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RefPoint_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RequestorID_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResGlobalInfoType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResGuest_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResGuestsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ReservationDetails_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ReturnResultsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStayCandidateType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStayCandidates_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStay_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStay_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStaysType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomTypeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomTypes_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortBy_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortOrder_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortResults_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SourceType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StateProvCodeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StateProvType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StaticDataRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StaticDataRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StaticData_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To128;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To16;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To255;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To32;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To64;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To8;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TPA_ExtensionsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type10;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type12;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type2;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type4;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type6;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type8;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Telephone_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TimeSpan_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UniqueID_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UniqueID_type0;

public class rezwsr_soap_hotel_test implements Runnable {

	public void callserver() {

		boolean normalSearch = false, availabillity = false, reservation = false, cancellation = false, info = false,
				resretrieve = false, download = false;

		try {
	        server = "staging";
            if (server.equals("live")) {

				// url = "http://xml.secure-reservation.com/axis2/services/ReservationService";
				// url = "http://192.168.1.153:9090/axis2/services/ReservationService";
				url = "http://rezwsr.secure-reservation.com:9991/axis2/services/ReservationService";

				agentid = "TRIP_LIVE_1";
				username = "martinliveto";
				password = "123456";
				// agentid = "UNISHORE"; username = "unishore"; password = "123456";
				// agentid = "V3_TEST"; username = "testxmlout"; password = "123456";

				hotel_info_Target = Target_type6.Production;
				hotel_room_Target = Target_type2.Production;
				hotel_search_Target = Target_type0.Production;
				hotel_res_Target = Target_type4.Production;
				hotel_cncl_Target = Target_type10.Production;

			} else if (server.equals("staging")) {

				url = "http://192.168.1.126:9090/axis2/services/ReservationService";
				// url = "http://192.168.1.153:9090/axis2/services/ReservationService";
				// url = "http://xml-test.secure-reservation.com/axis2/services/ReservationService";
				// url = "http://rezwsr.secure-reservation.com/axis2/services/ReservationService";

				// agentid = "UNISHORE"; username = "unishore"; password = "123456";
				// agentid = "UNISHORE_T2"; username = "unishore2"; password = "123456";
				// agentid = "TRIP_TOUR_3"; username = "martinto"; password = "123456";
				agentid = "REZBASE_V3_TEST_CHARITHA";
				username = "charithato";
				password = "123456";
				// agentid = "TRIP_TOUR_3"; username = "martinto"; password = "123456";

				hotel_info_Target = Target_type6.Test;
				hotel_room_Target = Target_type2.Test;
				hotel_search_Target = Target_type0.Test;
				hotel_res_Target = Target_type4.Test;
				hotel_cncl_Target = Target_type10.Test;

			} else {

				throw new Exception("Please check your \"server\" parameter");

			}

			// ###################################################################################################################

			// EchoToken = "Hotel";
			// EchoToken = "HotelV3";
			// EchoToken = "ServiceHotel";
			// EchoToken = "RemoteHotel";
			// EchoToken = "ExternalHotel";
			EchoToken = "Default";

			noOfRooms = 1;

			noOfAdults = new Integer[] { 1};
			noOfChildren = new Integer[] { 0};
			childAges = new String[] { "0"};

	    //  airportcode = "NYC";

		    citycode = "3086"; // 1112 - LON, 1350 - NYC, 436-MAD, 670-ROM, 3086
 
			hotelId = "307911"; // hCode of search response
			roomId = "SGL-E10_FULL BOARD_AS_1";
			room_vendor = "hotelbeds_v2"; // BrandCode of search response // hotelbeds_v2, INV13

			availabilityHotelIndex = 15;
			availabilityRoomIndex = 0;

			cacheid = "hs-bdea7c85-85ca-4b27-bc00-7a2425509997"; // singleHotelSearch and availability
			Availcacheid = "ha-hs-19386d8b-e129-4688-88a7-c30505bababf-1437983730-1"; // reservation

			CancellationrefNo = "WSR.HSR.384033899.20";

			indate.set(2015, Calendar.SEPTEMBER, 28, 00, 00, 00);
			outdate.set(2015, Calendar.SEPTEMBER, 29, 00, 00, 00);

			indate.set(Calendar.MILLISECOND, 0);
			outdate.set(Calendar.MILLISECOND, 0);

			this.isOnlinePayment = false;

   //  normalSearch = true; //use for tripserve .1
		  // singleHotelSearch = false; // This is for LayOver Searches.
        //  availabillity = true; //Use for tripserve . 2
          //reservation = true; // use for tripserve. 3
		  //	 cancellation = true;
			 info = true;
			// resretrieve = true;
			// download = true;

			// ###################################################################################################################

			System.setProperty("javax.net.ssl.trustStore", "C:/rezsystem/jboss-4.0.3SP1/bin/certs/QA.keystore");
			System.setProperty("javax.net.ssl.trustStorePassword", "123456");

			if (noOfAdults.length < noOfRooms)
				throw new Exception("Insufficent Adult Count");
			if (noOfChildren.length < noOfRooms)
				throw new Exception("Insufficent Child Count");
			if (childAges.length < noOfRooms)
				throw new Exception("Insufficent Child Ages");

			if (normalSearch && !singleHotelSearch) {

				returnRoomTypes = false;
				if (agentid.startsWith("UNI"))
					citycode = null;
				hotelId = null;
				roomId = null;
				room_vendor = null;

			}

			if (normalSearch) 
				HotelSearch();
			if (availabillity)
				RoomSearch();
			if (reservation)
				HotelReservation();
			if (info)
				HotelInfo();
			if (cancellation)
				HotelCancellation();
			if (resretrieve)
				HotelResRetrieve();
			if (download)
				HotelDownload();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void HotelSearch() {
		System.out.println("================ start hotel search =====================");
		try {

			OTA_HotelSearchRQ SearchRQ = new OTA_HotelSearchRQ();

			SearchRQ.setTarget(hotel_search_Target);

			Calendar calendar = Calendar.getInstance();
			SearchRQ.setTimeStamp(calendar);

			StringLength1To128 stringLength1To128 = new StringLength1To128();
			stringLength1To128.setStringLength1To128(cacheid);
			//if (singleHotelSearch)
				SearchRQ.setTransactionIdentifier(stringLength1To128);

			POS_Type pos = new POS_Type();
			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();

			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(agentid);
			requestorID_type0.setID(st132);

			OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);

			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);

			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("USD");
			sourceType[0].setISOCurrency(alphaLength3);
			pos.setSource(sourceType);
			SearchRQ.setPOS(pos);

			StringLength1To128 stringLength1To12811 = new StringLength1To128();
			stringLength1To12811.setStringLength1To128(EchoToken);
			SearchRQ.setEchoToken(stringLength1To12811);

			PositiveInteger positiveInteger = new PositiveInteger("999");
			SearchRQ.setMaxResponses(positiveInteger);

			NonNegativeInteger nonNegativeInteger = new NonNegativeInteger("1");
			SearchRQ.setSequenceNmbr(nonNegativeInteger);

			HotelSearchCriteriaType hotelSearchCriteriaType = new HotelSearchCriteriaType();

			Criterion_type0[] criterion_type0 = new Criterion_type0[1];
			criterion_type0[0] = new Criterion_type0();

			RefPoint_type0[] refPoint_type0 = new RefPoint_type0[1];
			refPoint_type0[0] = new RefPoint_type0();
			StringLength1To8 stringLength1To8 = new StringLength1To8();

			if (null != citycode) {
				stringLength1To8.setStringLength1To8(citycode);
			} else {
				stringLength1To8.setStringLength1To8(airportcode);
				refPoint_type0[0].setCodeContext(CodeContext_type0.airport);
			}

			refPoint_type0[0].setCode(stringLength1To8);
			criterion_type0[0].setRefPoint(refPoint_type0);
			criterion_type0[0].setAlternateAvailability(AlternateAvailability_type0.Always);

			DateTimeSpanType dateTimeSpanType = new DateTimeSpanType();

			dateTimeSpanType.setStart(indate);
			dateTimeSpanType.setEnd(outdate);

			criterion_type0[0].setStayDateRange(dateTimeSpanType);

			RoomStayCandidates_type0 roomStayCandidates_type0 = new RoomStayCandidates_type0();
			RoomStayCandidateType[] roomStayCandidateType = new RoomStayCandidateType[noOfRooms];

			for (int i = 0; i < noOfRooms; i++) {

				roomStayCandidateType[i] = new RoomStayCandidateType();
				GuestCountType guestCountType = new GuestCountType();

				GuestCount_type0[] guestCount_type0 = new GuestCount_type0[noOfAdults[i] + noOfChildren[i]];

				int roomPassCount;

				for (roomPassCount = 0; roomPassCount < noOfAdults[i]; roomPassCount++) {

					guestCount_type0[roomPassCount] = new GuestCount_type0();
					OTA_CodeType oTA_CodeType = new OTA_CodeType();
					oTA_CodeType.setOTA_CodeType("10");
					guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType);

				}

				for (int childCount = 0; childCount < noOfChildren[i]; childCount++, roomPassCount++) {

					guestCount_type0[roomPassCount] = new GuestCount_type0();

					OTA_CodeType oTA_CodeType22 = new OTA_CodeType();
					oTA_CodeType22.setOTA_CodeType("8");

					Numeric0To999 numeric0To9993 = new Numeric0To999();
					BigInteger b3 = new BigInteger(childAges[i].split(",")[childCount]);
					numeric0To9993.setNumeric0To999(b3);
					guestCount_type0[roomPassCount].setAge(numeric0To9993);
					guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType22);

				}

				guestCountType.setGuestCount(guestCount_type0);
				roomStayCandidateType[i].setGuestCounts(guestCountType);

			}

			roomStayCandidates_type0.setRoomStayCandidate(roomStayCandidateType);

			criterion_type0[0].setRoomStayCandidates(roomStayCandidates_type0);

			/*
			 * Award_type0[] award_type0 = new Award_type0[1]; award_type0[0] = new Award_type0(); award_type0[0].setProvider("RezG"); award_type0[0].setRating("5EST");
			 * criterion_type0[0].setAward(award_type0);
			 */

			if (singleHotelSearch) {

				HotelRef_type0 hotelRef_type0 = new HotelRef_type0();

				StringLength1To8 stringLength1To82 = new StringLength1To8();
				stringLength1To82.setStringLength1To8(citycode);
				hotelRef_type0.setHotelCityCode(stringLength1To82);
				StringLength1To16 stringLength1To16 = new StringLength1To16();
				stringLength1To16.setStringLength1To16(hotelId);
				hotelRef_type0.setHotelCode(stringLength1To16);
				StringLength1To64 stringLength1To64 = new StringLength1To64();
				stringLength1To64.setStringLength1To64(room_vendor);
				hotelRef_type0.setBrandCode(stringLength1To64);
				criterion_type0[0].addHotelRef(hotelRef_type0);

			}

			TPA_ExtensionsType tPA_ExtensionsType = new TPA_ExtensionsType();

			ReservationDetails_type0 reservationDetails_type0 = new ReservationDetails_type0();

			DropOff_type0 dropOff_type0 = new DropOff_type0();
			StringLength1To255 stringLength1To255 = new StringLength1To255();
			stringLength1To255.setStringLength1To255("Domestic");
			dropOff_type0.setShipCompany(stringLength1To255);
			reservationDetails_type0.setDropOff(dropOff_type0);

			PickUp_type0 pickUp_type0 = new PickUp_type0();
			StringLength1To255 stringLength1To2552 = new StringLength1To255();
			stringLength1To2552.setStringLength1To255("International");
			pickUp_type0.setShipCompany(stringLength1To2552);
			reservationDetails_type0.setPickUp(pickUp_type0);

			tPA_ExtensionsType.setReservationDetails(reservationDetails_type0);

			SortResults_type0 sortResults_type0ResultsType0 = new SortResults_type0();
			// sortResults_type0ResultsType0.setSortBy(SortBy_type0.PRICE );
			// sortResults_type0ResultsType0.setSortOrder(SortOrder_type0.ASC);
			sortResults_type0ResultsType0.setSortBy(SortBy_type0.ASC);
			sortResults_type0ResultsType0.setSortOrder(SortOrder_type0.PRICE);
			tPA_ExtensionsType.addSortResults(sortResults_type0ResultsType0);

			if (server.equals("staging")) {

				ReturnResultsType returnResultsType = new ReturnResultsType();
				returnResultsType.setDescription(true);
				returnResultsType.setContract(true);
				returnResultsType.setRoomTypes(returnRoomTypes);
				returnResultsType.setImages(true);
				tPA_ExtensionsType.setReturnResults(returnResultsType);

			}

			/*
			 * FilterResultsType filterResultsType = new FilterResultsType(); filterResultsType.setFilter(Filter_type0.HotelNameFilter); filterResultsType.setStringLength0To255("H12");
			 * tPA_ExtensionsType.addFilterResults(filterResultsType );
			 */

			FilterResultsType filterResultsType = new FilterResultsType();
			filterResultsType.setFilter(Filter_type0.AvailableHotelFilter);
			filterResultsType.setStringLength0To255(hotelId);
			tPA_ExtensionsType.addFilterResults(filterResultsType);

			/*
			 * FilterResultsType filterResultsType = new FilterResultsType(); filterResultsType.setFilter(Filter_type0.StarCategoryFilter); filterResultsType.setStringLength0To255("3");
			 * tPA_ExtensionsType.addFilterResults(filterResultsType );
			 * 
			 * 
			 * FilterResultsType filterResultsType2 = new FilterResultsType(); filterResultsType2.setFilter(Filter_type0.HotelNameFilter); filterResultsType2.setStringLength0To255("Test Hotel");
			 * tPA_ExtensionsType.addFilterResults(filterResultsType2 );
			 */

			// criterion_type0[0].setTPA_Extensions(tPA_ExtensionsType);

			hotelSearchCriteriaType.setCriterion(criterion_type0);
			SearchRQ.setCriteria(hotelSearchCriteriaType);

			SearchRQ.setVersion(new BigDecimal(3.0));

			System.out.println("-----Posting-----");

			OTA_HotelSearchRS SearchRS = buildReservationServiceStub().HotelSearch(SearchRQ);

			System.out.println("-----Got Responce-----\n");

			Property_type0[] Property_type0 = SearchRS.getOTA_HotelSearchRSChoice_type0().getOTA_HotelSearchRSSequence_type0().getProperties().getProperty();

			System.out.println("TransactionIdentifier:" + SearchRS.getTransactionIdentifier());
			System.out.println("Property_type0.length-->" + Property_type0.length + "\n");
			this.cacheid = SearchRS.getTransactionIdentifier().toString();

			for (int p = 0; p < Property_type0.length; p++) {

				String hcode = Property_type0[p].getHotelCode().getStringLength1To16();
				String hname = Property_type0[p].getHotelName().getStringLength1To128();
				String brandcode = Property_type0[p].getBrandCode().getStringLength1To64();

				if (p == availabilityHotelIndex) {
					this.hotelId = hcode;
					this.room_vendor = brandcode;
				}
				
				String Romcodes = "";

				if (null != Property_type0[p].getTPA_Extensions().getRoomTypes()) {

					for (int k = 0; k < Property_type0[p].getTPA_Extensions().getRoomTypes().getRoomType().length; k++) {
						Romcodes += Property_type0[p].getTPA_Extensions().getRoomTypes().getRoomType()[k].getRoomTypeCode().getStringLength1To64() + "|";
						if (p == availabilityHotelIndex && k == availabilityRoomIndex){
							this.roomId = Property_type0[p].getTPA_Extensions().getRoomTypes().getRoomType()[k].getRoomTypeCode().getStringLength1To64();
						}
					}
				
				}

				if (hcode == null || hcode.equals("-"))
					throw new Exception("Hotel code is not there !!");

				//org.apache.axis2.databinding.types.URI urlz = Property_type0[p].getTPA_Extensions().getMultimediaDescriptions().getMultimediaDescription()[0].getImageItems().getImageItem()[0].getImageFormat()[0].getURL();
				// String imagepath = urlz.getPath();
				// System.out.println("imagepath befor decode-->"+imagepath);
				// imagepath = java.net.URLDecoder.decode(imagepath, "UTF-8");
				// System.out.println("imagepath after decode-->"+imagepath);

				System.out.println("----[" + p + "/" + Property_type0.length + "]-------------------------------------");
				System.out.println("BrandCode --> " + brandcode);
				System.out.println("HCode--> " + hcode);
				System.out.println("HName--> " + hname);
				System.out.println("RoomCodes--> " + Romcodes);
				System.out.println("");

			}

			System.out.println("TransactionIdentifier-->" + SearchRS.getTransactionIdentifier().getStringLength1To128());

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("================ end hotel search =====================\n");
	}

	public void RoomSearch() throws Exception {
		System.out.println("================ start Room Search =====================");
		try {

			System.out.println("hotelId-->" + hotelId);

			OTA_HotelAvailRQ oTA_HotelAvailRQ4 = new OTA_HotelAvailRQ();

			POS_Type pos = new POS_Type();
			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(agentid);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("USD");
			sourceType[0].setISOCurrency(alphaLength3);
			pos.setSource(sourceType);
			oTA_HotelAvailRQ4.setPOS(pos);

			StringLength1To128 stringLength1To128 = new StringLength1To128();
			stringLength1To128.setStringLength1To128(EchoToken);
			oTA_HotelAvailRQ4.setEchoToken(stringLength1To128);

			oTA_HotelAvailRQ4.setTarget(hotel_room_Target);

			oTA_HotelAvailRQ4.setVersion(new BigDecimal(3.0));

			Calendar calendar = Calendar.getInstance();
			oTA_HotelAvailRQ4.setTimeStamp(calendar);

			/*
			 * NonNegativeInteger nonNegativeInteger = new NonNegativeInteger("123456"); oTA_HotelAvailRQ4.setSequenceNmbr(nonNegativeInteger);
			 */

			StringLength1To128 stringLength1To12811 = new StringLength1To128();
			stringLength1To12811.setStringLength1To128(cacheid);
			oTA_HotelAvailRQ4.setTransactionIdentifier(stringLength1To12811);

			AvailRequestSegments_type0 availRequestSegments_type0 = new AvailRequestSegments_type0();

			AvailRequestSegment_type0[] availRequestSegment_type0 = new AvailRequestSegment_type0[1];
			availRequestSegment_type0[0] = new AvailRequestSegment_type0();

			HotelSearchCriteriaType hotelSearchCriteriaType = new HotelSearchCriteriaType();

			Criterion_type0[] criterion_type0 = new Criterion_type0[1];
			criterion_type0[0] = new Criterion_type0();

			/*
			 * RefPoint_type0[] refPoint_type0 = new RefPoint_type0[1]; refPoint_type0[0] = new RefPoint_type0(); StringLength1To8 stringLength1To8 = new StringLength1To8();
			 * stringLength1To8.setStringLength1To8("CY1"); refPoint_type0[0].setCode(stringLength1To8); criterion_type0[0].setRefPoint(refPoint_type0);
			 */

			HotelRef_type0[] hotelRef_type0 = new HotelRef_type0[1];
			hotelRef_type0[0] = new HotelRef_type0();
			StringLength1To8 stringLength1To8 = new StringLength1To8();
			stringLength1To8.setStringLength1To8(citycode);
			hotelRef_type0[0].setHotelCityCode(stringLength1To8);
			StringLength1To16 stringLength1To16 = new StringLength1To16();
			stringLength1To16.setStringLength1To16(hotelId);
			hotelRef_type0[0].setHotelCode(stringLength1To16);
			StringLength1To64 stringLength1To641 = new StringLength1To64();
			stringLength1To641.setStringLength1To64(room_vendor);
			hotelRef_type0[0].setBrandCode(stringLength1To641);

			criterion_type0[0].setHotelRef(hotelRef_type0);

			DateTimeSpanType dateTimeSpanType = new DateTimeSpanType();

			dateTimeSpanType.setStart(indate);
			dateTimeSpanType.setEnd(outdate);

			criterion_type0[0].setStayDateRange(dateTimeSpanType);

			String[] rid = roomId.split(",");
			if (rid.length != noOfRooms)
				throw new Exception("No of Selected Room Types are not equal to the No of Requested Rooms");

			RoomStayCandidates_type0 roomStayCandidates_type0 = new RoomStayCandidates_type0();
			RoomStayCandidateType[] roomStayCandidateType = new RoomStayCandidateType[noOfRooms];

			for (int i = 0; i < noOfRooms; i++) {

				roomStayCandidateType[i] = new RoomStayCandidateType();
				GuestCountType guestCountType = new GuestCountType();

				GuestCount_type0[] guestCount_type0 = new GuestCount_type0[noOfAdults[i] + noOfChildren[i]];

				int roomPassCount;

				for (roomPassCount = 0; roomPassCount < noOfAdults[i]; roomPassCount++) {

					guestCount_type0[roomPassCount] = new GuestCount_type0();
					OTA_CodeType oTA_CodeType = new OTA_CodeType();
					oTA_CodeType.setOTA_CodeType("10");
					guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType);

				}

				for (int childCount = 0; childCount < noOfChildren[i]; childCount++, roomPassCount++) {

					guestCount_type0[roomPassCount] = new GuestCount_type0();

					OTA_CodeType oTA_CodeType22 = new OTA_CodeType();
					oTA_CodeType22.setOTA_CodeType("8");

					Numeric0To999 numeric0To9993 = new Numeric0To999();
					BigInteger b3 = new BigInteger(childAges[i].split(",")[childCount]);
					numeric0To9993.setNumeric0To999(b3);
					guestCount_type0[roomPassCount].setAge(numeric0To9993);
					guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType22);

				}

				guestCountType.setGuestCount(guestCount_type0);
				roomStayCandidateType[i].setGuestCounts(guestCountType);

				StringLength1To64 stringLength1To64 = new StringLength1To64();
				System.out.println("RoomSearch().rid[i] : " + rid[i] + " added!");

				stringLength1To64.setStringLength1To64(rid[i]);
				roomStayCandidateType[i].setRoomTypeCode(stringLength1To64);

			}

			roomStayCandidates_type0.setRoomStayCandidate(roomStayCandidateType);
			criterion_type0[0].setRoomStayCandidates(roomStayCandidates_type0);

			/*
			 * Award_type0[] award_type0 = new Award_type0[1]; award_type0[0] = new Award_type0(); award_type0[0].setProvider("Rezg"); award_type0[0].setRating("5");
			 * criterion_type0[0].setAward(award_type0);
			 */

			hotelSearchCriteriaType.setCriterion(criterion_type0);
			availRequestSegment_type0[0].setHotelSearchCriteria(hotelSearchCriteriaType);

			availRequestSegments_type0.setAvailRequestSegment(availRequestSegment_type0);

			oTA_HotelAvailRQ4.setAvailRequestSegments(availRequestSegments_type0);

			OTA_HotelAvailRS oTA_HotelAvailRS = (stub == null ? buildReservationServiceStub() : stub).HotelAvailability(oTA_HotelAvailRQ4);

			System.out.println("TransactionIdentifier-->" + oTA_HotelAvailRS.getTransactionIdentifier());
			this.Availcacheid = oTA_HotelAvailRS.getTransactionIdentifier().toString();

			HotelStay_type0 hotelStay_type0 = oTA_HotelAvailRS.getOTA_HotelAvailRSChoice_type0().getOTA_HotelAvailRSSequence_type0().getHotelStays().getHotelStay()[0];

			String hcode = hotelStay_type0.getBasicPropertyInfo().getHotelCode().getStringLength1To16();
			String hname = hotelStay_type0.getBasicPropertyInfo().getHotelName().getStringLength1To128();
			String BrandCode = hotelStay_type0.getBasicPropertyInfo().getBrandCode().getStringLength1To64();

			System.out.println("hcode-->" + hcode);
			System.out.println("hname-->" + hname);
			System.out.println("BrandCode-->" + BrandCode);

			RoomStay_type0[] roomStay_type0s = oTA_HotelAvailRS.getOTA_HotelAvailRSChoice_type0().getOTA_HotelAvailRSSequence_type0().getRoomStays().getRoomStay();

			for (int k = 0; k < roomStay_type0s.length; k++) {

				String RoomTypeCode = roomStay_type0s[k].getRoomTypes().getRoomType()[0].getRoomTypeCode().getStringLength1To64();
				String RoomType = roomStay_type0s[k].getRoomTypes().getRoomType()[0].getRoomType().getStringLength1To64();

				System.out.println("RoomTypeCode-->" + RoomTypeCode);
				System.out.println("RoomType-->" + RoomType);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("================ end Room Search =====================\n");
	}

	public void HotelInfo() {

		try {

			System.out.println("hotelId-->" + hotelId);

			OTA_HotelDescriptiveInfoRQ oTA_HotelDescriptiveInfoRQ2 = new OTA_HotelDescriptiveInfoRQ();

			POS_Type pos = new POS_Type();
			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(agentid);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("USD");
			sourceType[0].setISOCurrency(alphaLength3);
			pos.setSource(sourceType);
			oTA_HotelDescriptiveInfoRQ2.setPOS(pos);
			StringLength1To128 stringLength1To128 = new StringLength1To128();
			stringLength1To128.setStringLength1To128(EchoToken);
			oTA_HotelDescriptiveInfoRQ2.setEchoToken(stringLength1To128);

			StringLength1To128 stringLength1To1281 = new StringLength1To128();
			stringLength1To1281.setStringLength1To128(cacheid);
			oTA_HotelDescriptiveInfoRQ2.setTransactionIdentifier(stringLength1To1281);

			oTA_HotelDescriptiveInfoRQ2.setTarget(hotel_info_Target);
			oTA_HotelDescriptiveInfoRQ2.setVersion(new BigDecimal(3.0));

			Calendar calendar = Calendar.getInstance();
			oTA_HotelDescriptiveInfoRQ2.setTimeStamp(calendar);

			HotelDescriptiveInfos_type0 hotelDescriptiveInfos_type0 = new HotelDescriptiveInfos_type0();
			HotelDescriptiveInfo_type0 hotelDescriptiveInfo_type0 = new HotelDescriptiveInfo_type0();
			StringLength1To8 stringLength1To8 = new StringLength1To8();
			stringLength1To8.setStringLength1To8(citycode);
			hotelDescriptiveInfo_type0.setHotelCityCode(stringLength1To8);
			StringLength1To16 stringLength1To16 = new StringLength1To16();
			System.out.println("hotelCode--->" + hotelId);
			stringLength1To16.setStringLength1To16(hotelId);
			hotelDescriptiveInfo_type0.setHotelCode(stringLength1To16);

			StringLength1To64 stringLength1To641 = new StringLength1To64();
			stringLength1To641.setStringLength1To64(room_vendor);
			hotelDescriptiveInfo_type0.setBrandCode(stringLength1To641);

			hotelDescriptiveInfos_type0.addHotelDescriptiveInfo(hotelDescriptiveInfo_type0);
			oTA_HotelDescriptiveInfoRQ2.setHotelDescriptiveInfos(hotelDescriptiveInfos_type0);

			// Rampart module should be in the repository
			ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem("C:/rezsystem/Axis2/axis2-1.4.1/repository", null);

			ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
			stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(1000 * 60 * 2);

			ServiceClient sc = stub._getServiceClient();

			sc.engageModule("rampart");
			Options options = sc.getOptions();
			options.setUserName(username);
			options.setPassword(password);

			System.out.println("-----Posting-----");

			OTA_HotelDescriptiveInfoRS oTA_HotelDescriptiveInfoRS = (stub == null ? buildReservationServiceStub() : stub).HotelInfo(oTA_HotelDescriptiveInfoRQ2);

			org.apache.axis2.databinding.types.URI urlz = oTA_HotelDescriptiveInfoRS.getOTA_HotelDescriptiveInfoRSChoice_type0().getOTA_HotelDescriptiveInfoRSSequence_type0().getHotelDescriptiveContents().getHotelDescriptiveContent()[0].getMultimediaDescriptions().getMultimediaDescription()[0].getImageItems().getImageItem()[0].getImageFormat()[0].getURL();
			String imagepath = urlz.getPath();
			System.out.println("imagepath befor decode-->" + imagepath);
			imagepath = java.net.URLDecoder.decode(imagepath, "UTF-8");
			System.out.println("imagepath after decode-->" + imagepath);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void HotelReservation() throws Exception {
		System.out.println("================ start Hotel Reservation =====================");
		try {

			if (server.equals("live")) {

				System.out.print("Going to do a HotelReservation() in [" + server + "]. Proceed (y/n) ? ");
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String input = reader.readLine();
				if (input.equals("y"))
					System.out.println("Continuing...");
				else {
					System.out.println("Abort...");
					throw new Exception("User aborted the HotelSearch()");
				}
			}

			System.out.println("hotelId-->" + hotelId);

			OTA_HotelResRQ oTA_HotelResRQ0 = new OTA_HotelResRQ();
			HotelResRequestType hotelResRequestType = new HotelResRequestType();

			POS_Type pos = new POS_Type();
			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(agentid);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("USD");
			sourceType[0].setISOCurrency(alphaLength3);

			/*------------ Package reference -----------*/
			BookingChannel_type0 bookingChannel_type0 = new BookingChannel_type0();

			CompanyNameType companyNameType = new CompanyNameType();
			companyNameType.setStringLength0To128("Trips");
			bookingChannel_type0.setCompanyName(companyNameType);

			/*
			 * OTA_CodeType oTA_CodeType23= new OTA_CodeType(); oTA_CodeType23.setOTA_CodeType("2"); bookingChannel_type0.setType(oTA_CodeType23);
			 */

			StringLength1To32 stringLength1To32_divition = new StringLength1To32();
			stringLength1To32_divition.setStringLength1To32("123456");
			companyNameType.setDivision(stringLength1To32_divition);

			sourceType[0].setBookingChannel(bookingChannel_type0);
			/*------------ Package reference -----------*/

			pos.setSource(sourceType);
			hotelResRequestType.setPOS(pos);

			StringLength1To128 stringLength1To128 = new StringLength1To128();
			stringLength1To128.setStringLength1To128(EchoToken);
			hotelResRequestType.setEchoToken(stringLength1To128);

			hotelResRequestType.setTarget(hotel_res_Target);

			hotelResRequestType.setVersion(new BigDecimal(3.0));

			/*
			 * NonNegativeInteger nonNegativeInteger = new NonNegativeInteger("123456"); hotelResRequestType.setSequenceNmbr(nonNegativeInteger);
			 */

			Calendar calendar = Calendar.getInstance();
			hotelResRequestType.setTimeStamp(calendar);

			StringLength1To128 stringLength1To128111 = new StringLength1To128();
			stringLength1To128111.setStringLength1To128(Availcacheid);
			hotelResRequestType.setTransactionIdentifier(stringLength1To128111);

			HotelResRequestTypeSequence_type0 hotelResRequestTypeSequence_type0 = new HotelResRequestTypeSequence_type0();
			HotelReservationsType hotelReservationsType = new HotelReservationsType();
			HotelReservationType[] hotelReservationTypes = new HotelReservationType[1];
			hotelReservationTypes[0] = new HotelReservationType();

			ResGlobalInfoType resGlobalInfoType = new ResGlobalInfoType();

			ProfilesType profilesType22 = new ProfilesType();
			ProfileInfo_type0 profileInfo_type02 = new ProfileInfo_type0();
			ProfileType profileType22 = new ProfileType();
			CustomerType customerType2 = new CustomerType();

			PersonNameType personNameType2 = new PersonNameType();
			StringLength1To16 stringLength1To16s22 = new StringLength1To16();
			stringLength1To16s22 = new StringLength1To16();

			stringLength1To16s22.setStringLength1To16("Mr");
			personNameType2.setNamePrefix(stringLength1To16s22);
			StringLength1To64 stringLength1To6422s = new StringLength1To64();

			stringLength1To6422s = new StringLength1To64();
			stringLength1To6422s.setStringLength1To64("ChandikaXMLout");
			personNameType2.setGivenName(stringLength1To6422s);

			StringLength1To64 stringLength1To642222 = new StringLength1To64();
			stringLength1To642222.setStringLength1To64("Gunawardhana");
			personNameType2.setSurname(stringLength1To642222);

			StringLength1To64 stringLength1To64222222 = new StringLength1To64();
			stringLength1To64222222.setStringLength1To64("MiddileName");
			personNameType2.setMiddleName(stringLength1To64222222);

			customerType2.setPersonName(personNameType2);

			EmailType emailType2 = new EmailType();
			emailType2.setStringLength1To128("dulan@rezgateway.com");
			customerType2.setEmail(emailType2);

			Address_type0 addressInfoType = new Address_type0();
			StringLength1To255 stringLength1To255222 = new StringLength1To255();
			stringLength1To255222.setStringLength1To255("No. 3, Galle Road, Colombo 4");
			addressInfoType.setAddressLine(stringLength1To255222);

			StateProvType stateProvType2 = new StateProvType();
			StateProvCodeType stateProvCodeType2 = new StateProvCodeType();
			stateProvCodeType2.setStringLength1To8("HI");
			stateProvType2.setStateCode(stateProvCodeType2);
			stateProvType2.setStringLength0To64("Western");
			addressInfoType.setStateProv(stateProvType2);

			StringLength1To16 stringLength1To16222 = new StringLength1To16();
			stringLength1To16222.setStringLength1To16("0001");
			addressInfoType.setPostalCode(stringLength1To16222);

			StringLength1To64 stringLength1To6422221 = new StringLength1To64();
			stringLength1To6422221.setStringLength1To64("Colombo");
			CountryNameType countryNameType2 = new CountryNameType();
			countryNameType2.setStringLength0To64("Sri Lanka");
			ISO3166 iso316622 = new ISO3166();
			iso316622.setISO3166("LK");
			countryNameType2.setCode(iso316622);
			addressInfoType.setCountryName(countryNameType2);
			addressInfoType.setCityName(stringLength1To6422221);

			customerType2.setAddress(addressInfoType);

			Telephone_type0 telephone_type2s = new Telephone_type0();
			StringLength1To32 stringLength1To32222 = new StringLength1To32();
			stringLength1To32222.setStringLength1To32("0771234567");
			telephone_type2s.setPhoneNumber(stringLength1To32222);
			customerType2.setTelephone(telephone_type2s);
			// customerType2.addContactPerson(contactPersonType );

			profileType22.setCustomer(customerType2);
			profileInfo_type02.setProfile(profileType22);
			profilesType22.addProfileInfo(profileInfo_type02);
			resGlobalInfoType.setProfiles(profilesType22);

			if (this.isOnlinePayment) { // for online payment method only
				GuaranteeType guaranteeType = new GuaranteeType();
				GuaranteesAccepted_type0 guaranteesAccepted_type0 = new GuaranteesAccepted_type0();
				GuaranteeAccepted_type0 guaranteeAccepted_type0 = new GuaranteeAccepted_type0();

				PaymentCardType paymentCardType = new PaymentCardType();
				NumericStringLength1To19 numericStringLength1To19 = new NumericStringLength1To19();
				numericStringLength1To19.setNumericStringLength1To19("4111111111111111");
				paymentCardType.setCardNumber(numericStringLength1To19);

				/*
				 * OTA_CodeType oTA_CodeType = new OTA_CodeType(); oTA_CodeType.setOTA_CodeType("1"); paymentCardType.setCardType(oTA_CodeType );
				 */

				MMYYDate mMYYDate = new MMYYDate();
				mMYYDate.setMMYYDate("0922");
				paymentCardType.setExpireDate(mMYYDate);

				NumericStringLength1To8 numericStringLength1To8 = new NumericStringLength1To8();
				numericStringLength1To8.setNumericStringLength1To8("456");
				paymentCardType.setSeriesCode(numericStringLength1To8);

				PaymentCardCodeType paymentCardCodeType = new PaymentCardCodeType();
				paymentCardCodeType.setUpperCaseAlphaLength1To2("VI");
				paymentCardType.setCardCode(paymentCardCodeType);

				StringLength1To64 stringLength1To645 = new StringLength1To64();
				stringLength1To645.setStringLength1To64("Chandika");
				paymentCardType.setCardHolderName(stringLength1To645);

				PaymentCardType paymentCardType2 = new PaymentCardType();
				StringLength1To128 stringLength1To1282 = new StringLength1To128();
				stringLength1To1282.setStringLength1To128("authorization successful");
				paymentCardType2.setRemark(stringLength1To1282);
				guaranteeAccepted_type0.setPaymentCard(paymentCardType2);

				StringLength1To64 stringLength1To64225 = new StringLength1To64();
				stringLength1To64225.setStringLength1To64("d613e693-7230-428b-8b59-7f6c43ef3b45");
				guaranteeAccepted_type0.setGuaranteeID(stringLength1To64225);

				guaranteeAccepted_type0.setPaymentTransactionTypeCode(PaymentTransactionTypeCode_type0.reserve);

				guaranteesAccepted_type0.addGuaranteeAccepted(guaranteeAccepted_type0);
				guaranteeType.setGuaranteesAccepted(guaranteesAccepted_type0);
				resGlobalInfoType.setGuarantee(guaranteeType);
			}

			hotelReservationTypes[0].setResGlobalInfo(resGlobalInfoType);

			String[] rid = roomId.split(",");
			if (rid.length != noOfRooms)
				throw new Exception("No of Selected Room Types are not equal to the No of Requested Rooms");

			RoomStaysType roomStaysType = new RoomStaysType();
			RoomStay_type1[] roomStay_type1s = new RoomStay_type1[noOfRooms];

			for (int i = 0; i < noOfRooms; i++) {

				roomStay_type1s[i] = new RoomStay_type1();

				RoomTypes_type1 roomTypes_type1 = new RoomTypes_type1();

				RoomTypeType[] roomTypeTypes = new RoomTypeType[1];
				roomTypeTypes[0] = new RoomTypeType();
				StringLength1To64 stringLength1To64 = new StringLength1To64();
				stringLength1To64.setStringLength1To64(rid[i]);
				roomTypeTypes[0].setRoomTypeCode(stringLength1To64);
				roomTypes_type1.setRoomType(roomTypeTypes);
				roomStay_type1s[i].setRoomTypes(roomTypes_type1);

				BasicPropertyInfoType basicPropertyInfoType = new BasicPropertyInfoType();
				StringLength1To16 stringLength1To16 = new StringLength1To16();
				stringLength1To16.setStringLength1To16(hotelId);
				basicPropertyInfoType.setHotelCode(stringLength1To16);

				StringLength1To64 stringLength1To642 = new StringLength1To64();
				stringLength1To642.setStringLength1To64(room_vendor);
				basicPropertyInfoType.setBrandCode(stringLength1To642);

				StringLength1To8 stringLength1To8 = new StringLength1To8();
				stringLength1To8.setStringLength1To8(citycode);
				basicPropertyInfoType.setHotelCityCode(stringLength1To8);

				roomStay_type1s[i].setBasicPropertyInfo(basicPropertyInfoType);

				GuestCountType guestCountType = new GuestCountType();

				GuestCount_type0[] guestCount_type0 = new GuestCount_type0[noOfAdults[i] + noOfChildren[i]];

				int roomPassCount;

				for (roomPassCount = 0; roomPassCount < noOfAdults[i]; roomPassCount++) {

					guestCount_type0[roomPassCount] = new GuestCount_type0();
					OTA_CodeType oTA_CodeType = new OTA_CodeType();
					oTA_CodeType.setOTA_CodeType("10");
					guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType);

				}

				for (int childCount = 0; childCount < noOfChildren[i]; childCount++, roomPassCount++) {

					guestCount_type0[roomPassCount] = new GuestCount_type0();

					OTA_CodeType oTA_CodeType22 = new OTA_CodeType();
					oTA_CodeType22.setOTA_CodeType("8");

					Numeric0To999 numeric0To9993 = new Numeric0To999();
					BigInteger b3 = new BigInteger(childAges[i].split(",")[childCount]);
					numeric0To9993.setNumeric0To999(b3);
					guestCount_type0[roomPassCount].setAge(numeric0To9993);
					guestCount_type0[roomPassCount].setAgeQualifyingCode(oTA_CodeType22);

				}

				guestCountType.setGuestCount(guestCount_type0);

				roomStay_type1s[i].setGuestCounts(guestCountType);

				TimeSpan_type0 timeSpan_type0 = new TimeSpan_type0();

				timeSpan_type0.setStart(indate);
				timeSpan_type0.setEnd(outdate);

				roomStay_type1s[i].setTimeSpan(timeSpan_type0);

			}

			roomStaysType.setRoomStay(roomStay_type1s);

			hotelReservationTypes[0].setRoomStays(roomStaysType);

			/*
			 * UniqueID_Type uniqueID_Type = new UniqueID_Type(); StringLength1To64 stringLength1To64222 = new StringLength1To64(); stringLength1To64222.setStringLength1To64("1234567");
			 * uniqueID_Type.setID(stringLength1To64222 ); OTA_CodeType oTA_CodeType22 = new OTA_CodeType(); oTA_CodeType22.setOTA_CodeType("38"); uniqueID_Type.setType(oTA_CodeType22 );
			 * StringLength1To32 stringLength1To32223 = new StringLength1To32(); stringLength1To32223.setStringLength1To32("taproNo"); uniqueID_Type.setID_Context(stringLength1To32223 );
			 * hotelReservationTypes[0].addUniqueID(uniqueID_Type );
			 */

			ResGuestsType resGuestsType = new ResGuestsType();

			for (int i = 0; i < noOfRooms; i++) {

				for (int adultCount = 0; adultCount < noOfAdults[i]; adultCount++) {

					ResGuest_type0 resGuest_type01 = new ResGuest_type0();
					ProfilesType profilesType1 = new ProfilesType();
					ProfileInfo_type0 profileInfo_type01 = new ProfileInfo_type0();
					ProfileType profileType1 = new ProfileType();
					CustomerType customerType1 = new CustomerType();
					PersonNameType personNameType1 = new PersonNameType();

					StringLength1To16 stringLength1To16s1 = new StringLength1To16();
					stringLength1To16s1 = new StringLength1To16();
					stringLength1To16s1.setStringLength1To16("Mr");
					personNameType1.setNamePrefix(stringLength1To16s1);

					StringLength1To64 stringLength1To64s1 = new StringLength1To64();
					stringLength1To64s1 = new StringLength1To64();
					stringLength1To64s1.setStringLength1To64("Chadika" + (convert(i)) + "rm" + convert(adultCount + 1) + "ad");
					personNameType1.setGivenName(stringLength1To64s1);

					StringLength1To64 stringLength1To641 = new StringLength1To64();
					stringLength1To641.setStringLength1To64("Gunawardhana" + (convert(i)) + "rm" + convert(adultCount + 1) + "ad");
					personNameType1.setSurname(stringLength1To641);

					StringLength1To64 stringLength1To64221 = new StringLength1To64();
					stringLength1To64221.setStringLength1To64("XML" + (convert(i)) + "rm" + convert(adultCount + 1) + "ad");
					personNameType1.setMiddleName(stringLength1To64221);

					if (i == 0 && adultCount == 0) {

						Telephone_type0 telephone_type1s = new Telephone_type0();
						StringLength1To32 stringLength1To322 = new StringLength1To32();
						stringLength1To322.setStringLength1To32("0771234567");
						telephone_type1s.setPhoneNumber(stringLength1To322);
						customerType1.setTelephone(telephone_type1s);

						EmailType emailType = new EmailType();
						emailType.setStringLength1To128("chandika@rezgateway.com");
						customerType1.setEmail(emailType);

						Address_type0 address_type1s = new Address_type0();
						StringLength1To255 stringLength1To255 = new StringLength1To255();
						stringLength1To255.setStringLength1To255("No. 3, Galle Road, Colombo 4");
						address_type1s.setAddressLine(stringLength1To255);

						StateProvType stateProvType = new StateProvType();
						StateProvCodeType stateProvCodeType = new StateProvCodeType();
						stateProvCodeType.setStringLength1To8("HI");
						stateProvType.setStateCode(stateProvCodeType);
						stateProvType.setStringLength0To64("Western");
						address_type1s.setStateProv(stateProvType);

						StringLength1To16 stringLength1To16 = new StringLength1To16();
						stringLength1To16.setStringLength1To16("0001");
						address_type1s.setPostalCode(stringLength1To16);

						StringLength1To64 stringLength1To642 = new StringLength1To64();
						stringLength1To642.setStringLength1To64("Colombo");
						CountryNameType countryNameType = new CountryNameType();
						countryNameType.setStringLength0To64("Sri Lanka");
						ISO3166 iso31662 = new ISO3166();
						iso31662.setISO3166("LK");
						countryNameType.setCode(iso31662);
						address_type1s.setCountryName(countryNameType);
						address_type1s.setCityName(stringLength1To642);
						customerType1.setAddress(address_type1s);
					}

					customerType1.setPersonName(personNameType1);

					profileType1.setCustomer(customerType1);
					profileInfo_type01.setProfile(profileType1);
					profilesType1.addProfileInfo(profileInfo_type01);
					resGuest_type01.setProfiles(profilesType1);

					Numeric0To999 numeric0To9991 = new Numeric0To999();
					BigInteger bigInteger1 = new BigInteger("24");
					numeric0To9991.setNumeric0To999(bigInteger1);
					resGuest_type01.setAge(numeric0To9991);
					resGuestsType.addResGuest(resGuest_type01);

				}

				for (int childCount = 0; childCount < noOfChildren[i]; childCount++) {

					ResGuest_type0 resGuest_type01 = new ResGuest_type0();
					ProfilesType profilesType1 = new ProfilesType();
					ProfileInfo_type0 profileInfo_type01 = new ProfileInfo_type0();
					ProfileType profileType1 = new ProfileType();
					CustomerType customerType1 = new CustomerType();
					PersonNameType personNameType1 = new PersonNameType();

					StringLength1To16 stringLength1To16s1 = new StringLength1To16();
					stringLength1To16s1 = new StringLength1To16();
					stringLength1To16s1.setStringLength1To16("Mr");
					personNameType1.setNamePrefix(stringLength1To16s1);

					StringLength1To64 stringLength1To64s1 = new StringLength1To64();
					stringLength1To64s1 = new StringLength1To64();
					stringLength1To64s1.setStringLength1To64("Chandika" + (convert(i)) + "rm" + convert(childCount + 1) + "chd");
					personNameType1.setGivenName(stringLength1To64s1);

					StringLength1To64 stringLength1To641 = new StringLength1To64();
					stringLength1To641.setStringLength1To64("Gunawardhana" + (convert(i)) + "rm" + convert(childCount + 1) + "chd");
					personNameType1.setSurname(stringLength1To641);

					StringLength1To64 stringLength1To64221 = new StringLength1To64();
					stringLength1To64221.setStringLength1To64("XML" + (convert(i)) + "rm" + convert(childCount + 1) + "chd");
					personNameType1.setMiddleName(stringLength1To64221);

					customerType1.setPersonName(personNameType1);

					profileType1.setCustomer(customerType1);
					profileInfo_type01.setProfile(profileType1);
					profilesType1.addProfileInfo(profileInfo_type01);
					resGuest_type01.setProfiles(profilesType1);

					Numeric0To999 numeric0To9991 = new Numeric0To999();
					BigInteger bigInteger1 = new BigInteger(childAges[i].split(",")[childCount]);
					numeric0To9991.setNumeric0To999(bigInteger1);
					resGuest_type01.setAge(numeric0To9991);
					resGuestsType.addResGuest(resGuest_type01);

				}

			}

			hotelReservationTypes[0].setResGuests(resGuestsType);

			// ResGlobalInfoType resGlobalInfoType = new ResGlobalInfoType();
			/*
			 * TimeSpan_type0 timeSpan_type0 = new TimeSpan_type0();
			 * 
			 * Calendar cal1 = Calendar.getInstance(); cal1.setTime(indate); Calendar cal2 = Calendar.getInstance(); cal2.setTime(outdate);
			 * 
			 * timeSpan_type0.setStart(cal1); timeSpan_type0.setEnd(cal2);
			 */

			// resGlobalInfoType.setTimeSpan(timeSpan_type0);
			// hotelReservationTypes[0].setResGlobalInfo(resGlobalInfoType);

			hotelReservationsType.setHotelReservation(hotelReservationTypes);
			hotelResRequestTypeSequence_type0.setHotelReservations(hotelReservationsType);
			hotelResRequestType.setHotelResRequestTypeSequence_type0(hotelResRequestTypeSequence_type0);

			oTA_HotelResRQ0.setOTA_HotelResRQ(hotelResRequestType);

			System.out.println("Start calling reservation...");
			OTA_HotelResRS oTA_HotelResRS = (stub == null ? buildReservationServiceStub() : stub).HotelReservation(oTA_HotelResRQ0);
			System.out.println("End calling reservation...");

			UniqueID_Type[] uniqueID_Types = oTA_HotelResRS.getOTA_HotelResRS().getHotelResResponseTypeChoice_type0().getHotelResResponseTypeSequence_type0().getHotelReservations().getHotelReservation()[0].getUniqueID();

			System.out.println("uniqueID 1-->" + uniqueID_Types[0].getID().getStringLength1To64());
			System.out.println("Reservation No:" + uniqueID_Types[1].getID().getStringLength1To64());

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("================ end Hotel Reservation =====================");
	}

	public void HotelCancellation() {

		try {

			System.out.println("hotelId-->" + hotelId);

			OTA_CancelRQ oTA_CancelRQ8 = new OTA_CancelRQ();

			POS_Type pos = new POS_Type();
			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(agentid);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("USD");
			sourceType[0].setISOCurrency(alphaLength3);

			pos.setSource(sourceType);
			oTA_CancelRQ8.setPOS(pos);
			StringLength1To128 stringLength1To128 = new StringLength1To128();
			stringLength1To128.setStringLength1To128(EchoToken);
			oTA_CancelRQ8.setEchoToken(stringLength1To128);

			oTA_CancelRQ8.setTarget(hotel_cncl_Target);
			oTA_CancelRQ8.setVersion(new BigDecimal(3.0));
			/*
			 * NonNegativeInteger nonNegativeInteger = new NonNegativeInteger("123456"); oTA_CancelRQ8.setSequenceNmbr(nonNegativeInteger);
			 */

			Calendar calendar = Calendar.getInstance();
			oTA_CancelRQ8.setTimeStamp(calendar);

			// oTA_CancelRQ8.setCancelType(TransactionActionType.Cancel);

			UniqueID_type0 uniqueID_type0 = new UniqueID_type0();

			OTA_CodeType oTA_CodeType = new OTA_CodeType();
			oTA_CodeType.setOTA_CodeType("1");
			uniqueID_type0.setType(oTA_CodeType);

			StringLength1To64 stringLength1To32 = new StringLength1To64();
			stringLength1To32.setStringLength1To64(CancellationrefNo);
			uniqueID_type0.setID(stringLength1To32);
			oTA_CancelRQ8.addUniqueID(uniqueID_type0);

			Reasons_type0 reasons_type0 = new Reasons_type0();
			FreeTextType freeTextType = new FreeTextType();
			freeTextType.setString("Due to test booking.");
			reasons_type0.addReason(freeTextType);
			oTA_CancelRQ8.setReasons(reasons_type0);

			// Rampart module should be in the repository
			ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem("C:/rezsystem/Axis2/axis2-1.4.1/repository", null);

			ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
			stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(1000 * 60 * 2);

			ServiceClient sc = stub._getServiceClient();

			sc.engageModule("rampart");
			Options options = sc.getOptions();
			options.setUserName(username);
			options.setPassword(password);

			// ReservationServiceStub stub = new ReservationServiceStub();

			OTA_CancelRS oTA_HotelResRS = stub.HotelCancel(oTA_CancelRQ8);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void HotelResRetrieve() {

		try {

			OTA_ReadRQ oTA_ReadRQ4 = new OTA_ReadRQ();

			POS_Type pos = new POS_Type();
			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64("TRIP_TOUR_1");
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);
			ISO3166 iso3166 = new ISO3166();
			iso3166.setISO3166("LK");
			sourceType[0].setISOCountry(iso3166);
			AlphaLength3 alphaLength3 = new AlphaLength3();
			alphaLength3.setAlphaLength3("USD");
			sourceType[0].setISOCurrency(alphaLength3);
			pos.setSource(sourceType);
			oTA_ReadRQ4.setPOS(pos);
			StringLength1To128 stringLength1To128 = new StringLength1To128();
			stringLength1To128.setStringLength1To128(EchoToken);
			oTA_ReadRQ4.setEchoToken(stringLength1To128);

			oTA_ReadRQ4.setTarget(Target_type12.Test);
			oTA_ReadRQ4.setVersion(new BigDecimal(3.0));

			Calendar calendar = Calendar.getInstance();
			oTA_ReadRQ4.setTimeStamp(calendar);

			UniqueID_Type uniqueID_Type = new UniqueID_Type();
			OTA_CodeType oTA_CodeType = new OTA_CodeType();
			oTA_CodeType.setOTA_CodeType("1");
			uniqueID_Type.setType(oTA_CodeType);

			StringLength1To64 stringLength1To32 = new StringLength1To64();
			stringLength1To32.setStringLength1To64(CancellationrefNo);
			uniqueID_Type.setID(stringLength1To32);
			oTA_ReadRQ4.setUniqueID(uniqueID_Type);

			// Rampart module should be in the repository
			ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem("C:/rezsystem/Axis2/axis2-1.4.1/repository", null);

			ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
			stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(1000 * 60 * 2);

			ServiceClient sc = stub._getServiceClient();

			sc.engageModule("rampart");
			Options options = sc.getOptions();
			options.setUserName(username);
			options.setPassword(password);

			OTA_ResRetrieveRS oTA_ResRetrieveRS = stub.HotelBookingRetrieve(oTA_ReadRQ4);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void HotelDownload() {

		try {

			StaticDataRQ staticDataRQ = new StaticDataRQ();

			StaticData_type0 staticData_type0DataType0 = new StaticData_type0();

			POS_Type pos = new POS_Type();
			SourceType[] sourceType = new SourceType[1];
			sourceType[0] = new SourceType();
			RequestorID_type0 requestorID_type0 = new RequestorID_type0();
			StringLength1To64 st132 = new StringLength1To64();
			st132.setStringLength1To64(agentid);
			requestorID_type0.setID(st132);
			OTA_CodeType codeType = new OTA_CodeType();
			codeType.setOTA_CodeType("5");
			requestorID_type0.setType(codeType);
			sourceType[0].setRequestorID(requestorID_type0);

			pos.setSource(sourceType);
			staticDataRQ.setPOS(pos);

			StringLength1To128 stringLength1To128 = new StringLength1To128();
			stringLength1To128.setStringLength1To128(EchoToken);
			staticData_type0DataType0.setEchoToken(stringLength1To128);

			staticData_type0DataType0.setTarget(Target_type8.Test);
			staticData_type0DataType0.setVersion(new BigDecimal(3.0));

			Calendar calendar = Calendar.getInstance();
			staticData_type0DataType0.setTimeStamp(calendar);

			NonNegativeInteger nonNegativeInteger = new NonNegativeInteger("1");
			staticData_type0DataType0.setSequenceNmbr(nonNegativeInteger);

			staticData_type0DataType0.setBrandCode(room_vendor);
			staticData_type0DataType0.setInfoType("HotelStaticData");
			// staticData_type0DataType0.setInfoType("CityData");
			// staticData_type0DataType0.setInfoType("CountryData");
			// staticData_type0DataType0.setInfoType("StateData");
			// staticData_type0DataType0.setInfoType("SuburbData");

			staticDataRQ.setStaticData(staticData_type0DataType0);

			// Rampart module should be in the repository
			ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem("C:/rezsystem/Axis2/axis2-1.4.1/repository", null);

			ReservationServiceStub stub = new ReservationServiceStub(ctx, url);
			stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(1000 * 60 * 2);

			ServiceClient sc = stub._getServiceClient();

			sc.engageModule("rampart");
			Options options = sc.getOptions();
			options.setUserName(username);
			options.setPassword(password);

			options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_GZIP_REQUEST, Boolean.TRUE);
			options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_GZIP_RESPONSE, Boolean.TRUE);
			options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_ACCEPT_GZIP, Boolean.TRUE);

			System.out.println("-----Posting-----");

			StaticDataRS staticDataRS = stub.HotelStaticData(staticDataRQ);

			System.out.println("-----Done-----");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private ReservationServiceStub buildReservationServiceStub() throws Exception {

		System.out.println("buildTheStub().Start=========================================================");

		ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem("C:/rezsystem/Axis2/axis2-1.4.1/repository", "C:/rezsystem/Axis2/axis2-1.4.1/conf/axis2.xml");
		stub = new ReservationServiceStub(ctx, url);
		stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(1000 * 60 * 2);

		ServiceClient sc = stub._getServiceClient();
		sc.engageModule("rampart");
		sc.engageModule("xmloutlogging");
		Options options = sc.getOptions();

		System.out.println("username--->" + username + " password-->" + password);

		options.setUserName(username);
		options.setPassword(password);

		options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_GZIP_REQUEST, Boolean.FALSE); // Content-Encoding: gzip
		options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_GZIP_RESPONSE, Boolean.TRUE);
		options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_ACCEPT_GZIP, Boolean.TRUE); // Accept-Encoding: gzip

		System.out.println("buildTheStub().End=========================================================");

		return stub;

	}

	final private static String[] units = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
	final private static String[] tens = { "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

	public static String convert(Integer i) {
		if (i < 20)
			return units[i];
		if (i < 100)
			return tens[i / 10] + ((i % 10 > 0) ? " " + convert(i % 10) : "");
		if (i < 1000)
			return units[i / 100] + " hundred" + ((i % 100 > 0) ? " and " + convert(i % 100) : "");
		if (i < 1000000)
			return convert(i / 1000) + " thousand " + ((i % 1000 > 0) ? " " + convert(i % 1000) : "");
		return convert(i / 1000000) + " million " + ((i % 1000000 > 0) ? " " + convert(i % 1000000) : "");
	}

	public void run() {

		rezwsr_soap_hotel_test rezwsr_soap_hotel_test = new rezwsr_soap_hotel_test();
		rezwsr_soap_hotel_test.callserver();

	}

	public static void main(String[] args) {
		DOMConfigurator.configure("log4j.xml");
		rezwsr_soap_hotel_test rezwsr_soap_hotel_test = new rezwsr_soap_hotel_test();
		rezwsr_soap_hotel_test.callserver();

	}

	public rezwsr_soap_hotel_test() {
		logger = Logger.getLogger(this.getClass());
	}

	private static Logger logger = null;

	String airportcode = null;
	String citycode = null;
	String hotelId = null;
	String room_vendor = null;
	String roomId = null;
	String cacheid = "-";
	String Availcacheid = null;
	String agentid = null;
	String CancellationrefNo = null;
	String EchoToken = null;
	String username = null;
	String password = null;
	int selectedHotelIndex = 0;
	int selectedRoomIndex = 0;

	String url = null;
	String sslurl = null;

	Target_type6 hotel_info_Target = null;
	Target_type2 hotel_room_Target = null;
	Target_type0 hotel_search_Target = null;
	Target_type4 hotel_res_Target = null;
	Target_type10 hotel_cncl_Target = null;

	String server = null;

	Calendar indate = Calendar.getInstance();
	Calendar outdate = Calendar.getInstance();

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	boolean returnRoomTypes = true;
	boolean singleHotelSearch = false;

	int noOfRooms = 0;

	Integer[] noOfAdults = null;
	Integer[] noOfChildren = null;
	String[] childAges = null;
	ReservationServiceStub stub = null;

	boolean isOnlinePayment = true;

	int availabilityHotelIndex = 0;
	int availabilityRoomIndex = 0;

}