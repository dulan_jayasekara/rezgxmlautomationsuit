package auto.xmlout.hotel.processors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import auto.xmlout.com.types.ContractType;
import auto.xmlout.hotel.pojo.GeneratedAvailabilityResponse;
import auto.xmlout.hotel.pojo.GeneratedCancellationResponse;
import auto.xmlout.hotel.pojo.GeneratedHotelInfoResponse;
import auto.xmlout.hotel.pojo.GeneratedReservationResponse;
import auto.xmlout.hotel.pojo.GeneratedSearchResponse;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.hotel.pojo.RoomType;
import auto.xmlout.hotel.pojo.SearchType;
import auto.xmlout.hotel.pojo.XmlOutError;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Comment_type1;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ErrorType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ErrorsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelResResponseTypeChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelStay_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_CancelRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelDescriptiveInfoRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelResRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Property_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ResGlobalInfoType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStay_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomTypeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomTypes_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TPA_ExtensionsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.UniqueID_Type;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.VendorMessageType;

public class ResponseProcessors {

	public GeneratedSearchResponse processSearchResponse(OTA_HotelSearchRS response) {
		GeneratedSearchResponse ExtractedResponse = new GeneratedSearchResponse();

		if (response == null) {
			ExtractedResponse.setResultsAvailable(false);
			return ExtractedResponse;
		}
		OTA_HotelSearchRSChoice_type0 RequestType = response.getOTA_HotelSearchRSChoice_type0();
		ErrorsType ErrorsList = RequestType.getErrors();
		Property_type0[] Property_type0 = null;

		if (ErrorsList != null) {
			ExtractedResponse.setResultsAvailable(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setResultsAvailable(true);
		}

		if (ExtractedResponse.isResultsAvailable()) {
			try {
				ExtractedResponse.setTransactionIdentifier(response.getTransactionIdentifier().toString().trim());
				Property_type0 = response.getOTA_HotelSearchRSChoice_type0().getOTA_HotelSearchRSSequence_type0().getProperties().getProperty();
				ExtractedResponse.setNumberOfHotels(Property_type0.length);
				for (int i = 0; i < Property_type0.length; i++) {

					Property_type0 CurrentProperty = Property_type0[i];
					Hotel CurrentHotel = new Hotel();
					CurrentHotel.setHotelCode(CurrentProperty.getHotelCode().toString().trim());
					CurrentHotel.setHotelName(CurrentProperty.getHotelName().toString().trim());
					CurrentHotel.setHotelVendor(CurrentProperty.getBrandCode().toString().trim());
					CurrentHotel.setAddressLine1(CurrentProperty.getAddress().getAddressLine().toString());
					CurrentHotel.setCity(CurrentProperty.getAddress().getCityName().toString().trim());
					CurrentHotel.setHotelCurrency(CurrentProperty.getRateRange().getCurrencyCode().toString().trim());
					CurrentHotel.setLatitude(CurrentProperty.getPosition().getLatitude().toString().trim());
					CurrentHotel.setLongitude(CurrentProperty.getPosition().getLongitude().toString().trim());
					try {
						CurrentHotel.setStarCategory(CurrentProperty.getAward()[0].getRating());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						CurrentHotel.setStarCategory("N/A");
					}

					TPA_ExtensionsType extentions = CurrentProperty.getTPA_Extensions();
					RoomTypes_type0 rooms = extentions.getRoomTypes();
					RoomTypeType[] types = rooms.getRoomType();

					for (int j = 0; j < types.length; j++) {

						RoomTypeType CurrentRoom = types[j];
						String RoomTypeId = CurrentRoom.getRoomID().toString().trim();
						RoomType ResultsRoom = new RoomType();
						ResultsRoom.setRoomType(CurrentRoom.getRoomType().toString().trim());
						ResultsRoom.setRoomCode(CurrentRoom.getRoomTypeCode().toString());
						ResultsRoom.setTotalRate(CurrentRoom.getAdditionalDetails().getAdditionalDetail()[0].getAmount().toString().trim());
						ResultsRoom.setCurrency(CurrentRoom.getAdditionalDetails().getAdditionalDetail()[0].getCurrencyCode().toString());
						ResultsRoom.setRateType(CurrentRoom.getAdditionalDetails().getAdditionalDetail()[0].getDetailDescription().getParagraphTypeChoice()[0].getText().toString().trim());
						ResultsRoom.setBedType(CurrentRoom.getAdditionalDetails().getAdditionalDetail()[1].getDetailDescription().getParagraphTypeChoice()[0].getText().toString().trim());

						CurrentHotel.addToResultsRooms(RoomTypeId, ResultsRoom);
					}

					ExtractedResponse.addToHotelList(CurrentHotel);
					ExtractedResponse.addToVendorHotelMap(CurrentHotel.getHotelVendor(), CurrentHotel);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

	public void fillAvailabilityDetails(SearchType search, Hotel hotel) {
		search.setHotelID(hotel.getHotelCode().trim());
		search.setHotelVendor(hotel.getHotelVendor());

	   if(search.getHotelContract() == ContractType.INTERNAL){
			java.util.TreeMap<String, String> ExpectedRooms = (java.util.TreeMap<String, String>) search.getSelectedRooms();

			Iterator<Map.Entry<String, String>> itr = ExpectedRooms.entrySet().iterator();
			while (itr.hasNext()) {
				Map.Entry<String, String> entry = itr.next();
				String RoomNum = entry.getKey();
				String RoomCode = entry.getValue();

				ArrayList<RoomType> AvailableRooms = hotel.getResultsRooms().get(RoomNum);

				for (Iterator<RoomType> iterator = AvailableRooms.iterator(); iterator.hasNext();) {
					RoomType roomType = (RoomType) iterator.next();
					String AvailableCode = roomType.getRoomType() + "/" + roomType.getBedType() + "/" + roomType.getRateType();

					if (RoomCode.trim().equalsIgnoreCase(AvailableCode)) {
						search.addToSelectedRoomCodes(RoomNum, roomType.getRoomCode().trim());
					}

				}
			}
	   }else{
		   
		   Map<String, ArrayList<RoomType>> AvailableRooms = hotel.getResultsRooms();
		
		   Iterator<Map.Entry<String,ArrayList<RoomType>>>  itr = AvailableRooms.entrySet().iterator();
		   while (itr.hasNext()) {
			Map.Entry<java.lang.String, java.util.ArrayList<auto.xmlout.hotel.pojo.RoomType>> entry = (Map.Entry<java.lang.String, java.util.ArrayList<auto.xmlout.hotel.pojo.RoomType>>) itr.next();
			String RoomNumber          = entry.getKey().trim();
			int incr = (entry.getValue().size()==1) ? 0: new Random().nextInt(entry.getValue().size()-1);
			String SelectedCode        = entry.getValue().get(incr).getRoomCode();
			search.addToSelectedRoomCodes(RoomNumber,SelectedCode);
			
			}
		   
	   }

	}

	public GeneratedAvailabilityResponse processAvailabilityResponse(OTA_HotelAvailRS response) {
		GeneratedAvailabilityResponse ExtractedResponse = new GeneratedAvailabilityResponse();

		if (response == null) {
			ExtractedResponse.setHotelAvailable(false);
			return ExtractedResponse;
		}
		OTA_HotelAvailRSChoice_type0 RequestType = response.getOTA_HotelAvailRSChoice_type0();
        ErrorsType ErrorsList = RequestType.getErrors();
		HotelStay_type0 hotelStay_type0 = null;

		if (ErrorsList != null) {
			ExtractedResponse.setHotelAvailable(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setHotelAvailable(true);
		}

		// ================================================================================================//

		if (ExtractedResponse.isHotelAvailable()) {
			try {
				Hotel ResultHotel = new Hotel();
				ExtractedResponse.setTransactionIdentifier(response.getTransactionIdentifier().toString());
				hotelStay_type0 = response.getOTA_HotelAvailRSChoice_type0().getOTA_HotelAvailRSSequence_type0().getHotelStays().getHotelStay()[0];

				ResultHotel.setHotelCode(hotelStay_type0.getBasicPropertyInfo().getHotelCode().getStringLength1To16());
				ResultHotel.setHotelName(hotelStay_type0.getBasicPropertyInfo().getHotelName().getStringLength1To128());
				ResultHotel.setHotelVendor(hotelStay_type0.getBasicPropertyInfo().getBrandCode().getStringLength1To64());
				ResultHotel.setHotelAvailability(hotelStay_type0.getAvailability().toString());

				VendorMessageType[] VendorMessages = hotelStay_type0.getBasicPropertyInfo().getVendorMessages()[0].getVendorMessage();

				for (VendorMessageType vendorMessageType : VendorMessages) {
					String MessageType = vendorMessageType.getSubSection()[0].getSubTitle().toString();
					String Message = vendorMessageType.getSubSection()[0].getParagraph()[0].getParagraphTypeChoice()[0].getText().toString();

					if (MessageType.contains("Short"))
						ResultHotel.setShortDescription(Message);
					else if (MessageType.contains("long"))
						ResultHotel.setLongDescription(Message);
				}

				RoomStay_type0[] roomStay_type0s = response.getOTA_HotelAvailRSChoice_type0().getOTA_HotelAvailRSSequence_type0().getRoomStays().getRoomStay();

				for (int k = 0; k < roomStay_type0s.length; k++) {

					String RoomTypeCode = roomStay_type0s[k].getRoomTypes().getRoomType()[0].getRoomTypeCode().getStringLength1To64();
					String RoomType = roomStay_type0s[k].getRoomTypes().getRoomType()[0].getRoomType().getStringLength1To64();

					System.out.println("RoomTypeCode-->" + RoomTypeCode);
					System.out.println("RoomType-->" + RoomType);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

	public GeneratedReservationResponse processReservationResponse(OTA_HotelResRS response) {
		GeneratedReservationResponse ExtractedResponse = new GeneratedReservationResponse();

		if (response == null) {
			ExtractedResponse.setConfirmationSuccess(false);
			return ExtractedResponse;
		}
		HotelResResponseTypeChoice_type0 ResponseType = response.getOTA_HotelResRS().getHotelResResponseTypeChoice_type0();
		ErrorsType ErrorsList = ResponseType.getErrors();
	

		if (ErrorsList != null) {
			ExtractedResponse.setConfirmationSuccess(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setConfirmationSuccess(true);
		}

		// ================================================================================================//

		if (ExtractedResponse.isConfirmationSuccess()) {
			try {
				UniqueID_Type[] UniqueIDs = ResponseType.getHotelResResponseTypeSequence_type0().getHotelReservations().getHotelReservation()[0].getUniqueID();

				for (UniqueID_Type uniqueID_Type : UniqueIDs) {
					String Context = uniqueID_Type.getID_Context().getStringLength1To32();

					if (Context.equalsIgnoreCase("ReservationNo")) {
						ExtractedResponse.setConfirmationNumber(uniqueID_Type.getID().getStringLength1To64());
					} else if (Context.equalsIgnoreCase("ServiceID")) {
						ExtractedResponse.setServiceID(uniqueID_Type.getID().getStringLength1To64());

					}
				}

				ResGlobalInfoType GlobalInfo = ResponseType.getHotelResResponseTypeSequence_type0().getHotelReservations().getHotelReservation()[0].getResGlobalInfo();
                Comment_type1[] Comments    = GlobalInfo.getComments().getComment();
                
				for (Comment_type1 comment_type1 : Comments) {
					String CommentCategory = comment_type1.getName().getStringLength1To64().trim();

					if (CommentCategory.equalsIgnoreCase("SupplierReferenceMessage")) {
						ExtractedResponse.setSupplierReferenceMessage(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("BookingConfirmationDetail")) {
						ExtractedResponse.setBookingConfirmationDetail(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("TripSummary")) {
						ExtractedResponse.setTripSummary(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("CustomerContactDetails")) {
						ExtractedResponse.setCustomerContactDetails(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("TripSummaryBookingStatus")) {
						ExtractedResponse.setTripSummaryBookingStatus(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("EstimatedArrivalTime")) {
						ExtractedResponse.setEstimatedArrivalTime(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("TripSummaryCheckIn")) {
						ExtractedResponse.setTripSummaryCheckIn(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					} else if (CommentCategory.equalsIgnoreCase("TripSummaryCheckOut")) {
						ExtractedResponse.setTripSummaryCheckOut(comment_type1.getParagraphTypeChoice()[0].getText().getString());
					}
				}
			
				ExtractedResponse.setTotalRate(GlobalInfo.getTotal().getAmountAfterTax().toString());
				ExtractedResponse.setRateCurrency(GlobalInfo.getTotal().getCurrencyCode().getAlphaLength3());
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

	public GeneratedCancellationResponse processCancellationResponse(OTA_CancelRS response) {
		GeneratedCancellationResponse ExtractedResponse = new GeneratedCancellationResponse();

		if (response == null) {
			ExtractedResponse.setCancellationSuccess(false);
			return ExtractedResponse;
		}
		OTA_CancelRSChoice_type0 ResponseType = response.getOTA_CancelRSChoice_type0();
		ErrorsType ErrorsList = ResponseType.getErrors();
	

		if (ErrorsList != null) {
			ExtractedResponse.setCancellationSuccess(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setCancellationSuccess(true);
		}

		// ================================================================================================//

		if (ExtractedResponse.isCancellationSuccess()) {
			try {
				UniqueID_Type[]  UniqueType = ResponseType.getOTA_CancelRSSequence_type0().getUniqueID();
				
				for (int i = 0; i < UniqueType.length; i++) {
					if(UniqueType[i].getID_Context().getStringLength1To32().equalsIgnoreCase("CancellationNo"))
					ExtractedResponse.setCancellationNumber(UniqueType[i].getID().getStringLength1To64());
					else if(UniqueType[i].getID_Context().getStringLength1To32().equalsIgnoreCase("ServiceID"))
					ExtractedResponse.setServiceID(UniqueType[i].getID().getStringLength1To64());
					else if(UniqueType[i].getID_Context().getStringLength1To32().equalsIgnoreCase("SupplierConfirmation"))
				    ExtractedResponse.setSupplierConfirmation(UniqueType[i].getID().getStringLength1To64());
				}
				
				ExtractedResponse.setComment(ResponseType.getOTA_CancelRSSequence_type0().getComment().getParagraphTypeChoice()[0].getText().toString());
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

	public GeneratedHotelInfoResponse processHotelInfoResponse(OTA_HotelDescriptiveInfoRS response) {
		GeneratedHotelInfoResponse ExtractedResponse = new GeneratedHotelInfoResponse();

		if (response == null) {
			ExtractedResponse.setHotelInfoAvailable(false);
			return ExtractedResponse;
		}
		OTA_HotelDescriptiveInfoRSChoice_type0 ResponseType = response.getOTA_HotelDescriptiveInfoRSChoice_type0();
		ErrorsType ErrorsList = ResponseType.getErrors();
	

		if (ErrorsList != null) {
			ExtractedResponse.setHotelInfoAvailable(false);
			ErrorType[] Errors = ErrorsList.getError();

			for (int i = 0; i < Errors.length; i++) {
				ErrorType Error = Errors[i];
				XmlOutError Err = new XmlOutError();
				Err.setErrorCode(Error.getType().toString());
				Err.setErrorDescription(Error.getShortText().toString());
				ExtractedResponse.addToErrors(Err);

			}
			return ExtractedResponse;
		} else {
			ExtractedResponse.setHotelInfoAvailable(true);
		}

		// ================================================================================================//

		if (ExtractedResponse.isHotelInfoAvailable()) {
			try {
				
				org.apache.axis2.databinding.types.URI urlz = ResponseType.getOTA_HotelDescriptiveInfoRSSequence_type0().getHotelDescriptiveContents().getHotelDescriptiveContent()[0].getMultimediaDescriptions().getMultimediaDescription()[0].getImageItems().getImageItem()[0].getImageFormat()[0].getURL();
				String imagepath = urlz.getPath();
				System.out.println("imagepath befor decode-->" + imagepath);
				imagepath = java.net.URLDecoder.decode(imagepath, "UTF-8");
				System.out.println("imagepath after decode-->" + imagepath);
				
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}



}
