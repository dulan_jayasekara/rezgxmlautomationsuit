package auto.xmlout.hotel.loaders;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import auto.xmlout.com.types.HotelXmlSearchByType;
import auto.xmlout.hotel.pojo.SearchType;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.hotel.pojo.RoomType;
import auto.xmlout.hotel.pojo.CartDetails;

public class SearchDataLoader {

	private Map<String, String> CurrencyMap;
	private String PortalCurrency;
	private Logger logger = null;

	public SearchDataLoader(Map<Integer, String> map, String Currency) {
		logger = Logger.getLogger(this.getClass().getName());

		try {
			PortalCurrency = Currency;
            CurrencyMap = new HashMap<String, String>();
			Iterator<Map.Entry<Integer, String>> iterator = map.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<Integer, String> entry = (Map.Entry<Integer, String>) iterator.next();
				String[] AllValues = entry.getValue().split(",");
				CurrencyMap.put(AllValues[0], AllValues[1]);

			}

		} catch (Exception e) {
			logger.fatal("Error when updating the currency list -->" ,e);
			// System.out.println("ErrorOccured While updating the CurrencyList");
		}

	}

	public ArrayList<SearchType> loadOccupancySearchDetails(Map<Integer, String> map) throws ParseException {
		ArrayList<SearchType> searchList = new ArrayList<SearchType>();

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			SearchType CurrentSearch = new SearchType();
			String[] AllValues = it.next().getValue().split(",");
			CurrentSearch.setCountryOFResidance(AllValues[0]);
			CurrentSearch.setSerachBy(HotelXmlSearchByType.getChargeType(AllValues[1].trim()));

			if (CurrentSearch.getSerachBy() == HotelXmlSearchByType.CITYCODE)
				CurrentSearch.setCityCode(AllValues[2].trim());
			else if (CurrentSearch.getSerachBy() == HotelXmlSearchByType.AIRPORTCODE)
				CurrentSearch.setAirPortCode(AllValues[2].trim());

			CurrentSearch.setHiddenLoc(AllValues[2]);
			CurrentSearch.setNights(AllValues[4]);
			CurrentSearch.setBookingType(AllValues[3]);
			CurrentSearch.setRooms(AllValues[5]);
			CurrentSearch.setAdults(AllValues[6]);
			CurrentSearch.setChildren(AllValues[7]);
			CurrentSearch.setChildAges(AllValues[8]);
			CurrentSearch.setHotelName(AllValues[9]);
			CurrentSearch.setHotelAvailability(AllValues[10]);
			CurrentSearch.setHotelAvailability(AllValues[11]);
			CurrentSearch.setRoomTypes(AllValues[12]);
			// CurrentSearch.setDefaultRoom(AllValues[13]);
			CurrentSearch.setDescription(AllValues[14]);
			CurrentSearch.setSearchCurrency(AllValues[15]);

			if (!(AllValues[13].equalsIgnoreCase("Not Available")) && !(PortalCurrency.equalsIgnoreCase(AllValues[14])))
				CurrentSearch.setExpected(AllValues[13], CurrencyMap.get(AllValues[14]));
			else
				CurrentSearch.setExpected(AllValues[13]);

			searchList.add(CurrentSearch);
		}
		return searchList;

	}

	public Map<String, SearchType> loadSearchDetails(Map<Integer, String> map, Map<String, Hotel> HotelList) throws ParseException {
		Map<String, SearchType> searchMap = new HashMap<String, SearchType>();

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			SearchType CurrentSearch = new SearchType();
			String[] AllValues = it.next().getValue().split(",");

			CurrentSearch.setCountryOFResidance(AllValues[1].trim());
	
			CurrentSearch.setSerachBy(HotelXmlSearchByType.getChargeType(AllValues[2].trim()));

			if (CurrentSearch.getSerachBy() == HotelXmlSearchByType.CITYCODE)
				CurrentSearch.setCityCode(AllValues[3].trim());
			else if (CurrentSearch.getSerachBy() == HotelXmlSearchByType.AIRPORTCODE)
				CurrentSearch.setAirPortCode(AllValues[3].trim());
			
			CurrentSearch.setNights(AllValues[6].trim());
			CurrentSearch.setBookingDate(AllValues[4].trim());
			// CurrentSearch.setBookinDateType(AllValues[4].trim());
			CurrentSearch.setBookingType(AllValues[5].trim());
			CurrentSearch.setRooms(AllValues[7].trim());
			CurrentSearch.setAdults(AllValues[8].trim());
			CurrentSearch.setChildren(AllValues[9].trim());
			CurrentSearch.setChildAges(AllValues[10].trim());
			CurrentSearch.setHotelContract(AllValues[11].trim());
			CurrentSearch.setHotelVendor(AllValues[12].trim());
			CurrentSearch.setHotelName(AllValues[13].trim());
			CurrentSearch.setValidationHotel(HotelList.get(CurrentSearch.getHotelName()));
			CurrentSearch.setAvailability(AllValues[14].trim());
			// CurrentSearch.setHotelAvailability(AllValues[11]);
			// CurrentSearch.setRoomTypes(AllValues[13].trim());
			// CurrentSearch.setDefaultRoom(AllValues[14].trim());
			// CurrentSearch.setSelectedRooms(AllValues[15].trim());
			CurrentSearch.setDescription(AllValues[15].trim());
			CurrentSearch.setSearchCurrency(AllValues[16].trim());
			// CurrentSearch.setTotalRate(AllValues[15].trim());
			CurrentSearch.setPaymentType(AllValues[18].trim());
			CurrentSearch.setCardType(AllValues[19].trim());
			CurrentSearch.setOfflineMethod(AllValues[20].trim());
			CurrentSearch.setPaymentRef(AllValues[21].trim());
			CurrentSearch.setAvailability(AllValues[22].trim());
			CurrentSearch.setAgentID(AllValues[23].trim());
			CurrentSearch.setScenarioExcecutionState(AllValues[24].trim());
			CurrentSearch.setResultsSortOrderType(AllValues[25].trim());
			CurrentSearch.setResultsSortByType(AllValues[26].trim());
			CurrentSearch.setCancellationNeeded(AllValues[27].trim());
			CurrentSearch.setBaseCurrency(PortalCurrency);
			/*
			 * if(!(AllValues[13].equalsIgnoreCase("Not Available")) && !(PortalCurrency.equalsIgnoreCase(AllValues[14]))) CurrentSearch.setExpected (AllValues[13],CurrencyMap.get(AllValues[14]));
			 * else CurrentSearch.setExpected(AllValues[13]);
			 */

			searchMap.put(AllValues[0].trim(), CurrentSearch);
		}
		return searchMap;

	}

	public Map<String, SearchType> loadRoomDetails(Map<Integer, String> map, Map<String, SearchType> Searchmap) {

		Iterator<Map.Entry<String, SearchType>> it = Searchmap.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<String, SearchType> CurrentEntry = it.next();
			String CurrentIndex = CurrentEntry.getKey();
			SearchType CurrentSearch = CurrentEntry.getValue();
			ArrayList<ArrayList<RoomType>> RoomListArray = new ArrayList<ArrayList<RoomType>>(Integer.parseInt(CurrentSearch.getRooms()));
			Double DefaultRoomRateTotal = 0.0;
			Double SelectedRoomRateTotal = 0.0;
			// ArrayList<ArrayList<RoomType>> RoomListArray = new
			// ArrayList<ArrayList<RoomType>>(3);

			for (int i = 0; i < Integer.parseInt(CurrentSearch.getRooms()); i++) {
				RoomListArray.add(new ArrayList<RoomType>());

			}

			Iterator<Map.Entry<Integer, String>> rawit = map.entrySet().iterator();

			while (rawit.hasNext()) {
				String[] AllValues = rawit.next().getValue().split(",");

				if (AllValues[0].equalsIgnoreCase(CurrentIndex)) {
					RoomType room = new RoomType();
					int RoomNum = Integer.parseInt(AllValues[1]);
					room.setRoomLable(AllValues[2]);
					room.setRoomType(AllValues[3]);
					room.setBedType(AllValues[4]);
					room.setRateType(AllValues[5]);
					room.setDailyRates(new ArrayList<String>(Arrays.asList(AllValues[6].split(";"))));
					room.setTotalRate(AllValues[7]);
					// room.setCurrency(AllValues[8]);
					room.setPromotionApplied((AllValues[10].trim().equalsIgnoreCase("yes") ? true : false));
					room.setPromoCode(AllValues[11].trim());

					if (AllValues[8].trim().equalsIgnoreCase("Yes")) {
						CurrentSearch.addToDefaultRooms(Integer.toString(RoomNum), room.getRoomType() + "/" + room.getBedType() + "/" + room.getRateType());
						DefaultRoomRateTotal += Double.parseDouble(room.getTotalRate());
					}
					if (AllValues[9].trim().equalsIgnoreCase("Yes")) {
						CurrentSearch.addToSelectedRooms(Integer.toString(RoomNum), room.getRoomType() + "/" + room.getBedType() + "/" + room.getRateType());
						CurrentSearch.addToSelectedRoomObj(room);
						SelectedRoomRateTotal += Double.parseDouble(room.getTotalRate());
					}
					try {
						// RoomListArray.get(1);
						RoomListArray.get(RoomNum - 1).add(room);
					} catch (Exception e) {
						System.out.println("sds");
					}

				}
			}

			int count = 1;
			for (ListIterator<ArrayList<RoomType>> iterator = RoomListArray.listIterator(); iterator.hasNext();) {
				try {
					ArrayList<RoomType> arrayList = (ArrayList<RoomType>) iterator.next();
					CurrentSearch.addToResultsRoomMap(arrayList, Integer.toString(count));
					// Searchmap.remove(CurrentIndex);
					Searchmap.put(CurrentIndex, CurrentSearch);
				} catch (Exception e) {
					System.out.println("Errorrrrr");
				}

				count++;

			}
			CurrentSearch.setTotalRate(new BigDecimal(Math.ceil(DefaultRoomRateTotal)).stripTrailingZeros().toPlainString());

			// System.out.println(CurrentSearch.getTotalRate());

		}

		return Searchmap;

	}

	public Map<String, SearchType> loadCartDetails(Map<Integer, String> map, Map<String, SearchType> Searchmap) {
		Iterator<Map.Entry<Integer, String>> rawit = map.entrySet().iterator();

		while (rawit.hasNext()) {
			CartDetails cart = new CartDetails();
			String[] AllValues = rawit.next().getValue().split(",");
			cart.setHotelLable(AllValues[1]);
			cart.setRate(AllValues[2]);
			cart.setNetRate(AllValues[3]);
			cart.setServiceFeeType(AllValues[4]);
			cart.setServiceFeeInPortalCurrency(AllValues[5]);

			Searchmap.get(AllValues[0].trim()).setCart(cart);

		}

		return Searchmap;

	}

	public double convertCurrency(String Value, String FromCurrency, String ToCurrency) {
		double returnValue = 0.0;

		try {
			double value = Double.parseDouble(Value.trim());
			double Portalvalue = value / (Double.parseDouble(CurrencyMap.get(FromCurrency.trim())));
			returnValue = Portalvalue * (Double.parseDouble(CurrencyMap.get(ToCurrency.trim())));
		} catch (Exception e) {
			System.out.println("Error in exchange rate conversion" + e.toString());
		}

		return returnValue;

	}

	public Map<String, SearchType> getSearchDetails(ArrayList<Map<Integer, String>> sheetList, Map<String, Hotel> HotelList) {
		Map<String, SearchType> SearchList = new HashMap<String, SearchType>();
		try {
			SearchList = loadSearchDetails(sheetList.get(0), HotelList);
			logger.info("Initial Search Details Loaded Successfully");
			logger.debug(SearchList);
		} catch (Exception e) {
			logger.fatal("Initial Search Details Not Loaded Sucessfully");
			logger.fatal(e.toString());
		}

		try {
			SearchList = loadRoomDetails(sheetList.get(1), SearchList);

			// TODO:Delete this///////////////////////////////////////

			Iterator<Map.Entry<String, SearchType>> iter = SearchList.entrySet().iterator();

			while (iter.hasNext()) {
				Map.Entry<String, SearchType> type = iter.next();
				String index = type.getKey();
				SearchType Value = type.getValue();

				for (Iterator<Map.Entry<String, ArrayList<RoomType>>> iterator = Value.getResultsRoomMap().entrySet().iterator(); iterator.hasNext();) {
					Map.Entry<String, ArrayList<RoomType>> type2 = (Entry<String, ArrayList<RoomType>>) iterator.next();
					logger.info("Scenario " + index + " Room " + type2.getKey() + " rooms--->");
					System.out.println("Scenario " + index + " Room " + type2.getKey() + " rooms--->");

					for (Iterator<RoomType> iterator2 = type2.getValue().iterator(); iterator2.hasNext();) {
						RoomType type3 = (RoomType) iterator2.next();
						System.out.println(type3.getRoomType());

					}

				}
			}

			// //////////////////////////////////////////////////////////////////
			logger.info("Room Details Details Loaded Successfully");
			logger.debug(SearchList);
		} catch (Exception e) {
			logger.fatal("Room Details Details Not Loaded Sucessfully");
			logger.fatal(e.toString());
		}

		try {
			SearchList = loadCartDetails(sheetList.get(2), SearchList);
			logger.info("Cart Details Loaded Successfully");
			logger.debug(SearchList);
		} catch (Exception e) {
			logger.fatal("Cart Details Not Loaded Sucessfully", e);
			// logger.fatal(e.toString());
		}

		return SearchList;

	}

}
