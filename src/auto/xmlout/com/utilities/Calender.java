package auto.xmlout.com.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Calender {


public static String getDate(int filed,int increment)
{
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
	cal.add(filed,increment);
	
	return sdf.format(cal.getTime());
}

public static String getDate(int filed,int increment,String pattern)
{
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
	cal.add(filed,increment);
	
	return sdf.format(cal.getTime());
}


public static String getDate(int months,int days,String pattern,String ee)
{
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
	cal.add(Calendar.MONTH,months);
	cal.add(Calendar.DATE,days);
	
	return sdf.format(cal.getTime());
}

public static String getDate(String AvailPattern,String NeededPattern,String Date) throws ParseException
{
	//Calendar cal = Calendar.getInstance();
	SimpleDateFormat sdf1 = new SimpleDateFormat(AvailPattern);
	SimpleDateFormat sdf2 = new SimpleDateFormat(NeededPattern);
	return sdf2.format(sdf1.parse(Date));
}

	public static Calendar getCalendarDate(String Date, String AvailPattern)
			throws ParseException {

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(AvailPattern);

		cal.setTime(sdf.parse(Date));
		cal.set(Calendar.HOUR, 00);
		cal.set(Calendar.MINUTE, 00);
		cal.set(Calendar.SECOND, 00);
		cal.set(Calendar.MILLISECOND, 00);

		return cal;
	}

}
