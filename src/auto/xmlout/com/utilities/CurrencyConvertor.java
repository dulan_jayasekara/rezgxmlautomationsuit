package auto.xmlout.com.utilities;

import java.util.HashMap;


public class CurrencyConvertor {

	private HashMap<String, String> CurrencyMap = null;
	public CurrencyConvertor(HashMap<String, String> map){
		if(map == null)
		CurrencyMap = map;
	}
	
	public double convertCurrency(String Value, String FromCurrency, String ToCurrency) throws Exception {
		double returnValue = 0.0;

		try {
			double value = Double.parseDouble(Value.trim());
			double Portalvalue = value / (Double.parseDouble(CurrencyMap.get(FromCurrency.trim())));
			returnValue = Portalvalue * (Double.parseDouble(CurrencyMap.get(ToCurrency.trim())));
		} catch (Exception e) {
			throw new Exception("Error in exchange rate conversion");
			//System.out.println("Error in exchange rate conversion" + e.toString());
		}

		return returnValue;

	}

}
