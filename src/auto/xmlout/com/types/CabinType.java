package auto.xmlout.com.types;

public enum CabinType {
	
	ECONOMY,BUSSINESS,FIRST,NONE;
	
	public static CabinType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Economy"))return ECONOMY;
		else if(ChargeType.equalsIgnoreCase("First"))return BUSSINESS;
		else if(ChargeType.equalsIgnoreCase("Bussiness"))return FIRST;
		else return NONE;
		
	}

	
}
