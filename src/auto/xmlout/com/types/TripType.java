package auto.xmlout.com.types;

public enum TripType {
	
	ONEWAY,TWOWAY,NONE;
	
	public static TripType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("OneWay"))return ONEWAY;
		else if(ChargeType.equalsIgnoreCase("TwoWay"))return TWOWAY;
		else return NONE;
		
	}

	
}
