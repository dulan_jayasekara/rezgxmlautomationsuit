package auto.xmlout.com.types;

public enum PricingSourceType {
	
 PUBLISHED,PRIVATE,BOTH,NONE;
 
 public static PricingSourceType getPricingSource(String ChargeType)
 {
 	if(ChargeType.equalsIgnoreCase("Published"))return PUBLISHED;
 	else if(ChargeType.equalsIgnoreCase("Private"))return PRIVATE;
 	else if(ChargeType.equalsIgnoreCase("Both"))return BOTH;
 	else return NONE;
 	
 }

}
