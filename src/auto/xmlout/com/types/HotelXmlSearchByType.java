package auto.xmlout.com.types;

public enum HotelXmlSearchByType {
	
	CITYCODE,AIRPORTCODE,NONE;
	
	public static HotelXmlSearchByType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("CityCode"))return CITYCODE;
		else if(ChargeType.equalsIgnoreCase("IATA"))return AIRPORTCODE;
		else return NONE;
		
	}

	
}
