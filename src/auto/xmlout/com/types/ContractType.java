package auto.xmlout.com.types;

public enum ContractType {
	
	INTERNAL,THIRDPARTY,NONE;
	
	public static ContractType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Internal"))return INTERNAL;
		else if(ChargeType.equalsIgnoreCase("ThirdParty"))return THIRDPARTY;
		else return NONE;
		
	}

	
}
